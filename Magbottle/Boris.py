## This is a implementation of the Boris pusher using python
## Miguel Pardal
## 16/6/2017

import numpy as np

class particle(object):
    def __init__(self,q,m):
        self.qe=q
        self.m=m
        self.q_m=q/m;
        self.u=[0,0,0]
        self.r=[0,0,0]
    
    def setv(self,v):
        self.v=v
        c=3e8
        c2=c*c
        v2=sum([vi**2 for vi in v])
        gamma=np.sqrt(1-v2/c2)
        self.u=[vi/gamma for vi in v]


    def setu(self,u):
        self.u=u
        c=3e10
        c2=c*c
        self.v=[ui/np.sqrt(1+sum([ui**2 for ui in u])/c2) for ui in u]


    def setr(self,r):
        self.r=r
    def getv(self):
        return self.v
    
    def getu(self):
        return self.u
    def getr(self):
        return self.r


class trajectory(object):
    def __init__(self,guy1,dtime=0.01):
        self.q=[]
        if isinstance(guy1,particle):
            self.part=guy1
        
        else:
            raise TypeError("Sorry, guy1 is not a paticle. Try again!")
        
        
        self.q.append(list(guy1.getr())+list(guy1.getv()))
        self.dt=dtime
        self.itr=0
        self.tem=self.dt*0.5*guy1.q_m
    
    
    def setEM(self,E,B):
        self.Efield=staticmethod(E)
        self.Bfield=staticmethod(B)
    
    
    def set_step(self,dtime):
        self.dt=dtime
        #recalculate tem
        self.tem=dtime*0.5*self.part.q_m
    
    #advance one time step (once)
    def advonce(self):
        #obtain the currnent step status
        tn=self.dt*self.itr
        un=self.part.getu()
        rn=self.part.getr()
        En=self.Efield.__func__(rn,tn)
        Bn=self.Bfield.__func__(rn,tn)
        dt=self.dt
        tem=self.tem
        
        #Start calculating stuff
        ## u'
        c=3e10
        c2=c*c
        uprime=[uni+tem*Eni for uni,Eni in zip(un,En)]
        ## |u'|^2
        up2=sum([uni*uni for uni in uprime])
        ## gamma_n
        gamma_n=np.sqrt(1+up2/c2)
        gtem=tem/gamma_n
        ## t vector
        t_vec=[Bni*gtem for Bni in Bn]
        tv2=sum([tvi*tvi for tvi in t_vec])
        ## u''
        cross1=np.cross(uprime,t_vec)
        upprime=[upi+crossi for upi,crossi in zip(uprime,cross1)]
        #print(up2)
        
        ## s
        s=[2*ti/(1+tv2) for ti in t_vec]
        #print(sum([uni*uni for uni in s]))
        
        ## u'''
        cross2=np.cross(upprime,s)
        uppprime=[upi+crossi for upi,crossi in zip(uprime,cross2)]
        ## un2
        un2=[uni+tem*Eni for uni,Eni in zip(uppprime,En)]
        
        ## |un2|^2
        un22=np.linalg.norm(un2)
        un22*=un22
        
        ## gamma_n2
        gamma_n2=np.sqrt(1+un22/c2)
        #vn
        vn=[un2i/gamma_n2 for un2i in un2]
        #print(vn)
        
        ## x(n+1)
        rnew=[rni+dt*vni for rni,vni in zip(rn,vn)]
        
        #update status for the next step
        self.itr=self.itr+1
        self.part.setr(rnew)
        self.part.setu(un2)
        self.q.append(list(rnew)+list(vn))
    
    
    def gettimes(self):
        return [self.dt*i for i in range(self.itr)]
    
    def getq(self):
        return self.q
    
    
    
    def solve(self,tf,dt):
        i=0
        self.set_step(dt)
        Npts=int(tf/dt)
        while i<Npts:
            self.advonce();
            i=i+1;

if __name__=='__main__':
    def E(r,t):
        return [0.0001,0,0]


    def B(r,t):
        return [0,0,0.5]


    jose=particle(10,1)
    jose.setu([0.001,0,0])
    traj=trajectory(jose)
    traj.setEM(E,B)

    i=0
    while i<10000:
        i+=1
        traj.advonce()
        #print(traj.part.getr())
        #print(traj.part.getu())
        #x=[ ]
        #for i in q[i][0]
    r=traj.getr()



    import matplotlib
    matplotlib.use('PDF')

    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D


    fig = plt.figure()
    ax=fig.gca(projection='3d')
    #ax = fig.add_subplot(111)
    #fig.subplots_adjust(top=0.85)
    #ax.set_title('Some drift')

    ax.set_xlabel('x ($\omega_c/v_d$)')
    ax.set_ylabel('y ($\omega_c/v_d$)')
    ax.set_zlabel('z ($\omega_c/v_d$)')




    r=np.transpose(r)
    #print(r)


    ax.plot(r[0],r[1],r[2],lw=0.5)
    ax.scatter(r[0,0],r[1,0],r[2,0],c='red')

    #print(r)
    fig.savefig('drift.pdf')
    #plt.save('drft.pdf')
    #plt.show()

