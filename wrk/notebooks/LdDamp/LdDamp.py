import os
import ipywidgets as widgets
from ipywidgets import interact, interactive, fixed, interact_manual,Layout

import numpy as np

import matplotlib
from matplotlib import pyplot as plt
from matplotlib import animation

from zdf import read
import matplotlib.patches as patches
import scipy.fftpack

from visual import *


if os.name == 'nt':
    bar="\\"
else:
    bar="/"

def pha_shower(ndump,fldrnam):
    import zdf
    import matplotlib.pyplot as plt
    bar="/"
    fldr=fldrnam+bar
    key="marker1"
    files1 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    key="marker2"
    files2 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    key="marker3"
    files3 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]

    (particles1,info1) = zdf.read(files1[0])
    (particles3,info3) = zdf.read(files3[0])

    x1=particles1["x1"]
    v1=particles1["v1"]

    x3=particles3["x1"]
    v3=particles3["v1"]


    xmin=min([min(x1),min(x3)])
    xmax=max([max(x1),max(x3)])

    vmin=min([min(v1),min(v3)])
    vmax=max([max(v1),max(v3)])
    dv=vmax-vmin
    vmin=vmin-dv/2
    vmax=vmax+dv/2

    def pltstf(itr,xlims):
        itr=int(itr/ndump)
        (particles1,info) = zdf.read(files1[itr])
        (particles2,info2) = zdf.read(files2[itr])
        (particles3,info3) = zdf.read(files3[itr])

        x1=particles1["x1"]
        v1=particles1["v1"]

        x2=particles2["x1"]
        v2=particles2["v1"]

        x3=particles3["x1"]
        v3=particles3["v1"]

        fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k')
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')
        #plt.style.use("default")
        ax1=plt.gca()
        ax1.patch.set_facecolor("k")
        lines=[]
        lines.append(ax1.plot(x1,v1,".",ms=3.,alpha=1,c="y"))
        lines.append(ax1.plot(x2,v2,".",ms=3.,alpha=0.8,c="orange"))
        lines.append(ax1.plot(x3,v3,".",ms=3.,alpha=0.8,c="r"))
        #lines.append(ax1.plot(xx,data,'+',c="coral")[0]);
        ax1.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='right')
        
        ax1.set_xlim(xlims)
        ax1.set_ylim([vmin,vmax])

        #xlabel = "x_1\,[{:s}]".format( info.particles.units.x1 )
        ylabel = "$u_1\,[{:s}]$".format( info.particles.units['v1'] )
        xlabel = "$x_1\,[{:s}]$".format( info.particles.units['x1'] )
        
        ax1.set_xlabel(xlabel)
        ax1.set_ylabel(ylabel)

        plt.show()

    sldr=widgets.IntSlider(value=0,min=0,max=ndump*(len(files1)-1),step=ndump,orientation='horizontal',readout=True,description='Iteration: ')
    sldr2=widgets.FloatRangeSlider(value=[xmin,xmax],min=xmin,max=xmax,step=(xmax-xmin)/511,orientation='horizontal',readout=True,description='Xlim: ')
    return interact(pltstf,itr=sldr,xlims=sldr2)


def damp(om,tab,gamma):  
    import scipy
    fldr=tab.children[0].children[1].children[0].value
    bar="/"
    key="E1"

    files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    (data, info) = read(files[-1])

    
    # find the current dimensions of the box
    xmin=info.grid.axis[0].min
    xmax=info.grid.axis[0].max
    npts=info.grid.nx[0]
    # sample spacing
    T = (xmax-xmin) / npts
    xf = 2*3.14*np.linspace(0.0, 1.0/(1.0*T), npts)
    tmax=info.iteration.t
    nt=len(files)-1
    p1=[]
    t=[]
    omc=om
    z1=[((x>omc-0.1) and (x<omc+0.1)) for x in xf]
    for fil in files[1:]:
        #Read the file chosen by the user
        (data, info) = read(fil)
        y = data
        yf = 2.0/npts * np.abs(scipy.fftpack.fft(y))
        p1.append(np.sum(yf[z1]))
        t.append(info.iteration.t)
    ymin=min(p1)
    ymax=max(p1)
    fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    ax1=plt.gca()
    omega_i=gamma
    cena=p1[0]*np.exp(omega_i*np.array(t))

    ax=plt.gca()
    ax.semilogy(t,p1,'.',lw=3,label="$"+"E_1"+"$")
    ax.semilogy(t,cena,lw=3,label="$"+"pred"+"$")
    ax.set_ylabel(r"|FFT $"+info.grid.label+"$|"+" [$"+info.grid.units+r"\times "+info.grid.axis[0].units+"$]")
    ax.set_xlabel("$t$ "+"$["+info.grid.axis[0].units+"$]")
    ax.set_ylim([ymin,ymax])
    plt.legend()
    plt.savefig("lddampdot.pdf")
    plt.show()
