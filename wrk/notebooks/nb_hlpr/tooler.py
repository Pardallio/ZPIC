import os
import ipywidgets as widgets
from ipywidgets import interact, interactive, fixed, interact_manual,Layout

import numpy as np

import matplotlib
from matplotlib import pyplot as plt
from matplotlib import animation

from zdf import read
import matplotlib.patches as patches
import scipy.fftpack


    
def skin_depth(tabod,filsod,omega):
    xlims=[i.value for i in   tabod.children[2].children[4].children]
    col1=tabod.children[2].children[1].children[1].value
    col2=tabod.children[2].children[2].children[1].value

    file2=filsod[0]
    file1=filsod[1]

    (dens,info1) = read(file1)

    xmin=info1.grid.axis[0].min
    xmax=info1.grid.axis[0].max
    npts=info1.grid.nx[0]
    xx=np.linspace(xmin,xmax,npts)
    ipl=0
    for it,i in enumerate(dens[1:]):
        if i<0:
            ipl=it
            break

    (efld,info2) = read(file2)

    refpl=int(ipl+1/np.sqrt(1-omega**2)/((xmax-xmin)/npts))
    print(efld[refpl])

    print(efld[ipl]/efld[refpl])

    plt.clf()
    plt.cla()
    plt.close()

    fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    ax1=plt.gca()

    #lines.append(ax1.plot(xx,data,'+',c="coral")[0]);
    ax1.set_title("$n$="+str(info1.iteration.n)+", $t$="+'{:.2f}'.format(info1.iteration.t)+"$("+info1.iteration.tunits+")$",loc='right')
    #ax1.set_xlim([xmin,xmax])
    ax1.set_xlabel("$"+info1.grid.axis[0].label+"$ ($"+info1.grid.axis[0].units+"$)")
    ax1.set_ylabel("$"+info2.grid.label+"$"+" ($"+info2.grid.units+"$)")


    ax1.plot(xx,efld,'-',c=col1,lw=2.5,ms=10,label="$"+info2.grid.label+"$");
    ax1.legend()
    ax2=ax1.twinx()

    ax2.set_xlim(xlims)
    #ax2.set_xlabel("$"+info2.grid.axis[0].label+"$"+" ($"+info2.grid.axis[0].units+"$)")
    ax2.set_ylabel("$"+info1.grid.label+"$"+" ($"+info1.grid.units+"$)")
    ax2.plot(xx,dens,c=col2,lw=2.5,label="$"+info1.grid.label+"$")
    ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))


    #ax3=plt.gca()
    #ax3.plot(np.array(datavec[0])-par[1], Emfun(datavec[0], *par), 'b-', label='fit')[0]
    plt.axvline(x=xx[refpl],color='k')

    #plt.legend()
    plt.show()




def regselect(itr2,itr,minx,maxx,file1,file2,dist,bar1,bar2):

    (dens,info) = read(file1)
    
    #build the x axis array
    xmin=info.grid.axis[0].min
    xmax=info.grid.axis[0].max
    npts=info.grid.nx[0]
    xx=np.linspace(xmin,xmax,npts)

    xmin=minx
    xmax=maxx
    #read the data from file2 (EMF file)
    (data, info) = read(file2)
    bar1.min=minx
    bar1.max=maxx
    bar2.min=minx
    bar2.max=maxx
    
    #initialize the figure
    fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    ax1=plt.gca()
    #print([xmin,xmax])
    ax1.set_xlim([xmin,xmax])
    ax1.plot(xx,data,'-',c="darkorange",lw=2.5,label="$"+info.grid.label+"$");
    
    ax1.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='right')
    #ax1.set_xlim([xmin,maxx])
    ax1.set_xlabel("$"+info.grid.axis[0].label+"$"+" ($"+info.grid.axis[0].units+"$)")
    ax1.set_ylabel("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")
    
    #plot the trasition region
    plt.axvline(x=itr,color='k')
    plt.axvline(x=itr2,color='k')

    dist.value=str(abs(itr-itr2))
    
    
    plt.show()
    
def kcalculator(file1,file2):
    a=widgets.Text(description="peak distance")
    b=widgets.FloatText(description="nr of peaks")
    c=widgets.FloatText(description="$k (c/\omega_p)$")

    bb=widgets.Button(description="Calculate k:")

    def on_click(asd):
        #check for windows (bars are different)
        c.value=2*np.pi*b.value/float(a.value)

    bb.on_click(on_click)
    (data0,inf0)=read(file1)

    #build the x axis array
    xmin=inf0.grid.axis[0].min
    xmax=inf0.grid.axis[0].max
    npts=inf0.grid.nx[0]
    xx=np.linspace(xmin,xmax,npts)


    itl=0
    for i, xi in enumerate(data0[1:]): 
        if xi < 0:   #check charge value!!!!!!!!!!#
            itl=i
            break
    xmin=xx[itl]


    sldr=widgets.FloatSlider(value=(xmin),min=xmin,max=xmax,orientation='horizontal',description='xmin:')
    sldr1=widgets.FloatSlider(value=(xmax),min=xmin,max=xmax,orientation='horizontal',description='xmax:')
    
    sldr2=widgets.FloatSlider(value=xmin-(xmin-xmax)/3,min=xmin,max=xmax,orientation='horizontal',description='bar 1:',step=0.005)
    sldr3=widgets.FloatSlider(value=xmin-2*(xmin-xmax)/3,min=xmin,max=xmax,orientation='horizontal',description='bar 2:',step=0.005)
    
    display(interact(regselect,minx=sldr,maxx=sldr1,itr2=sldr3,itr=sldr2,file1=fixed(file1),file2=fixed(file2),dist=fixed(a),bar1=fixed(sldr2),bar2=fixed(sldr3)),a,b,bb,c)


