import os
import ipywidgets as widgets
from ipywidgets import interact, interactive, fixed, interact_manual,Layout

import numpy as np

import matplotlib
from matplotlib import pyplot as plt
from matplotlib import animation

from zdf import read_grid, read_particles
import matplotlib.patches as patches
import scipy.fftpack

if os.name == 'nt':
    bar="\\"
else:
    bar="/"



#an ugly file browser (sorry...)
class FileBrowser(object):
    def __init__(self):
        self.path = os.getcwd()
        self._update_files()
        self.i=0
    def _update_files(self):
        self.files = list()
        self.dirs = list()
        if os.name == 'nt':
            bar="\\"
        else:
            bar="/"        
        if(os.path.isdir(self.path)):
            for f in os.listdir(self.path):
                ff = self.path + bar + f
                if os.path.isdir(ff):
                    self.dirs.append(f)
                else:
                    if ".zdf" in ff:
                        self.files.append(f)
    def widget(self):
        vmbox=widgets.VBox(layout=Layout(width='100%'))
        vvb=widgets.VBox()
        mbox= widgets.HBox(layout=Layout(flex='1 1 auto',width='55%'))
        vb= widgets.VBox(layout=Layout(align_items="center",align_self="stretch",width="25px"))#layout=Layout(display="flex", flex_flow='collumn',align_items='stretch',border='solid'))
        box = widgets.VBox(layout=Layout(flex='1 1 auto',width='auto',border="1.5px solid PALEGOLDENROD"))
        mbox.children= tuple([box,vb])
        vmbox.children= tuple([mbox,vvb])
        self._update(box,vvb,vb)
        
        return vmbox
    
    def _update(self, box,vvb,vb):
        def on_click(b):
            #check for windows (bars are different)                
            if b.description == '..':
                self.path = os.path.split(self.path)[0]
            else:
                self.path = self.path + bar + b.description
            self._update_files()
            self._update(box,vvb,vb)
            
        items_layout = Layout(flex='1 1 auto',width='auto',margin="1px 0px 1px 0px")
        box_layout = Layout(display='flex',flex_flow="row",align_items='stretch',
                    width='100%')

        def clear(b):
            self._clear(box,vvb,vb)        
        buttons = []
        if self.files or 1:
            button = widgets.Button(description='..',layout=items_layout)
            button.style.button_color="MOCCASIN"
            #            b2=widgets.Button(description="",layout=items_layout)
            #b2.style.button_color="MOCCASIN"
            button.on_click(on_click)
            
            hb1=widgets.HBox([button],layout=box_layout)
    
            buttons.append(hb1)

        if len(self.files+self.dirs)<20:
            for f in self.dirs:
                ll=len(f)*8-(f.count('i')+f.count('l')+f.count('.')+f.count('j')+f.count('I'))*4+sum(1 for c in f if c.isupper())*2-(f.count('t')+f.count('r')+f.count('f')+f.count('-'))*2
                #print(f,ll)
                button = widgets.Button(description=f,layout=Layout(width="{}px".format(ll),margin="1px 0px 1px 0px"), background_color='#d0d0ff',    tooltip=f)
                b2=widgets.Button(description="",layout=items_layout)
                button.style.button_color="LEMONCHIFFON"
                b2.style.button_color="LEMONCHIFFON"

                button.on_click(on_click)
                hb1=widgets.HBox([button,b2],layout=box_layout)
                buttons.append(hb1)

            for f in self.files:
                ll=len(f)*8-(f.count('i')+f.count('l')+f.count('.')+f.count('j')+f.count('I'))*4+sum(1 for c in f if c.isupper())*2-(f.count('t')+f.count('r')+f.count('f')+f.count('-'))*2
                button = widgets.Button(description=f,layout=Layout(width="{}px".format(ll),margin="1px 0px 1px 0px"), background_color='#d0d0ff',    tooltip=f)
                b2=widgets.Button(description="",layout=items_layout)
                button.on_click(on_click)

                hb1=widgets.HBox([button,b2],layout=box_layout)
    
                buttons.append(hb1)

                                
            vb.children=tuple([])

        else:
            def _scroll_dn(b):
                if self.i<len(self.dirs+self.files)-19:
                    self.i=self.i+1;
                    sldr.value=len(self.dirs+self.files)-self.i
                    self._update(box,vvb,vb)
                    
            def _scroll_up(b):
                if self.i>0:
                    self.i=self.i-1;
                    sldr.value=len(self.dirs+self.files)-self.i
                    self._update(box,vvb,vb)

            upbut=widgets.Button(icon="sort-up", button_style='info',layout=Layout(width="20px"))
            dbut=widgets.Button(icon="sort-down", button_style='info',layout=Layout(width="20px"))
            upbut.on_click(_scroll_up)
            dbut.on_click(_scroll_dn)
            sldr=widgets.IntSlider(
                value=len(self.dirs+self.files)-1-self.i,
                min=19,
                max=len(self.dirs+self.files)-1,
                continuous_update=False,
                orientation='vertical',
                readout=False,
                layout=Layout(display="flex",flex="1")
            )
            
            def on_value_change(change):
                self.i=len(self.dirs+self.files)-1-change["new"]
                self._update(box,vvb,vb)
                
            sldr.observe(on_value_change, names='value')
            vb.children=tuple([upbut,sldr,dbut])
            ult_list=self.dirs+self.files
            for f in ult_list[self.i:self.i+19]:
                ll=len(f)*8-(f.count('i')+f.count('l')+f.count('.')+f.count('j')+f.count('I'))*4+sum(1 for c in f if c.isupper())*4-(f.count('t')+f.count('r')+f.count('f')+f.count('-'))*4

                button = widgets.Button(description=f,layout=Layout(width="{}px".format(ll),margin="1px 0px 1px 0px"), background_color='#d0d0ff',    tooltip=f)
                b2=widgets.Button(description="",layout=items_layout)

                if f in self.dirs:
                    button.style.button_color="LEMONCHIFFON"
                    b2.style.button_color="LEMONCHIFFON"
                    button.on_click(on_click)
            
                hb1=widgets.HBox([button,b2],layout=box_layout)
    
    
                buttons.append(hb1)

            #buttons.append(widgets.Button(description=str(len(self.files))+" .zdf files in this folder"))
        but=widgets.Button(description="HIDE", button_style='info')
        but.on_click(clear)
        #buttons.append(but)
        if self.files:
            vaal=self.files[0].split('-')[0]
        else:
            vaal=None
        box.children = tuple(buttons)
        vvb.children=tuple([self.data_folder(),self.data_type(vaal)])

    def _clear(self,box,vvb,vb):
        def on_click(b):
            self._update_files()
            self._update(box,vvb,vb)
        buttons=[]
        but=widgets.Button(description="UNHIDE", button_style='info')
        but.on_click(on_click)
        buttons.append(but)
        if self.files:
            vaal=self.files[0].split('-')[0]
        else:
            vaal=None
        vb.children=tuple()
        box.children = tuple(buttons)
        vvb.children=tuple([self.data_folder(),self.data_type(vaal)])
        
        return box

            
    def data_folder(self):
        self.DF=widgets.Text(value=self.path,placeholder='Enter Data Folder Path (the folder where you have your data files)',description='Data Folder:',layout=Layout(width='75%'),disabled=False)
        return self.DF

    def data_type(self,val):
        self.DK=widgets.Text(value=val,placeholder='Enter the key of the data files (e.g. "charge")',description='Data Key:',layout=Layout(width='75%'),disabled=False)
        return self.DK

    def filelist(self):
        return [self.DF.value+bar+i for i in os.listdir(path) if os.path.isfile(os.path.join(path,i)) and self.DK.value in i]


def tabmaker():
    f = FileBrowser()
    f2= FileBrowser()
    
    w0=widgets.Label("Select which data sets you want to plot",layout=Layout(flex='1 1 auto',width='auto'))

    w1=widgets.Checkbox(
        value=True,
        description='Data Set 1',
        disabled=False
    )

    wcp1=widgets.ColorPicker(
        concise=False,
        description='line color',
        value="darkorange",
        disabled=False
    )

    hb1=widgets.HBox([w1,wcp1])
    
    w2=widgets.Checkbox(
        value=False,
        description='Data Set 2',
        disabled=False
    )


    wcp2=widgets.ColorPicker(
        concise=False,
        description='line color',
        value="seagreen",
        disabled=False
    )

    wp2=widgets.Checkbox(
        value=False,
        description='Plasma:',
        disabled=False
    )

    
    w02=widgets.Label("Select the x-axis range",layout=Layout(flex='1 1 auto',width='auto'))

    wxl=widgets.FloatText(
        description='lower limit:',
        value=0,
        disabled=False
    )

    wxu=widgets.FloatText(
        description='upper limit:',
        value=100,
        disabled=False
    )

    hb3=widgets.HBox([wxl,wxu])

    
    hb2=widgets.HBox([w2,wcp2,wp2])
    children = [f.widget(),f2.widget(),widgets.VBox([w0,hb1,hb2,w02,hb3])]
    tab = widgets.Tab()
    tab.children = children
    tab.set_title(0, "Data Set 1")
    tab.set_title(1, "Data Set 2")
    tab.set_title(2, "Settings")
    return tab

    

def pltinteract(filelst1,col1,filelst2=[],col2=None,xlims=None,ylim=None,fils=[],plas=False):
    ylim.clear()
    (data0,inf0)=read_grid(filelst1[0])
    ymin=min(data0)
    ymax=max(data0)
    
    ylimin=ymin-(ymax-ymin)/10
    ylimax=ymax+(ymax-ymin)/10
    ylims=[ylimin,ylimax]
    ylims2=[0,0]
    ylim.append(ylims)

    if filelst2:
        (data02,inf02)=read_grid(filelst2[0])
        ymin2=min(data02)
        ymax2=max(data02)

        ylimin2=ymin2-(ymax2-ymin2)/10
        ylimax2=ymax2+(ymax2-ymin2)/10
        ylims2=[ylimin2,ylimax2]
    ylim.append(ylims2)
    def pltstf(itr,fil,col,fil2=[],col2=None,xlims=None,ylims=None,fils=[],plasm=False):
        (data, info) = read_grid(fil[itr])
        fils.clear()
        fils.append(fil[itr])
        xmin=info['grid']['axis'][0]['min']
        xmax=info['grid']['axis'][0]['max']
        npts=info['grid']['nx'][0]
        global ymin
        global ymax
        ymin=min(data)
        ymax=max(data)
        ylimin=ymin-(ymax-ymin)/10
        ylimax=ymax+(ymax-ymin)/10

        if ylimin<ylims[0][0]:
            ylims[0][0]=ylimin

        if ylimax>ylims[0][1]:
            ylims[0][1]=ylimax

        
        xx=np.linspace(xmin,xmax,npts)
                
        xmin=xlims[0];
        xmax=xlims[1];
        
        fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')

        ax1=plt.gca()
        lines=[]
        lines.append(ax1.plot(xx,data,c=col,lw=3.5,label="$"+info['grid']['label']+"$")[0]);
        #lines.append(ax1.plot(xx,data,'+',c="coral")[0]);
        ax1.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
        ax1.set_xlim(xlims)
        ax1.set_ylim(ylims[0])
        ax1.set_xlabel("$"+info['grid']['axis'][0]['label']+"$"+" ($"+info['grid']['axis'][0]['units']+"$)")
        ax1.set_ylabel("$"+info['grid']['label']+"$"+" ($"+info['grid']['units']+"$)")
        if plasm and not fil2:
            ax1.fill_between(xx,data,0,alpha=0.4,color=col)
        if fil2:
            (data2, info2) = read_grid(fil2[itr])
            fils.append(fil2[itr])
            ax2=ax1.twinx()
            
            ymin2=min(data2)
            ymax2=max(data2)
            ylimin2=ymin2-(ymax2-ymin2)/10
            ylimax2=ymax2+(ymax2-ymin2)/10

            if ylimin2<ylims[1][0]:
                ylims[1][0]=ylimin2
            
            if ylimax2>ylims[1][1]:
                ylims[1][1]=ylimax2

            ax2.set_xlim(xlims)
            ax2.set_ylim(ylims[1])
            ax2.set_xlabel("$"+info2['grid']['axis'][0]['label']+"$"+" ($"+info2['grid']['axis'][0]['units']+"$)")
            ax2.set_ylabel("$"+info2['grid']['label']+"$"+" ($"+info2['grid']['units']+"$)")
            ax2.plot(xx,data2,c=col2,lw=3.5,label="$"+info2['grid']['label']+"$")
            ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))
            if plasm:
                ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
            #lin[0].set_data(xx,data)
            #lin[1].set_data(xx,data)
        
            #xx=np.transpose(xx)
            #print(xx,len(xx))
            ax1.legend(loc=0)
        plt.show()
        
    sldr=widgets.IntSlider(value=0,min=0,max=len(filelst1)-1,step=1,orientation='horizontal',readout=False,description=' ')
    buuuu=widgets.Button()
    return interact(pltstf,itr=sldr,fil=fixed(filelst1),fil2=fixed(filelst2),col=fixed(col1),col2=fixed(col2),xlims=fixed(xlims),ylims=fixed(ylim),fils=fixed(fils),plasm=fixed(plas))



def shower(tab,yl,fils):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value

    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value
    
    xlims=[i.value for i in   tab.children[2].children[4].children]

    pl2=tab.children[2].children[2].children[2].value
    
    if bool1 and bool2:
        fldr=tab.children[0].children[1].children[0].value
        key=tab.children[0].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    
        fldr=tab.children[1].children[1].children[0].value
        key=tab.children[1].children[1].children[1].value
        files2 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        return [pltinteract(files,col1,files2,col2,xlims,yl,fils,plas=pl2),files,files2]
    
    elif bool1 or bool2:    
        fldr=tab.children[int(bool2)].children[1].children[0].value
        key=tab.children[int(bool2)].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]

        col=tab.children[2].children[int(bool2)+1].children[1].value
        return [pltinteract(files,col,xlims=xlims,ylim=yl,fils=fils,plas=(pl2 and bool2)),files]
    

def FT_pltinteract(filelst1,col1,filelst2=[],col2=None,xlims=None,ylim=None,fils=[],plas=False):
    ylim.clear()

    (data0,inf0)=read_grid(filelst1[0])
    xmin=inf0['grid']['axis'][0]['min']
    xmax=inf0['grid']['axis'][0]['max']
    npts=inf0['grid']['nx'][0]
    
    # sample spacing
    T = xmax / npts
    x = np.linspace(0.0, npts*T, npts)
    y = data0
    yf = scipy.fftpack.fft(y)
    xf = 2*3.14*np.linspace(0.0, 1.0/(2.0*T), npts/2)

    ymin=min(yf)
    ymax=max(yf)
    
    ylimin=ymin-(ymax-ymin)/10
    ylimax=ymax+(ymax-ymin)/10
    ylims=[ylimin,ylimax]
    ylims2=[0,0]
    ylim.append(ylims)

    if filelst2:
        (data02,inf02)=read_grid(filelst2[0])

        y2 = data02
        yf = scipy.fftpack.fft(y2)

        ymin2=min(yf)
        ymax2=max(yf)

        ylimin2=ymin2-(ymax2-ymin2)/10
        ylimax2=ymax2+(ymax2-ymin2)/10
        ylims2=[ylimin2,ylimax2]
    ylim.append(ylims2)
    
    def pltstf(itr,fil,col,fil2=[],col2=None,xlims=None,ylims=None,fils=[],plasm=False):
        (data, info) = read_grid(fil[itr])
        fils.clear()
        fils.append(fil[itr])
        xmin=info['grid']['axis'][0]['min']
        xmax=info['grid']['axis'][0]['max']
        npts=info['grid']['nx'][0]

        # sample spacing
        T = xmax / npts
        x = np.linspace(0.0, npts*T, npts)
        y = data0
        yf = scipy.fftpack.fft(y)
        xf = 2*3.14*np.linspace(0.0, 1.0/(2.0*T), npts/2)

        global ymin
        global ymax
        ymin=min(yf)
        ymax=max(yf)
        ylimin=ymin-(ymax-ymin)/10
        ylimax=ymax+(ymax-ymin)/10

        if ylimin<ylims[0][0]:
            ylims[0][0]=ylimin

        if ylimax>ylims[0][1]:
            ylims[0][1]=ylimax
                        
        xmin=xlims[0];
        xmax=xlims[1];
        
        fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')

        ax1=plt.gca()
        lines=[]
        lines.append(ax1.plot(xf,2.0/N * np.abs(yf[:int(N//2)]),c=col,lw=3.5,label="$"+info['grid']['label']+"$")[0]);
        #lines.append(ax1.plot(xx,data,'+',c="coral")[0]);
        ax1.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
        ax1.set_xlim(xlims)
        ax1.set_ylim(ylims[0])
        ax1.set_xlabel("$"+info['grid']['axis'][0]['label']+"$"+" ($"+info['grid']['axis'][0]['units']+"$)")
        ax1.set_ylabel("$"+info['grid']['label']+"$"+" ($"+info['grid']['units']+"$)")
        if plasm and not fil2:
            ax1.fill_between(xx,data,0,alpha=0.4,color=col)
        if fil2:
            (data2, info2) = read_grid(fil2[itr])
            fils.append(fil2[itr])
            ax2=ax1.twinx()

            # sample spacing
            y2  = data2
            yf2 = scipy.fftpack.fft(y2)

            ymin2=min(yf2)
            ymax2=max(yf2)
            ylimin2=ymin2-(ymax2-ymin2)/10
            ylimax2=ymax2+(ymax2-ymin2)/10

            if ylimin2<ylims[1][0]:
                ylims[1][0]=ylimin2
            
            if ylimax2>ylims[1][1]:
                ylims[1][1]=ylimax2

            ax2.set_xlim(xlims)
            ax2.set_ylim(ylims[1])
            ax2.set_xlabel("$"+info2['grid']['axis'][0]['label']+"$"+" ($"+info2['grid']['axis'][0]['units']+"$)")
            ax2.set_ylabel("$"+info2['grid']['label']+"$"+" ($"+info2['grid']['units']+"$)")
            ax2.plot(xf,2.0/N * np.abs(yf2[:int(N//2)]),c=col2,lw=3.5,label="$"+info2['grid']['label']+"$")
            ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))
            if plasm:
                ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
            #lin[0].set_data(xx,data)
            #lin[1].set_data(xx,data)
        
            #xx=np.transpose(xx)
            #print(xx,len(xx))
            ax1.legend(loc=0)
        plt.show()
        
    sldr=widgets.IntSlider(value=0,min=0,max=len(filelst1)-1,step=1,orientation='horizontal',readout=False,description=' ')
    buuuu=widgets.Button()
    return interact(pltstf,itr=sldr,fil=fixed(filelst1),fil2=fixed(filelst2),col=fixed(col1),col2=fixed(col2),xlims=fixed(xlims),ylims=fixed(ylim),fils=fixed(fils),plasm=fixed(plas))


def FT_shower(tab,yl,fils):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value

    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value
    
    xlims=[i.value for i in   tab.children[2].children[4].children]

    pl2=tab.children[2].children[2].children[2].value
    
    if bool1 and bool2:
        fldr=tab.children[0].children[1].children[0].value
        key=tab.children[0].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    
        fldr=tab.children[1].children[1].children[0].value
        key=tab.children[1].children[1].children[1].value
        files2 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        return [FT_pltinteract(files,col1,files2,col2,xlims,yl,fils,plas=pl2),files,files2]
    
    elif bool1 or bool2:    
        fldr=tab.children[int(bool2)].children[1].children[0].value
        key=tab.children[int(bool2)].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]

        col=tab.children[2].children[int(bool2)+1].children[1].value
        return [FT_pltinteract(files,col,xlims=xlims,ylim=yl,fils=fils,plas=(pl2 and bool2)),files]

    
def filer(tab):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value

    xlims=[i.value for i in   tab.children[2].children[4].children]
    
    if bool1 and bool2:
        fldr=tab.children[0].children[1].children[0].value
        key=tab.children[0].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    
        fldr=tab.children[1].children[1].children[0].value
        key=tab.children[1].children[1].children[1].value
        files2 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    
        return [files,files2,xlims]
    
    elif bool1 or bool2:    
        fldr=tab.children[int(bool2)].children[1].children[0].value
        key=tab.children[int(bool2)].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        return [files,None,xlims]


def animate(tab,yl):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value
    
    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value
    
    xlims=[i.value for i in   tab.children[2].children[4].children]

    
    fil=filer(tab)
    
    pl2=tab.children[2].children[2].children[2].value
    # First set up the figure, the axis, and the plot element we want to animate
    fig=plt.figure(figsize=(16,9), dpi= 120, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')

    if bool2 and bool1:
        (data, info) = read_grid(fil[0][0])
        xmin=info['grid']['axis'][0]['min']
        xmax=info['grid']['axis'][0]['max']
        npts=info['grid']['nx'][0]
        xx=np.linspace(xmin,xmax,npts)

        (data2, info2) = read_grid(fil[1][0])
        
        ax = plt.axes()
        ax.set_xlim(xlims)
        ax.set_ylim(yl[0])
        ax2 = ax.twinx()
        ax2.set_ylim(yl[1])
        lines =[]
        lines.append(ax.plot([], [], lw=2,c=col1,label="$"+info['grid']['label']+"$")[0])
        lines.append(ax2.plot([], [], lw=2,c=col2,label="$"+info2['grid']['label']+"$")[0])
        ax.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
        ax.set_xlabel("$"+info['grid']['axis'][0]['label']+"$"+" ($"+info['grid']['axis'][0]['units']+"$)")
        ax.set_ylabel("$"+info['grid']['label']+"$"+" ($"+info['grid']['units']+"$)")
        ax2.set_ylabel("$"+info2['grid']['label']+"$"+" ($"+info2['grid']['units']+"$)")
        ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))
        if pl2:
            ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
        ax.legend(loc=0)
        
    elif bool1 or bool2:
        (data, info) = read_grid(fil[bool2][0])
        xmin=info['grid']['axis'][0]['min']
        xmax=info['grid']['axis'][0]['max']
        npts=info['grid']['nx'][0]
        xx=np.linspace(xmin,xmax,npts)

        ax = plt.axes(xlim=xlims, ylim=yl[bool2])
        lines =[]
        lines.append(ax.plot([], [], lw=2)[0])
        ax.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
        ax.set_xlabel("$"+info['grid']['axis'][0]['label']+"$"+" ($"+info['grid']['axis'][0]['units']+"$)")
        ax.set_ylabel("$"+info['grid']['label']+"$"+" ($"+info['grid']['units']+"$)")
        if pl2 and not bool1:
            ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
        
    # initialization function: plot the background of each frame
    def init():
        for line in lines:
            line.set_data([], [])
        ax.set_xlim(xlims)
        return lines

    # animation function.  This is called sequentially
    def animate(i,files):
        if bool1 and bool2:
            (data, info) = read_grid(files[0][i])
            ax.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
            x = xx
            y = data            
            lines[0].set_data(x, y)
            (data, info) = read_grid(files[1][i])
            y = data            
            lines[1].set_data(x,y)
        elif bool1 or bool2:
            (data, info) = read_grid(files[bool2][i])
            ax.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
            x = xx
            y = data            
            lines[0].set_data(x, y)
            
        return lines

    # call the animator.  blit=True means only re-draw the parts that have changed.
    return animation.FuncAnimation(fig, animate,fargs=(fil,), init_func=init,
                               frames=len(fil[0]), interval=20, blit=True)



def skin_depth(tabod,filsod,omega):
    xlims=[i.value for i in   tabod.children[2].children[4].children]
    col1=tabod.children[2].children[1].children[1].value
    col2=tabod.children[2].children[2].children[1].value

    file2=filsod[0]
    file1=filsod[1]

    (dens,info1) = read_grid(file1)

    xmin=info1['grid']['axis'][0]['min']
    xmax=info1['grid']['axis'][0]['max']
    npts=info1['grid']['nx'][0]
    xx=np.linspace(xmin,xmax,npts)
    ipl=0
    for it,i in enumerate(dens[1:]):
        if i<0:
            ipl=it
            break

    (efld,info2) = read_grid(file2)

    refpl=int(ipl+1/np.sqrt(1-omega**2)/((xmax-xmin)/npts))
    print(efld[refpl])

    print(efld[ipl]/efld[refpl])

    plt.clf()
    plt.cla()
    plt.close()

    fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    ax1=plt.gca()

    #lines.append(ax1.plot(xx,data,'+',c="coral")[0]);
    ax1.set_title("$n$="+str(info1['iteration']['n'])+", $t$="+'{:.2f}'.format(info1['iteration']['t'])+"$("+info1['iteration']['tunits']+")$",loc='right')
    #ax1.set_xlim([xmin,xmax])
    ax1.set_xlabel("$"+info1['grid']['axis'][0]['label']+"$ ($"+info1['grid']['axis'][0]['units']+"$)")
    ax1.set_ylabel("$"+info2['grid']['label']+"$"+" ($"+info2['grid']['units']+"$)")


    ax1.plot(xx,efld,'-',c=col1,lw=2.5,ms=10,label="$"+info2['grid']['label']+"$");
    ax1.legend()
    ax2=ax1.twinx()

    ax2.set_xlim(xlims)
    #ax2.set_xlabel("$"+info2['grid']['axis'][0]['label']+"$"+" ($"+info2['grid']['axis'][0]['units']+"$)")
    ax2.set_ylabel("$"+info1['grid']['label']+"$"+" ($"+info1['grid']['units']+"$)")
    ax2.plot(xx,dens,c=col2,lw=2.5,label="$"+info1['grid']['label']+"$")
    ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))


    #ax3=plt.gca()
    #ax3.plot(np.array(datavec[0])-par[1], Emfun(datavec[0], *par), 'b-', label='fit')[0]
    plt.axvline(x=xx[refpl],color='k')

    #plt.legend()
    plt.show()




def regselect(itr2,itr,minx,maxx,file1,file2,dist,bar1,bar2):

    (dens,info) = read_grid(file1)
    
    #build the x axis array
    xmin=info['grid']['axis'][0]['min']
    xmax=info['grid']['axis'][0]['max']
    npts=info['grid']['nx'][0]
    xx=np.linspace(xmin,xmax,npts)

    xmin=minx
    xmax=maxx
    #read the data from file2 (EMF file)
    (data, info) = read_grid(file2)
    bar1.min=minx
    bar1.max=maxx
    bar2.min=minx
    bar2.max=maxx
    
    #initialize the figure
    fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    ax1=plt.gca()
    #print([xmin,xmax])
    ax1.set_xlim([xmin,xmax])
    ax1.plot(xx,data,'-',c="darkorange",lw=2.5,label="$"+info['grid']['label']+"$");
    
    ax1.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
    #ax1.set_xlim([xmin,maxx])
    ax1.set_xlabel("$"+info['grid']['axis'][0]['label']+"$"+" ($"+info['grid']['axis'][0]['units']+"$)")
    ax1.set_ylabel("$"+info['grid']['label']+"$"+" ($"+info['grid']['units']+"$)")
    
    #plot the trasition region
    plt.axvline(x=itr,color='k')
    plt.axvline(x=itr2,color='k')

    dist.value=str(abs(itr-itr2))
    
    
    plt.show()
    
def kcalculator(file1,file2):
    a=widgets.Text(description="peak distance")
    b=widgets.FloatText(description="nr of peaks")
    c=widgets.FloatText(description="$k (c/\omega_p)$")

    bb=widgets.Button(description="Calculate k:")

    def on_click(asd):
        #check for windows (bars are different)
        c.value=2*np.pi*b.value/float(a.value)

    bb.on_click(on_click)
    (data0,inf0)=read_grid(file1)

    #build the x axis array
    xmin=inf0['grid']['axis'][0]['min']
    xmax=inf0['grid']['axis'][0]['max']
    npts=inf0['grid']['nx'][0]
    xx=np.linspace(xmin,xmax,npts)


    itl=0
    for i, xi in enumerate(data0[1:]): 
        if xi < 0:   #check charge value!!!!!!!!!!#
            itl=i
            break
    xmin=xx[itl]


    sldr=widgets.FloatSlider(value=(xmin),min=xmin,max=xmax,orientation='horizontal',description='xmin:')
    sldr1=widgets.FloatSlider(value=(xmax),min=xmin,max=xmax,orientation='horizontal',description='xmax:')
    
    sldr2=widgets.FloatSlider(value=xmin-(xmin-xmax)/3,min=xmin,max=xmax,orientation='horizontal',description='bar 1:',step=0.005)
    sldr3=widgets.FloatSlider(value=xmin-2*(xmin-xmax)/3,min=xmin,max=xmax,orientation='horizontal',description='bar 2:',step=0.005)
    
    display(interact(regselect,minx=sldr,maxx=sldr1,itr2=sldr3,itr=sldr2,file1=fixed(file1),file2=fixed(file2),dist=fixed(a),bar1=fixed(sldr2),bar2=fixed(sldr3)),a,b,bb,c)


