//#include "simulation.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

//#include "emf.h"

void Bf(float* bvec,float x)
{
    if(x>0)
        {bvec[0]=0.1;}
    else
        {bvec[0]=0;}
    bvec[1]=0;
    bvec[2]=0;
}

float custom_n0( float x ) {
    double n;
    if(x<65)
        n=0;
    else if(x<75)
        n=1.-(75.-x)/10.;
    else
        n=1;
    return n;
    
}

void sim_init( t_simulation* sim )
{
  // Time step
  float dt = 0.018570;
  float tmax = 200.;

  // Simulation box
  int   nx  = 3500;
  float box = 65.0;
	
  // Diagnostic frequency
  int ndump = 25;

  // Initialize particles
  const int n_species = 1;
	
  // Use 128 particles per cell
  int ppc = 256;

  // Density profile
   // t_density density = { .type = UNIFORM };
  t_density density = { .type = STEP, .start = 63 };
  //	t_density density = { .type = SLAB, .start = 17.5, .end = 22.5 };
  //	t_density density = { .type = RAMP, .start = 65, .end = 75, .ramp = {0.0, 1.} };
  	//t_density density = { .type = CUSTOM, .custom = &custom_n0 };

  t_species* species = (t_species *) malloc( n_species * sizeof( t_species ));
  spec_new( &species[0], "electrons", -1.0, ppc, NULL, NULL, nx, box, dt, &density );
  // Initialize Simulation data
  sim_new( sim, nx, box, dt, tmax, ndump, species, n_species );

  // Add laser pulse (this must come after sim_new)
  t_emf_laser laser = {	  
    .start =62.0,
    .fall  = 30,
    .rise  = 30 ,
    .flat  =0,
    .a0 = 1e-8,
    .omega0 = 6,
    .polarization = M_PI_2
  };


    // Add laser pulse (this must come after sim_new)
  /* t_emf_laser laser2 = {	  
    .start =45.0,
    .fall  = 20,
    .rise  = 20 ,
    .flat  =0,
    .a0 = 0.0001,
    .omega0 = 6,
    .polarization = M_PI_2
  };*/
    sim_add_cmag(sim, Bf);
    sim_add_laser( sim, &laser );
    //sim_add_laser( sim, &laser2 );

  sim -> emf.bc_type = EMF_BC_OPEN;
  //sim -> emf.bc_type = EMF_BC_OPEN;

	
  //Set moving window (this must come after sim_new)
	sim_set_moving_window( sim );
}


void sim_report( t_simulation* sim ){

  	// All electric field components
  //	emf_report( &sim->emf, EFLD, 0 );
	emf_report( &sim->emf, EFLD, 2 );
    emf_report( &sim->emf, EFLD, 1 );

	// Charge density
	spec_report( &sim->species[0], CHARGE, NULL, NULL );

	// RAW dump
	//	spec_report( &sim->species[0], PARTICLES, NULL, NULL );
			
}
