
\documentclass[10pt,a4paper]{article}
\usepackage{ritaconfig}

%----------------------------------------------------------------------------------------
%       TITLE SECTION
%----------------------------------------------------------------------------------------
\title{\vspace{-20mm}\fontsize{18pt}{10pt}\textbf{Electromagnetic wave absorption by a plasma ramp}} % Article title
\author{\normalsize \textbf{The ZPIC Collection of plasma physics simulations} (\today) % Your institution
%\normalsize \href{mailto:john@smith.com}{john@smith.com} % Your email address
\vspace{-125mm}
}
\date{}

%----------------------------------------------------------------------------------------

\graphicspath{ {./img/} }

\begin{document}

%\begin{adjustwidth}{-27pt}{-27pt}
\maketitle % Insert title
%\end{adjustwidth}
\vspace{-50pt}

\section{Introduction}
When an electromagnetic wave reaches a plasma with an certain electron density $n_e$,
two possible scenarios may occur:
it can kepp propagating normally, or it can fade away exponentially until a given length.
The dispersion relation is given by $c^2k^2=\omega^2-\omega_p^2$, this way it is
possible to find the two different situations:
\begin{itemize}
\item $\omega^2\geq\omega_p^2\Rightarrow k^2\geq 0\Rightarrow$ $k$ is real, so
  the wave may propagate  with an oscilatory term ($\propto\exp{(ikr)}$).

\item $\omega^2\leq\omega_p^2\Rightarrow k^2\leq 0\Rightarrow$ $k$ is imaginary,
  therefore, the wave may propagate in space with an exponential attenuation term ($\propto\exp{(-kr)}$). The typical legth for the propagation of an attenuated wave is the so called \textit{skin depth}:
  \end{itemize}

\begin{equation}
\delta=k^{-1}=\frac{c}{\sqrt{\omega_{pe}^2-\omega^2}}
\end{equation}

If, for example the the plasma density is not constant (e.g increasing density and $\omega_{pe}$) the 
wave may propagate normally up until a certain point (the critical density) and then stop propagating. 
As the wave does not propagate inside into tthe plasma, it gets reflected, and its
interference with the incoming wave will produce a stationary wave 
as shown in the next figure:

\begin{wrapfigure}{l}{0.55\textwidth}
  \includegraphics[width=0.55\textwidth]{ex.pdf}
  \caption{Example of a flat top monochromatic laser hitting a plasma ramp. The critical density is located at $x=$30 $c/\omega_p$.}
\label{fig:examp}
\end{wrapfigure}

For a non-collisional plasma, with linearly growing density $n_e=xn_c/L$, the shape of the wave shown in Figure \ref{fig:examp} is described by an Airy function. If we introduce the variable $\eta=[\omega^2/(c^2L)]^{1/3}(x-L)$, we can write the wave equation as:

\begin{equation}
\frac{\partial^2E}{\partial \eta^2}-\eta E=0
\end{equation}

The solution to the above equation is an Airy function, given by:

\begin{equation}
 Ai(\eta)={\frac{1}{\pi}}{\int_0^\infty}\cos{\left({ \frac{t^3}{3}}-\eta t\right)}dt 
\end{equation}

The Airy Function can be approximated by more treatable functions like 
cosines (for the left part) and exponentials (for the right part), however, the  transition 
region is poorly approximated by any of these functions as you can see below:
%<img src="Mplwp_airyai_asymptotic.svg"  style="width: 50%; float: left;" />	

\begin{figure}[H]
  \begin{minipage}{.5\textwidth}
\begin{align*}
Ai(\eta)&\simeq \frac{1}{(\eta\pi^2)^{1/4}}\exp{\left(-\frac{2}{3}(\eta)^{3/2}\right)}, \;\;\text{for}\;\eta\gg 1\\
Ai(\eta)&\simeq\frac{1}{(-\eta\pi^{2})^{1/4}}\cos{\left(\frac{2}{3}(-\eta)^{3/2}-\frac{\pi}{4}\right)}, \;\;\text{for}\;\eta\ll -1
\end{align*}

    %\begin{align}
     %   Ai(\eta)\simeq\frac{1}{(-\eta\pi^{2})^{1/4}}\cos{\left(\frac{2}{3}(-\eta)^{3/2}-\frac{\pi}{4}\right)}, \;\;\text{for}\;\eta\ll -1
     %   Ai(\eta)\simeq \frac{1}{(\eta\pi^2)^{1/4}\exp{\left(-\frac{2}{3}(\eta)^{3/2}\right)}, \;\;\text{for}\;\eta\gg 1
     %   \end{align}
  \end{minipage}%
  \begin{minipage}{.5\textwidth}
    \centering
      \includegraphics[width=0.85\textwidth]{image.pdf}
  \caption{Airy function and its approximations.}
\label{fig:air}
      \end{minipage}
\end{figure}

\section{Some Results}

For this simulation you will need:

\begin{itemize}
  \item A monochromatic flat-top laser, long enough to achieve stationary regime.
  \item A plasma with a linear density profile.
  \item A long enough simulation box, on which both the plasma and the laser can fit.
  \end{itemize}

In order to achieve decent agreement between your simulation results and the backing theory,
 you will need to use a good enough resolution as the Airy Functions depend quite critically 
 on some parameters that are affected by resolution, like the slope of the plasma ramp and the
frequency of the laser.

For the example given below in Figure \ref{fig:comp} the following parameters were used:

\begin{table}[!htb]
    \begin{minipage}[t]{.33\linewidth}
      \centering
      \caption{Simulation box parameters}
      \label{tab:box}
      \begin{tabular}{|l|l|}
        \hline
        Param & Value \\ \hline\hline
        \texttt{dt}    &  0.028 $\omega_p^{-1}$     \\ \hline
        \texttt{tmax}  &  170  $\omega_p^{-1}$   \\ \hline
        \texttt{box}   &  225  $c/\omega_p$   \\ \hline
        \texttt{nx}    &  8000     \\ \hline
        \texttt{ppc}   &  8     \\ \hline
      \end{tabular}
    \end{minipage}%
    \begin{minipage}[t]{.33\linewidth}
      \centering
            \caption{Laser parameters}
      \label{tab:laser}
      \begin{tabular}{|l|l|}
        \hline
        Param & Value \\ \hline\hline
        \texttt{start}  &   175  $c/\omega_p$  \\ \hline
        \texttt{rise}    &  25   $c/\omega_p$  \\ \hline
        \texttt{flat}  &   145  $c/\omega_p$  \\ \hline
        \texttt{fall}   &   5  $c/\omega_p$  \\ \hline
        \texttt{a0}    &    1$\times10^{-6}$ $m_ec\omega_p^{-2}$ \\ \hline
        \texttt{omega}   &   6  $\omega_{p}$  \\ \hline
        \texttt{polarization}   &  $\pi/2$ \SI{}{\radian}     \\ \hline
      \end{tabular}
    \end{minipage}
        \begin{minipage}[t]{.33\linewidth}
      \centering
      \caption{Density profile parameters}
      \label{tab:density}
      \begin{tabular}{|l|l|}
        \hline
        Param & Value \\ \hline\hline
        \texttt{start}    & 180  $c/\omega_p$    \\ \hline
        \texttt{end}  &    305   $c/\omega_p$\\ \hline
        \texttt{start\_val}   & 0      \\ \hline
        \texttt{end\_val}    &  150     \\ \hline
      \end{tabular}
    \end{minipage} 
\end{table}

\begin{table}[ht]
\begin{minipage}[b]{0.3\linewidth}
\centering
\begin{tabular}{|l|r|r|}
\hline
\multirow{2}{*}{Param} & \multicolumn{2}{c|}{Value from} \\ \cline{2-3} 
                  &     Sim.      & Fit          \\ \hline \hline
            $\omega_0 (\omega_{pe})$      &      6.000     &  6.011        \\ \hline
            $slope$      &    1.200       &        1.207   \\ \hline
\end{tabular}
   \caption{Simulated vs input parameters}
    \label{table:student}
\end{minipage}\hfill
\begin{minipage}[b]{0.7\linewidth}
    \centering
      \includegraphics[width=1\textwidth]{dcompmedr.pdf}
      \caption{Comparison between the the simulated results and the theoretical ones}
      \label{fig:comp}
\end{minipage}
\end{table}

\section{Additional suggestions}
It is possible to verify if the position of
the critical point is influenced  by changing the frequency of the incoming wave.
Try to find $L$'s dependence on $\omega$, both by theory and by simulation.

\begin{thebibliography}{99}
\bibitem{mora}
Mora, P., 2008, \textit{Introduction aux plasmas cr\'{e}e\'{e}s par laser}, Ecole Polytechnique.
\end{thebibliography}


%$ Ai(\eta)\simeq{\large \frac{1}{\pi^{1/2}(-\eta)^{1/4}}}\cos{\left({{\large\frac{2}{3}}(-\eta)^{3/2}}-{\large\frac{\pi}{4}}\right)}dt, \;\;\text{for}\;\eta\ll -1$ 


%$ Ai(\eta)\simeq{\large \frac{1}{\pi^{1/2}(\eta)^{1/4}}}\exp{\left({{\large-\frac{2}{3}}(\eta)^{3/2}}\right)}dt, \;\;\text{for}\;\eta\gg 1$ 



\begin{multicols}{2} % Two-column layout throughout the main article text


\end{multicols}
\end{document}
