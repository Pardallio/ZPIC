"""
This python file contains the theorical trajectories for the main known drifts
that occur in a plasma.

"""
import operator
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

class sys_params(object):
    def __init__(self,m=0,q=0):
        self.m=m;
        self.q=q;
    def setm(self,m):
        self.m=m;
    def setq(self,q):
        self.q=q;

    def getmq(self):
        return m,q  

def project(a,b):
    a=np.dot(a,b)/np.dot(b,b)
    return [i * a for i in b]
def reject(a,b):
    """ reject a from b """
    return list(map(operator.sub, a, project(a,b)))


    
def ExB(qi,tf,ht,E,B,m,q):
    ri=qi[:3]
    vi=qi[3:]
    
    #project the initial conditions on the new axis
    vpar0=np.linalg.norm(project(vi,B))
    B0=np.sqrt(np.dot(B,B))  #magnitude of magnetic field
    b=[i/B0 for i in B]
    Epar=project(E,B)
    Ex=reject(E,B)
    Exmod=np.linalg.norm(Ex)
    Eparmod=np.linalg.norm(Epar)
    eperpx=[i / np.sqrt(np.dot(Ex,Ex)) for i in Ex]
    Eperpy=np.cross(Ex,B)
    eperpy=[i/np.sqrt(np.dot(Eperpy,Eperpy)) for i in Eperpy]

    vperpxi=np.linalg.norm(project(vi,eperpx))
    vperpyi=np.linalg.norm(project(vi,eperpy))

    xperpi=np.linalg.norm(project(ri,eperpx))
    yperpi=np.linalg.norm(project(ri,eperpy))
    zi=np.linalg.norm(project(ri,B))

    #get scale factors
    wc=q*B0/m
    vd=Exmod/B0
               
    #obtain parameters from initial conditions
    C1=vperpxi
    C2=vperpyi+vd
    C3=vpar0
    C4=xperpi+C2/wc
    C5=yperpi-C1/wc
    C6=zi

    def r(t):                                         #t->t*wc
        x=(-C2*np.cos(t)+C1*np.sin(t))/vd+C4               #x->x*wc/vd
        y=t+C5+(C1*np.cos(t)+C2*np.sin(t))/vd      #y->y*wc/vd
        z=(Eparmod*q*t*t/(2*m)+C3*t+C6)                   #z->z*wc/vd
        
        #print(x,y,z)
        
        xac=z*b[0]+x*eperpx[0]+y*eperpy[0]
        yac=z*b[1]+x*eperpx[1]+y*eperpy[1]
        zac=z*b[2]+x*eperpx[2]+y*eperpy[2]
        return [xac*wc/vd,yac*wc/vd,zac*wc/vd]

    tt=0
    tl=[]
    i=0
    rp=[]
    tf=tf/wc
    ht=ht/wc
    
    while tt<tf:
        rp.append(r(tt))
        tl.append(tt)
        i=i+1
        tt=i*ht
    

    print(vd)

    return tl,rp


#def r(t):                                         #t->t*wc
#    x=(-C2*np.cos(t*wc)+C1*np.sin(t*wc))/wc+C4               #x->x*wc/vd
#        y=vd*t+C5+(C1*np.cos(t*wc)+C2*np.sin(t*wc))/wc      #y->y*wc/vd
#        z=(Eparmod*q*t*t/(2*m)+C3*t+C6)                   #z->z*wc/vd
#
#
#        xac=z*b[0]+x*eperpx[0]+y*eperpy[0]
#        yac=z*b[1]+x*eperpx[1]+y*eperpy[1]
#        zac=z*b[2]+x*eperpx[2]+y*eperpy[2]
#        return [xac*wc/vd,yac*wc/vd,zac*wc/vd]
#    tt=0
#tl=[]
    #i=0
    #rp=[]
    ##tf=tf/wc
#ht=ht/wc




import matplotlib.pyplot as plt
fig = plt.figure()
ax=fig.gca(projection='3d')
#ax = fig.add_subplot(111)
#fig.subplots_adjust(top=0.85)
#ax.set_title('Some drift')

ax.set_xlabel('x ($\omega_c/v_d$)')
ax.set_ylabel('y ($\omega_c/v_d$)')
ax.set_zlabel('z ($\omega_c/v_d$)')

E=(0.1,0,0)
B=(0,0,2)
qi=(0,0,0,1,0,0.1)
t,r=ExB(qi,3*6.28,0.1,E,B,1,1)

r=np.transpose(r)
ax.plot(r[0],r[1],r[2],lw=0.5)
ax.scatter(r[0,0],r[1,0],r[2,0],c='red')

#print(r)
plt.save('drft.pdf')
