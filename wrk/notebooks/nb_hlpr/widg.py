import os
import ipywidgets as widgets
from ipywidgets import interact, interactive, fixed, interact_manual,Layout

import numpy as np

import matplotlib
from matplotlib import pyplot as plt
from matplotlib import animation

from zdf import read
import matplotlib.patches as patches
import scipy.fftpack

#from visual import *
from tkinter import filedialog
from tkinter import *

import visual
if os.name == 'nt':
    bar="\\"
else:
    bar="/"



#an ugly file browser (sorry...)
class FileBrowser(object):
    def __init__(self):
        self.path = os.getcwd()
        self._update_files()
        self.i=0
    def _update_files(self):
        self.files = list()
        self.dirs = list()
        if os.name == 'nt':
            bar="\\"
        else:
            bar="/"        
        if(os.path.isdir(self.path)):
            for f in os.listdir(self.path):
                ff = self.path + bar + f
                if os.path.isdir(ff):
                    self.dirs.append(f)
                else:
                    if ".zdf" in ff:
                        self.files.append(f)

    def widget(self):

        def on_click(b): 
            root = Tk()
            root.filename =  filedialog.askdirectory(initialdir = "")
            if root.filename:
                self.path=root.filename
            #print(root.filename)
            self._update()
            root.destroy()

        
        vmbox=widgets.VBox(layout=Layout(width='100%'))
        self.vvb=widgets.VBox()
        button = widgets.Button(description="Choose data folder",layout=Layout(width="",margin="1px 0px 1px 0px"), background_color='#d0d0ff',tooltip="Open a file chooser window")
        #button.style.button_color="LEMONCHIFFON"
        button.on_click(on_click)
                
        vmbox.children= tuple([button,self.vvb])
        self._update()
        
        return vmbox

    
    def _update(self):
        self._update_files()
        if self.files:
            vaal=self.files[0].split('-')[0]
        else:
            vaal=None
        #        box.children = tuple(buttons)
        self.vvb.children=tuple([self.data_folder(),self.data_type(vaal)])

            
    def data_folder(self):
        self.DF=widgets.Text(value=self.path,placeholder='Enter Data Folder Path (the folder where you have your data files)',description='Data Folder:',layout=Layout(width='75%'),disabled=False)
        return self.DF

    def data_type(self,val):
        self.DK=widgets.Text(value=val,placeholder='Enter the key of the data files (e.g. "charge")',description='Data Key:',layout=Layout(width='75%'),disabled=False)
        return self.DK

    def filelist(self):
        return [self.DF.value+bar+i for i in os.listdir(path) if os.path.isfile(os.path.join(path,i)) and self.DK.value in i]


def tabmaker():
    f = FileBrowser()
    f2= FileBrowser()
    
    w0=widgets.Label("Select which data sets you want to plot",layout=Layout(flex='1 1 auto',width='auto'))

    w1=widgets.Checkbox(
        value=True,
        description='Data Set 1',
        disabled=False
    )

    wcp1=widgets.ColorPicker(
        concise=False,
        description='line color',
        value="darkorange",
        disabled=False
    )

    hb1=widgets.HBox([w1,wcp1])
    
    w2=widgets.Checkbox(
        value=False,
        description='Data Set 2',
        disabled=False
    )


    wcp2=widgets.ColorPicker(
        concise=False,
        description='line color',
        value="seagreen",
        disabled=False
    )

    wp2=widgets.Checkbox(
        value=False,
        description='Plasma:',
        disabled=False
    )

    wmw2=widgets.Checkbox(
        value=False,
        description='Moving Window:',
        disabled=False
    )

    
    w02=widgets.Label("Select the x-axis range",layout=Layout(flex='1 1 auto',width='auto'))

    wxl=widgets.FloatText(
        description='lower limit:',
        value=0,
        disabled=False
    )

    wxu=widgets.FloatText(
        description='upper limit:',
        value=100,
        disabled=False
    )

    hb3=widgets.HBox([wxl,wxu])

    
    hb2=widgets.HBox([w2,wcp2,wp2])
    children = [f.widget(),f2.widget(),widgets.VBox([w0,hb1,hb2,wmw2,w02,hb3])]
    tab = widgets.Tab()
    tab.children = children
    tab.set_title(0, "Data Set 1")
    tab.set_title(1, "Data Set 2")
    tab.set_title(2, "Settings")
    return tab


def shower(tab,yl,fils):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value

    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value

    mw=tab.children[2].children[3].value
    xlims=[i.value for i in   tab.children[2].children[5].children]

    pl2=tab.children[2].children[2].children[2].value
    
    if bool1 and bool2:
        fldr=tab.children[0].children[1].children[0].value
        key=tab.children[0].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    
        fldr=tab.children[1].children[1].children[0].value
        key=tab.children[1].children[1].children[1].value
        files2 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        return [visual.pltinteract(files,col1,files2,col2,xlims,yl,fils,plas=pl2,mw=mw),files,files2]
    
    elif bool1 or bool2:    
        fldr=tab.children[int(bool2)].children[1].children[0].value
        key=tab.children[int(bool2)].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]

        col=tab.children[2].children[int(bool2)+1].children[1].value
        return [visual.pltinteract(files,col,xlims=xlims,ylim=yl,fils=fils,plas=(pl2 and bool2),mw=mw),files]



def FT_shower(tab,yl,fils):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value

    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value
    
    xlims=[i.value for i in   tab.children[2].children[5].children]

    
    if bool1 and bool2:
        fldr=tab.children[0].children[1].children[0].value
        key=tab.children[0].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    
        fldr=tab.children[1].children[1].children[0].value
        key=tab.children[1].children[1].children[1].value
        files2 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        return [visual.FT_pltinteract(files,col1,files2,col2,xlims,yl,fils)]
    
    elif bool1 or bool2:    
        fldr=tab.children[int(bool2)].children[1].children[0].value
        key=tab.children[int(bool2)].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]

        col=tab.children[2].children[int(bool2)+1].children[1].value
        return [visual.FT_pltinteract(files,col,xlims=xlims,ylim=yl,fils=fils)]



#Show a heat map with the EM wave!
#X---> X
#Y---> Time

from matplotlib import ticker
def HeatMap_show(tabsd,om):
    files=visual.filer(tabsd)[0]
    x=[]
    t=[]
    E=[]
    #print(files)
    #Read the first file
    (data, info) = read(files[0])
    #build the x-axis array
    xmin=info.grid.axis[0].min
    xmax=info.grid.axis[0].max
    npts=info.grid.nx[0]
    xx=np.linspace(xmin,xmax,npts)

    #build the x,T,E list
    for fil in files[0::1]:
        #Read the files chosen by the user
        (data, info) = read(fil)
        for it,xi in enumerate(xx[0::1]):
            t.append(info.iteration.t)
            E.append(data)
            #print(data[it])
    #convert all values into matrix style
    x=xx
    t=np.unique(t)
    Ef=np.array(E)

    def hmplt(bar,xbas):
        fig=plt.figure(figsize=(11, 9))
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')
        ax1=fig.gca()
        #ax1.set_ylim([T[0,0],T[-1,0]])
        #ax1.set_xlim(xrg)

        #acutally plot stuff
        pax=ax1.imshow(Ef,cmap="RdBu",extent=(x[0],x[-1],t[0],t[-1]),aspect="auto",origin="lower")
        sfmt=ticker.ScalarFormatter(useMathText=True)
        sfmt.set_powerlimits((0, 0))
        cbar = fig.colorbar(pax,format=sfmt)
        cbar.set_label("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")

        m=1/(np.sqrt(1+1/(om**2-1))-1)
        dx=t[-1]/m
        ax1.set_xlabel("$"+info.grid.axis[0].label+"-ct$"+" [$"+info.grid.axis[0].units+"$]")
        ax1.set_ylabel("$t$"+" $["+info.iteration.tunits+"]$")

        if(bar):
            ax1.plot([xbas,xbas+dx],[0,t[-1]],c='k',lw=10)
            ax1.plot([xbas,xbas+dx],[0,t[-1]],c='lime',lw=5)

        plt.show()
    show_bar=widgets.Checkbox(value=True,description='Show slope:')    
    xbas_sldr=widgets.FloatSlider(value=xmax/2,max=xmax,min=0,orientation='horizontal',description='$x_1$')
    display(interact(hmplt,bar=show_bar,xbas=xbas_sldr))

