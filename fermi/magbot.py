from ipywidgets import widgets,interact,fixed
from IPython.display import display
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation
from scipy.integrate import odeint #probably needs to be installed as well
import loopfield_mp as lf
from pylab import *
from Boris import *
import copy
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

class coils(object):
    def __init__(self,p=0,d=2,r=1,curr=1):
        self.p1=p
        self.p2=p+d
        self.p20=p+d
        self.r=r
        self.curr=curr
        # create empty field with specified units 
        self.field = lf.Field(length_units = lf.m,
        current_units = lf.uA,
        field_units = lf.uT)
        self.v=0

    def set_coils(self,p=0,d=2,r=1,curr=1):    
        self.p1=p
        self.p2=p+d
        self.p20=p+d
        self.r=r
        self.curr=curr

    def coil_adv(self,dt):
        self.p2=self.p20+self.v*dt;
        self.set_Bfield();

    def set_Bfield(self):
        self.field = lf.Field(length_units = lf.cm, 
        current_units = lf.uA,
        field_units = lf.uT)     
        # single-turn 10 cm x-oriented coil at origin 
        position = [self.p1, self.r, self.r]
        normal = [1, 0., 0.] 
        radius = self.r*1000
        current = self.curr*300
        c = lf.Loop(position, normal, radius, current) 


        # add loop to field 
        self.field.addLoop(c);

        position = [self.p2, self.r, self.r] 
        normal = [1, 0., 0.] 
        radius = self.r 
        current = self.curr
        
        d = lf.Loop(position, normal, radius, current) 
        self.field.addLoop(d);
    
    def get_Bf(self,r,t):
        self.coil_adv(t)
        return self.field.evaluate(r)
        
    def get_flines(self,t):
        self.coil_adv(t)
        def flines(r,t,ds):
            Bx=self.get_Bf(r,t)[0]
            By=self.get_Bf(r,t)[1]
            Bz=self.get_Bf(r,t)[2]
            Bm=np.sqrt(Bx**2+By**2+Bz**2)
            if Bm==0:
                return np.array([0,0,0])
            
            return ds*self.get_Bf(r)/Bm

        def solve(ri,ds):
            np.array(ri)
            tf = 1000; NPts = 300
            t = np.linspace(0,tf,NPts)
            Qe = odeint(flines, ri, t, args=(ds,))
            return Qe

        basex=[(self.p1+self.p2)/2]#=list(np.arange(-0.5, 2.6, 1))
        basey=list(np.arange(0, 2*self.r+0.01, self.r))
        basez=list(np.arange(0, 2*self.r+0.01, self.r))
        set_bl=[]
        for xi in basex:
            for yi in basey:
                for zi in basez:
                    if yi==self.r and zi==self.r:
                        if self.curr<0:
                            xi=self.p2+self.r/2
                        else:
                            xi=self.p1-self.r/2                        
                    bl=solve((xi,yi,zi),0.1)
                    xi=basex[0]
                    bl=np.transpose(bl)                        
                    if yi==self.r and zi==self.r:
                        if self.curr<0:
                            bl[0]=[i if i>self.p1-self.r/2 else self.p1-self.r/2 for i in bl[0]]
                        else:
                            bl[0]=[i if i<self.p2+self.r/2 else self.p2+self.r/2 for i in bl[0]]
                            
                    set_bl.append(bl)
        self.setbl=copy.deepcopy(set_bl)
        return set_bl
    
    def calc_circ(self,t):
        self.coil_adv(t)
        r=self.r
        x1=self.p1
        x2=self.p2
        cur=self.curr
        
        self.cir=[]
        self.cir2=[]
        if cur<=0:
            a=-1
        else:
            a=1
        i=0
        while i<31:
            yr=r+r*np.sin(a*2*3.14*(i/30))
            xr=r+r*np.cos(a*2*3.14*(i/30))
            c1=[x1,xr,yr]
            c2=[x2,xr,yr]
            self.cir2.append(c1)
            self.cir.append(c2)
            i=i+1        

        self.cir=np.transpose(self.cir)
        self.cir2=np.transpose(self.cir2)    
        return self.cir,self.cir2
        

    def plot_flines(self,t):
        self.coil_adv(t)
        fig2 = plt.figure(dpi=80,figsize=(16, 12))
        ax2=fig2.gca(projection='3d')

        ax2.set_xlabel('x (cm)')
        ax2.set_ylabel('y (cm)')
        ax2.set_zlabel('z (cm)')


        cir=[]
        cir2=[]
        cir,cir2=self.calc_circ(t)
        #r=np.transpose(r)
        #print(r)

        ax2.plot(cir[0],cir[1],cir[2],lw=3.5,c='red')
        ax2.plot(cir2[0],cir2[1],cir2[2],lw=3.5,c='red')
        if self.curr != 0:
            cir=np.transpose(cir)
            cir2=np.transpose(cir2)    
            ra=cir[int(len(cir)/2)]
            rb=cir[int(len(cir)/2)+1]
            ar = Arrow3D([ra[0],rb[0]],[ra[1],rb[1]],[ra[2],rb[2]], mutation_scale=20,lw=2, arrowstyle="-|>", color="red")
            ax2.add_artist(ar)
            ra=cir2[int(len(cir2)/2)]
            rb=cir2[int(len(cir2)/2)+1]
            br = Arrow3D([ra[0],rb[0]],[ra[1],rb[1]],[ra[2],rb[2]], mutation_scale=20,lw=2, arrowstyle="-|>", color="red")
            ax2.add_artist(br)
            cir=np.transpose(cir)
            cir2=np.transpose(cir2)    
    


        ax2.set_xlim3d([self.p1-self.r/5, self.p2+self.r/5])

        ax2.set_ylim3d([-self.r/10, 2.1*self.r])

        ax2.set_zlim3d([-self.r/10,2.1*self.r ])
        set_bl=self.get_flines()
        for bli in set_bl:
            curr_bli=bli
            for jbli in np.arange(len(bli[1])):
                if  curr_bli[1][jbli]< -self.r/10:
                    curr_bli[1][jbli]= NaN
                elif curr_bli[1][jbli]> 2.1*self.r:   
                    curr_bli[1][jbli]= NaN        
                else:
                    pass
            for jbli in np.arange(len(bli[2])):
                if  curr_bli[2][jbli]< -self.r/10:
                    curr_bli[2][jbli]= NaN
                elif curr_bli[2][jbli]> 2.1*self.r:   
                    curr_bli[2][jbli]= NaN        
                else:
                    pass
            for jbli in np.arange(len(bli[0])):
                if  curr_bli[0][jbli]< -self.p1/10:
                    curr_bli[0][jbli]= NaN
                elif curr_bli[0][jbli]> self.p2*11/10:
                    curr_bli[0][jbli]= NaN        
                else:
                    pass

    
        
            ax2.plot(curr_bli[0],curr_bli[1],curr_bli[2],lw=2,c='seagreen',alpha=0.75)
            ra=[bli[0][2],bli[1][2],bli[2][2]]
            rb=[bli[0][3],bli[1][3],bli[2][3]]
            if bli[1][2]==bli[1][3] and bli[1][2]==self.r and bli[2][2]==bli[2][3] and bli[2][2]==self.r:
                ra=[(self.p1+self.p2)/2,bli[1][2],bli[2][2]]
                rb=[(self.p1+self.p2)/2-self.curr*(self.p1-self.p2)/100,bli[1][2],bli[2][2]]
            ar = Arrow3D([ra[0],rb[0]],[ra[1],rb[1]],[ra[2],rb[2]], mutation_scale=20,lw=2, arrowstyle="-|>", color="seagreen")
            ax2.add_artist(ar)

        #ax.scatter(bli[0,0],bli[1,0],bli[2,0],c='red')


        if self.curr>0:
            ax2.plot(cir[0][4:21],cir[1][4:21],cir[2][4:21],lw=3.5,c='red')
            ax2.plot(cir2[0][4:21],cir2[1][4:21],cir2[2][4:21],lw=3.5,c='red')
        else:
            ax2.plot(cir[0][11:26],cir[1][11:26],cir[2][11:26],lw=3.5,c='red')
            ax2.plot(cir2[0][11:26],cir2[1][11:26],cir2[2][11:26],lw=3.5,c='red')

    def parmag(self,t):
        self.coil_adv(t)
        x=np.arange(self.p1-(self.p2-self.p1)/2,self.p2+(self.p2-self.p1)/2,(self.p2-self.p1)/100)
        B=[self.get_Bf([xi,self.r,self.r],t)[0] for xi in x ]
        fig=plt.figure(figsize=(16, 9), dpi= 80, facecolor='w', edgecolor='k');
        matplotlib.rcParams.update({'font.size': 16})
        axes = plt.gca()
        axes.set_xlim([min(x),max(x)])
        axes.yaxis.get_major_formatter().set_powerlimits((0, 1))
        plt.ylabel('$B_\parallel$ ($\mu$T)')
        plt.xlabel('$x$ (cm)')
        plt.grid(alpha=0.6,linestyle='dashed')
        plt.title("Parallel magnetic field at the axis for I="+str(self.curr)+"mA")
        axes.plot(x,B,'-',lw=2);
        axes.plot(x,B,'.',c='mediumblue');



    def boris_calc(self,ri,vi,q,m,tf,dt):
        def Ef(r,t):
            Ex=0;
            Ey=0;
            Ez=0;
            return (Ex,Ey,Ez)
        
        def Bf(r,t):
            #Bx=-r[0]/2
            #By=-r[1]/2
            #Bz=(10+1*r[2]**2);
            return [bi*1e-6 for bi in self.get_Bf(r,t)]
        
        jose=particle(q,m)
        jose.setv(vi)
        jose.setr(ri)
        traj=trajectory(jose)
        traj.setEM(Ef,Bf)
        traj.solve(tf,dt)
        self.borisq=traj.getq()

#####
#FUNCTION FOR INTERACT
#####

def coilinter(coil1_pos,d,radius,fixed_scale,current,coil):
    matplotlib.rcParams.update({'font.size': 12})
    fig = plt.figure(dpi=80,figsize=(16, 12))
    ax=fig.gca(projection='3d')
    coil[0].set_coils(coil1_pos,d,radius,current)
    if fixed_scale:
        ax.set_xlim3d([0, 20])
        ax.set_ylim3d([0, 10])
        ax.set_zlim3d([0, 10])

    ax.set_xlabel('x (cm)')
    ax.set_ylabel('y (cm)')
    ax.set_zlabel('z (cm)')

    cir,cir2=coil[0].calc_circ(0)
    ax.plot(cir[0],cir[1],cir[2],lw=2.5,c='red')[0]
    ax.plot(cir2[0],cir2[1],cir2[2],lw=2.5,c='red')[0]
    coil[0].set_Bfield()
    if current != 0:
        cir=np.transpose(cir)
        cir2=np.transpose(cir2)    
        ra=cir[int(len(cir)/2)]
        rb=cir[int(len(cir)/2)+1]
        ar = Arrow3D([ra[0],rb[0]],[ra[1],rb[1]],[ra[2],rb[2]], mutation_scale=10,lw=2, arrowstyle="-|>", color="red")
        ax.add_artist(ar)
        ra=cir2[int(len(cir2)/2)]
        rb=cir2[int(len(cir2)/2)+1]
        br = Arrow3D([ra[0],rb[0]],[ra[1],rb[1]],[ra[2],rb[2]], mutation_scale=10,lw=2, arrowstyle="-|>", color="red")
        ax.add_artist(br)

    plt.show()


def qiinter(x,y,z,vx,vy,vz,fixed_scale,field_lines,coil,qi):
    matplotlib.rcParams.update({'font.size': 12})
    fig = plt.figure(dpi=80,figsize=(16, 12))
    ax=fig.gca(projection='3d')
    if fixed_scale:
        ax.set_xlim3d([0, 20])
        ax.set_ylim3d([0, 10])
        ax.set_zlim3d([0, 10])

    ax.set_xlabel('x (cm)')
    ax.set_ylabel('y (cm)')
    ax.set_zlabel('z (cm)')

    cir,cir2=coil[0].calc_circ(0)
    ax.plot(cir[0],cir[1],cir[2],lw=2.5,c='red')[0]
    ax.plot(cir2[0],cir2[1],cir2[2],lw=2.5,c='red')[0]
    
    if coil[0].curr != 0:
        cir=np.transpose(cir)
        cir2=np.transpose(cir2)    
        ra=cir[int(len(cir)/2)]
        rb=cir[int(len(cir)/2)+1]
        ar = Arrow3D([ra[0],rb[0]],[ra[1],rb[1]],[ra[2],rb[2]], mutation_scale=10,lw=2.5, arrowstyle="-|>", color="red")
        ax.add_artist(ar)
        ra=cir2[int(len(cir2)/2)]
        rb=cir2[int(len(cir2)/2)+1]
        br = Arrow3D([ra[0],rb[0]],[ra[1],rb[1]],[ra[2],rb[2]], mutation_scale=10,lw=2.5, arrowstyle="-|>", color="red")
        ax.add_artist(br)


    a=coil[0]
    if field_lines:
        set_bl=copy.deepcopy(coil[0].setbl)        
        for bli in set_bl:
            curr_bli=bli
            if not fixed_scale:
                for jbli in np.arange(len(bli[1])):
                    if  bli[1][jbli]< -a.r/10:
                        curr_bli[1][jbli]= NaN
                    elif bli[1][jbli]> 2.1*a.r:
                        curr_bli[1][jbli]= NaN
                    else:
                        pass
                for jbli in np.arange(len(bli[2])):
                    if  bli[2][jbli]< -a.r/10:
                        curr_bli[2][jbli]= NaN
                    elif bli[2][jbli]> 2.1*a.r:
                        curr_bli[2][jbli]= NaN
                    else:
                        pass
                for jbli in np.arange(len(bli[0])):
                    if  bli[0][jbli]< -a.p1/10:
                        curr_bli[0][jbli]= NaN
                    elif bli[0][jbli]> a.p2*11/10:
                        curr_bli[0][jbli]= NaN
                    else:
                        pass

            ax.plot(curr_bli[0],curr_bli[1],curr_bli[2],lw=1.5,c='seagreen',alpha=0.75)
            ra=[bli[0][2],bli[1][2],bli[2][2]]
            rb=[bli[0][3],bli[1][3],bli[2][3]]
            ar = Arrow3D([ra[0],rb[0]],[ra[1],rb[1]],[ra[2],rb[2]], mutation_scale=10,lw=1.5, arrowstyle="-|>", color='seagreen')
        ax.add_artist(ar)



    ax.scatter(x,y,z,s=5,c="dodgerblue")
    avx=[x,x+vx]
    avy=[y,y+vy]
    avz=[z,z+vz]
    
    var = Arrow3D(avx,avy,avz, mutation_scale=10,lw=2.5, arrowstyle="-|>", color="dodgerblue")
    ax.add_artist(var)

    #ax.scatter(bli[0,0],bli[1,0],bli[2,0],c='red')


    if coil[0].curr>0:
        ax.plot(cir[0][4:21],cir[1][4:21],cir[2][4:21],lw=2,c='red')
        ax.plot(cir2[0][4:21],cir2[1][4:21],cir2[2][4:21],lw=2,c='red')
    else:
        ax.plot(cir[0][11:26],cir[1][11:26],cir[2][11:26],lw=2,c='red')
        ax.plot(cir2[0][11:26],cir2[1][11:26],cir2[2][11:26],lw=2,c='red')
    
    plt.show()
    qi.clear()
    qi.append([x,y,z,vx,vy,vz])



    
