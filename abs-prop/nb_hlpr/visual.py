import os
import ipywidgets as widgets
from ipywidgets import interact, interactive, fixed, interact_manual,Layout

import numpy as np

import matplotlib
from matplotlib import pyplot as plt
from matplotlib import animation

from zdf import read_grid, read_particles

import matplotlib.patches as patches
import scipy.fftpack

if os.name == 'nt':
    bar="\\"
else:
    bar="/"


def pltinteract(filelst1,col1,filelst2=[],col2=None,xlims=None,ylim=None,fils=[],plas=False,mw=False):
    ylim.clear()
    (data0,inf0)=read_grid(filelst1[0])
    ymin=min(data0)
    ymax=max(data0)
    
    ylimin=ymin-(ymax-ymin)/10
    ylimax=ymax+(ymax-ymin)/10
    ylims=[ylimin,ylimax]
    ylims2=[0,0]
    ylim.append(ylims)

    if filelst2:
        (data02,inf02)=read_grid(filelst2[0])
        ymin2=min(data02)
        ymax2=max(data02)

        ylimin2=ymin2-(ymax2-ymin2)/10
        ylimax2=ymax2+(ymax2-ymin2)/10
        ylims2=[ylimin2,ylimax2]
    ylim.append(ylims2)
    def pltstf(itr,fil,col,fil2=[],col2=None,xlims=None,ylims=None,fils=[],plasm=False):
        (data, info) = read_grid(fil[itr])
        fils.clear()
        fils.append(fil[itr])
        xmin=info['grid']['axis'][0]['min']
        xmax=info['grid']['axis'][0]['max']
        npts=info['grid']['nx'][0]
        global ymin
        global ymax
        ymin=min(data)
        ymax=max(data)
        ylimin=ymin-(ymax-ymin)/10
        ylimax=ymax+(ymax-ymin)/10

        if ylimin<ylims[0][0]:
            ylims[0][0]=ylimin

        if ylimax>ylims[0][1]:
            ylims[0][1]=ylimax

        
        xx=np.linspace(xmin,xmax,npts)
        if not mw:
            xmin=xlims[0];
            xmax=xlims[1];
        
        fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')

        ax1=plt.gca()
        lines=[]
        lines.append(ax1.plot(xx,data,c=col,lw=3.5,label="$"+info['grid']['label']+"$")[0]);
        #lines.append(ax1.plot(xx,data,'+',c="coral")[0]);
        ax1.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
        ax1.set_xlim([xmin,xmax])
        ax1.set_ylim(ylims[0])
        ax1.set_xlabel("$"+info['grid']['axis'][0]['label']+"$"+" ($"+info['grid']['axis'][0]['units']+"$)")
        ax1.set_ylabel("$"+info['grid']['label']+"$"+" ($"+info['grid']['units']+"$)")
        if plasm and not fil2:
            ax1.fill_between(xx,data,0,alpha=0.4,color=col)
        if fil2:
            (data2, info2) = read_grid(fil2[itr])
            fils.append(fil2[itr])
            ax2=ax1.twinx()
            
            ymin2=min(data2)
            ymax2=max(data2)
            ylimin2=ymin2-(ymax2-ymin2)/10
            ylimax2=ymax2+(ymax2-ymin2)/10

            if ylimin2<ylims[1][0]:
                ylims[1][0]=ylimin2
            
            if ylimax2>ylims[1][1]:
                ylims[1][1]=ylimax2

            ax2.set_xlim([xmin,xmax])
            ax2.set_ylim(ylims[1])
            ax2.set_xlabel("$"+info2['grid']['axis'][0]['label']+"$"+" ($"+info2['grid']['axis'][0]['units']+"$)")
            ax2.set_ylabel("$"+info2['grid']['label']+"$"+" ($"+info2['grid']['units']+"$)")
            ax2.plot(xx,data2,c=col2,lw=3.5,label="$"+info2['grid']['label']+"$")
            ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))
            if plasm:
                ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
            #lin[0].set_data(xx,data)
            #lin[1].set_data(xx,data)
        
            #xx=np.transpose(xx)
            #print(xx,len(xx))
            ax1.legend(loc=0)
        plt.show()
        
    sldr=widgets.IntSlider(value=0,min=0,max=len(filelst1)-1,step=1,orientation='horizontal',readout=False,description=' ')
    buuuu=widgets.Button()
    return interact(pltstf,itr=sldr,fil=fixed(filelst1),fil2=fixed(filelst2),col=fixed(col1),col2=fixed(col2),xlims=fixed(xlims),ylims=fixed(ylim),fils=fixed(fils),plasm=fixed(plas))


def FT_pltinteract(filelst1,col1,filelst2=[],col2=None,xlims=None,ylim=None,fils=[]):
    #empty the y limits list
    ylim.clear()
    #read the first data set
    (data0,inf0)=read_grid(filelst1[0])
    #find the dimensions of the box
    xmin=inf0['grid']['axis'][0]['min']
    xmax=inf0['grid']['axis'][0]['max']
    npts=inf0['grid']['nx'][0]
    
    
    # sample period
    T = xmax / npts
    #Compute the first FFT
    yf = 2.0/npts * np.abs(scipy.fftpack.fft(data0))
    #Build the frequency array
    xf = 2*3.14*np.linspace(0.0, 1.0/(2.0*T), int(npts//2))
    
    # use propper limits for the y-axis
    ymin=min(yf)
    ymax=max(yf)
    ylimin=ymin-(ymax-ymin)/10
    ylimax=ymax+(ymax-ymin)/10
    ylims=[ylimin,ylimax]
    ylims2=[0,0]
    ylim.append(ylims)
    
    if filelst2:
        #read the second data set if there is one
        (data02,inf02)=read_grid(filelst2[0])
        #compute the second FFT
        yf = 2.0/npts * np.abs(scipy.fftpack.fft(data02))
        
        # use propper limits for the y-axis again
        ymin2=min(yf)
        ymax2=max(yf)
        ylimin2=ymin2-(ymax2-ymin2)/10
        ylimax2=ymax2+(ymax2-ymin2)/10
        ylims2=[ylimin2,ylimax2]
    ylim.append(ylims2)
    
    #This function is called whenever the user moves the slider
    def pltstf(itr,fil,col,fil2=[],col2=None,xlims=None,ylims=None,fils=[]):
        # Build the frame for the plot
        fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')
        ax1=plt.gca()
        
        #Read the file chosen by the user
        (data, info) = read_grid(fil[itr])
        
        # This variable stores the information about the current file so that it can
        # be accessed outside the function
        fils.clear()
        fils.append(fil[itr])
        
        # find the current dimensions of the box
        xmin=info['grid']['axis'][0]['min']
        xmax=info['grid']['axis'][0]['max']
        npts=info['grid']['nx'][0]
        
        # sample spacing
        T = (xmax-xmin) / npts
        y = data
        yf = 2.0/npts * np.abs(scipy.fftpack.fft(y))
        xf = 2*3.14*np.linspace(0.0, 1.0/(2.0*T), int(npts//2))
        
        # compute propper limits for the y axis
        ymin=min(yf)
        ymax=max(yf)
        ylimin=ymin-(ymax-ymin)/10
        ylimax=ymax+(ymax-ymin)/10
        ax1.set_ylim(ylims[0])
        ax1.set_xlim(xlims)
        
        #check if an update to the y-axis limits is necessary
        if ylimin<ylims[0][0]:
            ylims[0][0]=ylimin
        if ylimax>ylims[0][1]:
            ylims[0][1]=ylimax
        
        # plot the fourier transform
        ax1.plot(xf,yf[:int(npts//2)],c=col,lw=3.5,label="$"+info['grid']['label']+"$")
        
        #set the tilte of the plot: it will contain the information about the current time
        ax1.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='center')
        
        #label the axis
        ax1.set_xlabel("$ k$ [$\omega_p/c$]")
        ax1.set_ylabel("$FFT("+info['grid']['label']+")$"+" [$|"+info['grid']['units']+"|^2$]")
        
        #repeat the process for the second data set
        if fil2:
            (data2, info2) = read_grid(fil2[itr])
            fils.append(fil2[itr])
            ax2=ax1.twinx()
            
            yf2 = 2.0/npts * np.abs(scipy.fftpack.fft(data2))
            
            ymin2=min(yf2)
            ymax2=max(yf2)
            ylimin2=ymin2-(ymax2-ymin2)/10
            ylimax2=ymax2+(ymax2-ymin2)/10
            
            if ylimin2<ylims[1][0]:
                ylims[1][0]=ylimin2
            
            if ylimax2>ylims[1][1]:
                ylims[1][1]=ylimax2
            ax2.set_xlim(xlims)
            ax2.set_ylim(ylims[1])
            ax2.set_ylabel("$FFT("+info2['grid']['label']+")$"+" [$|"+info['grid']['units']+"|^2$]")
            ax2.plot(xf,yf2[:int(npts//2)],c=col2,lw=3.5,label="$"+info2['grid']['label']+"$")
            
            ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))
            ax1.legend(loc=0)
        plt.show()

    sldr=widgets.IntSlider(value=0,min=0,max=len(filelst1)-1,step=1,orientation='horizontal',readout=False,description=' ')
    return interact(pltstf,itr=sldr,fil=fixed(filelst1),fil2=fixed(filelst2),col=fixed(col1),col2=fixed(col2),xlims=fixed(xlims),ylims=fixed(ylim),fils=fixed(fils))


def filer(tab):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value
    
    xlims=[i.value for i in   tab.children[2].children[5].children]
    
    if bool1 and bool2:
        fldr=tab.children[0].children[1].children[0].value
        key=tab.children[0].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        fldr=tab.children[1].children[1].children[0].value
        key=tab.children[1].children[1].children[1].value
        files2 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        return [files,files2,xlims]
    
    elif bool1 or bool2:
        fldr=tab.children[int(bool2)].children[1].children[0].value
        key=tab.children[int(bool2)].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        return [files,None,xlims]

def animate(tab,yl):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value
    
    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value
    
    
    fil=filer(tab)
    xlims=fil[2]
    
    pl2=tab.children[2].children[2].children[2].value
    # First set up the figure, the axis, and the plot element we want to animate
    fig=plt.figure(figsize=(16,9), dpi= 120, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    mw=tab.children[2].children[4].value
    if bool2 and bool1:
        (data, info) = read_grid(fil[0][0])
        xmin=info['grid']['axis'][0]['min']
        xmax=info['grid']['axis'][0]['max']
        npts=info['grid']['nx'][0]
        xx=np.linspace(xmin,xmax,npts)

        (data2, info2) = read_grid(fil[1][0])
        
        ax = plt.axes()
        if not mw:
            xmin=xlims[0];
            xmax=xlims[1];

        ax.set_xlim([xmin,xmax])
        ax.set_ylim(yl[0])
        ax2 = ax.twinx()
        ax2.set_ylim(yl[1])
        lines =[]
        lines.append(ax.plot([], [], lw=2,c=col1,label="$"+info['grid']['label']+"$")[0])
        lines.append(ax2.plot([], [], lw=2,c=col2,label="$"+info2['grid']['label']+"$")[0])
        ax.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
        ax.set_xlabel("$"+info['grid']['axis'][0]['label']+"$"+" ($"+info['grid']['axis'][0]['units']+"$)")
        ax.set_ylabel("$"+info['grid']['label']+"$"+" ($"+info['grid']['units']+"$)")
        ax2.set_ylabel("$"+info2['grid']['label']+"$"+" ($"+info2['grid']['units']+"$)")
        ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))
        if pl2:
            for coll in (ax2.collections):
                ax2.collections.remove(coll)
            ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
        ax.legend(loc=0)
        
    elif bool1 or bool2:
        (data, info) = read_grid(fil[bool2][0])
        xmin=info['grid']['axis'][0]['min']
        xmax=info['grid']['axis'][0]['max']
        npts=info['grid']['nx'][0]
        xx=np.linspace(xmin,xmax,npts)
        if not mw:
            xmin=xlims[0];
            xmax=xlims[1];
    
        ax = plt.axes(xlim=[xmin,xmax], ylim=yl[bool2])
        lines =[]
        lines.append(ax.plot([], [], lw=2)[0])
        ax.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
        ax.set_xlabel("$"+info['grid']['axis'][0]['label']+"$"+" ($"+info['grid']['axis'][0]['units']+"$)")
        ax.set_ylabel("$"+info['grid']['label']+"$"+" ($"+info['grid']['units']+"$)")
        if pl2 and not bool1:
            ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
        
    # initialization function: plot the background of each frame
    def init():
        for line in lines:
            line.set_data([], [])
        ax.set_xlim(xlims)
        return lines

    # animation function.  This is called sequentially
    def animate(i,files,mw):
        if bool1 and bool2:
            (data, info) = read_grid(files[0][i])
            xmin=info['grid']['axis'][0]['min']
            xmax=info['grid']['axis'][0]['max']
            npts=info['grid']['nx'][0]
            xx=np.linspace(xmin,xmax,npts)
            if not mw:
                xmin=xlims[0];
                xmax=xlims[1];
            ax.set_xlim([xmin,xmax])
            ax2.set_xlim([xmin,xmax])
            ax.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
            lines[0].set_data(xx, data)
            (data, info) = read_grid(files[1][i])
            lines[1].set_data(xx,data)
            if pl2:
                for coll in (ax2.collections):
                    ax2.collections.remove(coll)
                ax2.fill_between(xx,data,0,alpha=0.4,color=col2)
        elif bool1 or bool2:
            (data, info) = read_grid(files[bool2][i])
            ax.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
            xmin=info['grid']['axis'][0]['min']
            xmax=info['grid']['axis'][0]['max']
            npts=info['grid']['nx'][0]
            if not mw:
                xmin=xlims[0];
                xmax=xlims[1];
            ax.set_xlim([xmin,xmax])
            xx=np.linspace(xmin,xmax,npts)
            x = xx
            y = data            
            lines[0].set_data(x, y)
            
        return lines

    # call the animator.  blit=True means only re-draw the parts that have changed.
    return animation.FuncAnimation(fig, animate,fargs=(fil,mw,), init_func=init,
                               frames=len(fil[0]), interval=20, blit=True)

def FT_animate(tab,yl):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value
    
    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value
    
    
    fil=filer(tab)
    xlims=fil[2]
    
    pl2=tab.children[2].children[2].children[2].value
    # First set up the figure, the axis, and the plot element we want to animate
    fig=plt.figure(figsize=(16,9), dpi= 120, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    
    if bool2 and bool1:
        (data, info) = read_grid(fil[0][0])
        xmin=info['grid']['axis'][0]['min']
        xmax=info['grid']['axis'][0]['max']
        npts=info['grid']['nx'][0]
        
        # sample spacing
        T = (xmax-xmin) / npts
        xf = 2*3.14*np.linspace(0.0, 1.0/(2.0*T), int(npts//2))
        
        
        (data2, info2) = read_grid(fil[1][0])
        
        ax = plt.axes()
        ax.set_xlim(xlims)
        ax.set_ylim(yl[0])
        ax2 = ax.twinx()
        ax2.set_ylim(yl[1])
        lines =[]
        lines.append(ax.plot([], [], lw=2,c=col1,label="$"+info['grid']['label']+"$")[0])
        lines.append(ax2.plot([], [], lw=2,c=col2,label="$"+info2['grid']['label']+"$")[0])
        ax.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
        ax.set_xlabel("$ k$ [$\omega_p/c$]")
        ax.set_ylabel("$FFT("+info['grid']['label']+")$"+" [$|"+info['grid']['units']+"|^2$]")
        ax2.set_ylabel("$FFT("+info2['grid']['label']+")$"+" [$|"+info['grid']['units']+"|^2$]")
        ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))
        if pl2:
            ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
        ax.legend(loc=0)

    elif bool1 or bool2:
        (data, info) = read_grid(fil[bool2][0])
        xmin=info['grid']['axis'][0]['min']
        xmax=info['grid']['axis'][0]['max']
        npts=info['grid']['nx'][0]
        xx=np.linspace(xmin,xmax,npts)
        
        ax = plt.axes(xlim=xlims, ylim=yl[bool2])
        lines =[]
        lines.append(ax.plot([], [], lw=2)[0])
        ax.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='right')
        ax.set_xlabel("$ k$ [$\omega_p/c$]")
        ax.set_ylabel("$FFT("+info['grid']['label']+")$"+" [$|"+info['grid']['units']+"|^2$]")
        if pl2 and not bool1:
            ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)

    # initialization function: plot the background of each frame
    def init():
        for line in lines:
            line.set_data([], [])
            ax.set_xlim(xlims)
        return lines
    
    # animation function.  This is called sequentially
    def animate(i,files):
        if bool1 and bool2:
            (data, info) = read_grid(files[0][i])
            ax.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='center')
            
            xmin=info['grid']['axis'][0]['min']
            xmax=info['grid']['axis'][0]['max']
            npts=info['grid']['nx'][0]
            # sample spacing
            T = (xmax-xmin) / npts
            xf = 2*3.14*np.linspace(0.0, 1.0/(2.0*T), int(npts//2))
            yf2 = 2.0/npts * np.abs(scipy.fftpack.fft(data))
            
            lines[0].set_data(xf, yf2[:int(npts//2)])
            (data, info) = read_grid(files[1][i])
            yf2 = 2.0/npts * np.abs(scipy.fftpack.fft(data))
            lines[1].set_data(xf,yf2[:int(npts//2)])
        elif bool1 or bool2:
            (data, info) = read_grid(files[bool2][i])
            xmin=info['grid']['axis'][0]['min']
            xmax=info['grid']['axis'][0]['max']
            npts=info['grid']['nx'][0]
            # sample spacing
            T = (xmax-xmin) / npts
            xf = 2*3.14*np.linspace(0.0, 1.0/(2.0*T), int(npts//2))
            ax.set_title("$n$="+str(info['iteration']['n'])+", $t$="+'{:.2f}'.format(info['iteration']['t'])+"$("+info['iteration']['tunits']+")$",loc='center')
            yf2 = 2.0/npts * np.abs(scipy.fftpack.fft(data))
            lines[0].set_data(xf, yf2[:int(npts//2)])
        
        return lines

    # call the animator.  blit=True means only re-draw the parts that have changed.
    return animation.FuncAnimation(fig, animate,fargs=(fil,), init_func=init,
                               frames=len(fil[0]), interval=20, blit=True)

