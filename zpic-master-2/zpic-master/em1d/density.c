#include "simulation.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include "emf.h"

float custom_n0( float x ) {

	return 1.0 + 0.5*sin(x/M_PI)*sin(x/M_PI);

} 

void sim_init( t_simulation* sim ){

	// Time step
	float dt = 0.01;
	float tmax = 50.00;

	// Simulation box
	int   nx  = 128;
	float box = 60.0;
	
	// Diagnostic frequency
	int ndump = 50;

    // Initialize particles
	const int n_species = 1;
	
	// Use 128 particles per cell
	int ppc = 128;

	// Density profile
//	t_density density = { .type = UNIFORM };
//	t_density density = { .type = STEP, .start = 17.5 };
//	t_density density = { .type = SLAB, .start = 17.5, .end = 22.5 };
	t_density density = { .type = RAMP, .start = 20, .end = 50, .ramp = { 10.0, 100.} };
	//	t_density density = { .type = CUSTOM, .custom = &custom_n0 };

	t_species* species = (t_species *) malloc( n_species * sizeof( t_species ));
	spec_new( &species[0], "electrons", -1.0, ppc, NULL, NULL, nx, box, dt, &density );
	// Initialize Simulation data
	sim_new( sim, nx, box, dt, tmax, ndump, species, n_species );

	// Add laser pulse (this must come after sim_new)
	t_emf_laser laser = {
		.start = 16.0,
		.fwhm  = 1.,
		.a0 = 1.,
		.omega0 = 10.,
		.polarization = M_PI_2
	};
	sim_add_laser( sim, &laser );

	
	// Set moving window (this must come after sim_new)
	//sim_set_moving_window( sim );

}


void sim_report( t_simulation* sim ){

  	// All electric field components
	emf_report( &sim->emf, EFLD, 0 );
	emf_report( &sim->emf, EFLD, 1 );
	emf_report( &sim->emf, EFLD, 2 );

	// Charge density
	spec_report( &sim->species[0], CHARGE, NULL, NULL );

	// RAW dump
	spec_report( &sim->species[0], PARTICLES, NULL, NULL );
			
}
