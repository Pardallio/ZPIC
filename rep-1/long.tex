
\documentclass[10pt,a4paper]{article}
\usepackage{ritaconfig}

%----------------------------------------------------------------------------------------
%       TITLE SECTION
%----------------------------------------------------------------------------------------
\title{\vspace{-20mm}\fontsize{18pt}{10pt}\textbf{Electromagnetic wave absorption by a plasma ramp}} % Article title
\author{\normalsize \textbf{The ZPIC Collection of plasma physics simulations} (\today) % Your institution
%\normalsize \href{mailto:john@smith.com}{john@smith.com} % Your email address
\vspace{-125mm}
}
\date{}

%----------------------------------------------------------------------------------------

\graphicspath{ {./img/} }

\begin{document}

%\begin{adjustwidth}{-27pt}{-27pt}
\maketitle % Insert title
%\end{adjustwidth}
\vspace{-50pt}

\section{Introduction}
When an electromagnetic wave reaches a plasma with an certain electron density $n_e$,
two possible scenarios may occur:
it can kepp propagating normally, or it can fade away exponentially until a given length.
The key to understand why such distinct phenomena may happen lies both on Maxwell's
equations and on the 
equation for the dielctric response of a non collisional plasma when excited by an
electromagnetic wave which when linearized arround $\mathbf{v_{e0}}=0$ yields:

\begin{equation}
  \frac{\partial \mathbf{v_e}}{\partial t}+(\mathbf{v_e}\cdot\mathbf{\nabla})\mathbf{v_e}=-\frac{e}{m_e}\mathbf{E}\Leftrightarrow   i\omega \mathbf{v_{e1}}=\frac{e\mathbf{E_1}}{m_e}
  \end{equation}

\noindent Amp\`ere's law states that:

\begin{equation}
  c^2\mathbf{\nabla}\times\mathbf{B_1}=\frac{1}{\varepsilon_0}\mathbf{j_1}+\mathbf{\dot{E}_1}\Leftrightarrow c^2\mathbf{\nabla}\times\mathbf{\dot{B}_1}=\frac{1}{\varepsilon_0}\frac{\partial\mathbf{j_1}}{\partial t}+\mathbf{\ddot{E}_1}
\end{equation}

\noindent assuming an $\exp{[-i(\omega t- \mathbf{k}\cdot\mathbf{r})]}$ dependence and
transverse waves ($\mathbf{k}\cdot\mathbf{E}=0)$ and
using the differential form of Faraday's law of induction ($\mathbf{\nabla}\times\mathbf{E}=\mathbf{\dot{B}}$) it is possible to arrive to:

\begin{equation}
(  \omega^2-c^2k^2)\mathbf{E_1}=-i\frac{\omega \mathbf{j_1}}{\varepsilon_0}=\frac{n_{e0}e^2}{\varepsilon_0m_e}\mathbf{E_1}
\end{equation}

From the previous equation one can identify the plasma frequency
$\omega_p=\sqrt{\frac{n_{e0}e^2}{\varepsilon_0m_e}}$ and arrive to the conclusion that
$c^2k^2=\omega^2-\omega_p^2$. This way it is possible to find two different situations
\begin{itemize}
\item $\omega^2\geq\omega_p^2\Rightarrow k^2\geq 0\Rightarrow$ $k$ is real, so
  the wave may propagate  with an oscilatory term ($\propto\exp{(ikr)}$).

\item $\omega^2\leq\omega_p^2\Rightarrow k^2\leq 0\Rightarrow$ $k$ is imaginary,
  therefore, the wave may propagate in space with an exponential attenuation term ($\propto\exp{(-kr)}$). The typical legth for the propagation of an attenuated wave is the so called \textit{skin depth}:
  \end{itemize}

\begin{equation}
\delta=k^{-1}=\frac{c}{\sqrt{\omega_{pe}^2-\omega^2}}
\end{equation}

If, for example the the plasma density is not constant (e.g increasing density) the 
wave may propagate normally up until a certain point and then start decaying, 
as shown in the next figure:

\begin{figure}[h]
  \centering
  \includegraphics[width=0.65\textwidth]{ex.pdf}
  \caption{Example of a flat top monochromatic laser hitting a plasma ramp. The critical density is located at $x=$30 $c/\omega_p$.}
\label{fig:examp}
\end{figure}

The shape of the wave show in Figure \ref{fig:examp} is described by an Airy function.
This can be easily understood by looking at the wave equation.

\begin{equation}
\nabla^2E+\frac{\omega^2}{c^2}\epsilon(x,\omega)E=0\Leftrightarrow\frac{\partial^2E}{\partial x^2}+\frac{\omega^2}{c^2}\epsilon(x,\omega)E=0
\end{equation}

For a non-collisional plasma, with linearly growing density $n_e=xn_c/L$ the dielectric function $\epsilon(x,\omega)$ is given by: 
($n_c$ is the critical density for the wave frequency $\omega$, \textit{i.e.}, the densty for which  the wave no longer propagates and starts to be absorbed)

\begin{equation}
\epsilon(x,\omega)=1-\frac{\omega_{pe}^2}{\omega^2}=1-\frac{n_e}{n_c}=1-\frac{x}{L}=-\frac{1}{L}(x-L)
\end{equation}

If we introduce the variable $\eta=[\omega^2/(c^2L)]^{1/3}(x-L)$, we can write the wave equation as:

\begin{align}
\frac{\partial^2E}{\partial x^2}=&\left(\frac{\omega^2}{c^2L}\right)^{2/3}\frac{\partial^2E}{\partial \eta^2}\;\;\text{and}\;\; \frac{\omega^2}{c^2}\epsilon(x,\omega)=-\left(\frac{\omega^2}{c^2L}\right)^{2/3}\eta\\
&\Rightarrow\frac{\partial^2E}{\partial \eta^2}-\eta E=0
\end{align}

The solution to the above equation is an Airy function, given by:

\begin{equation}
 Ai(\eta)={\frac{1}{\pi}}{\int_0^\infty}\cos{\left({ \frac{t^3}{3}}-\eta t\right)}dt 
\end{equation}

The Airy Function can be approximated by more treatable functions like 
cosines (for the left part) and exponentials (for the right part), however, the  transition 
region is poorly approximated by any of these functions as you can see below:
%<img src="Mplwp_airyai_asymptotic.svg"  style="width: 50%; float: left;" />	

\begin{figure}[H]
  \begin{minipage}{.5\textwidth}


\begin{align*}
Ai(\eta)&\simeq \frac{1}{(\eta\pi^2)^{1/4}}\exp{\left(-\frac{2}{3}(\eta)^{3/2}\right)}, \;\;\text{for}\;\eta\gg 1\\
Ai(\eta)&\simeq\frac{1}{(-\eta\pi^{2})^{1/4}}\cos{\left(\frac{2}{3}(-\eta)^{3/2}-\frac{\pi}{4}\right)}, \;\;\text{for}\;\eta\ll -1
\end{align*}

    %\begin{align}
     %   Ai(\eta)\simeq\frac{1}{(-\eta\pi^{2})^{1/4}}\cos{\left(\frac{2}{3}(-\eta)^{3/2}-\frac{\pi}{4}\right)}, \;\;\text{for}\;\eta\ll -1
     %   Ai(\eta)\simeq \frac{1}{(\eta\pi^2)^{1/4}\exp{\left(-\frac{2}{3}(\eta)^{3/2}\right)}, \;\;\text{for}\;\eta\gg 1
     %   \end{align}
  \end{minipage}%
  \begin{minipage}{.5\textwidth}
    \centering
      \includegraphics[width=0.95\textwidth]{image.pdf}
  \caption{Airy function and its approximations.}
\label{fig:air}
      \end{minipage}
\end{figure}

%$ Ai(\eta)\simeq{\large \frac{1}{\pi^{1/2}(-\eta)^{1/4}}}\cos{\left({{\large\frac{2}{3}}(-\eta)^{3/2}}-{\large\frac{\pi}{4}}\right)}dt, \;\;\text{for}\;\eta\ll -1$ 


%$ Ai(\eta)\simeq{\large \frac{1}{\pi^{1/2}(\eta)^{1/4}}}\exp{\left({{\large-\frac{2}{3}}(\eta)^{3/2}}\right)}dt, \;\;\text{for}\;\eta\gg 1$ 



\begin{multicols}{2} % Two-column layout throughout the main article text


\end{multicols}
\end{document}
