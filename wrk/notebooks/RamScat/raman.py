from zdf import read
import os
import ipywidgets as widgets
from ipywidgets import interact, interactive, fixed, interact_manual,Layout

import numpy as np

import matplotlib
from matplotlib import pyplot as plt
from matplotlib import animation

if os.name == 'nt':
    bar="\\"
else:
    bar="/"


def pltcomp(fil):
    (data0,inf0)=read(fil)
    ymin=min(data0)
    ymax=max(data0)
    
    ylimin=ymin-(ymax-ymin)/10
    ylimax=ymax+(ymax-ymin)/10

    xmin=inf0.grid.axis[0].min
    xmax=inf0.grid.axis[0].max

    def pltstf(fil,xlim,ypos,xpos,bars):
        fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')
        ax1=plt.gca()
        #Read the file chosen by the user
        (data, info)=read(fil)
        bars[0].min=xlim[0]
        bars[0].max=xlim[1]
        bars[0].step=(xlim[1]-xlim[0])/511
              
        # find the current dimensions of the box
        xmin=info.grid.axis[0].min
        xmax=info.grid.axis[0].max
        npts=info.grid.nx[0]
        
        ymin=min(data)
        ymax=max(data)
        ylimin=ymin-(ymax-ymin)/10
        ylimax=ymax+(ymax-ymin)/10

        ax1.set_ylim([ylimin,ylimax])
        ax1.set_xlim(xlim)
        
        xx=np.linspace(xmin,xmax,npts)
        

        lines=[]
        lines.append(ax1.plot(xx,data,lw=3.5,label="$"+info.grid.label+"$")[0]);
        #lines.append(ax1.plot(xx,data,'+',c="coral")[0]);
        ax1.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='right')
        ax1.set_xlabel("$"+info.grid.axis[0].label+"$"+" ($"+info.grid.axis[0].units+"$)")
        ax1.set_ylabel("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")

        xbarmin=xpos
        xbarmax=xbarmin+6.28
        ybar=ypos
        ylims=ax1.get_ylim()
        ybarmin=(ybar-ylimin)/(ylimax-ylimin)-0.03
        ybarmax=(ybar-ylimin)/(ylimax-ylimin)+0.03
        xbarmin=(xbarmin-xlim[0])/(xlim[1]-xlim[0])
        xbarmax=(xbarmax-xlim[0])/(xlim[1]-xlim[0])
        

        ax1.axhline(y=ybar, xmin=xbarmin, xmax=xbarmax,c='k',lw=5)
        ax1.axvline(x=xpos, ymin=ybarmin, ymax=ybarmax,c='k',lw=5)
        ax1.axvline(x=xpos+6.28, ymin=ybarmin, ymax=ybarmax,c='k',lw=5)
        plt.show()



    sldr1=widgets.FloatSlider(value=xmin,min=xmin,max=xmax,step=(xmax-xmin)/511,orientation='horizontal',readout=True,description='x pos: ')
    sldr2=widgets.FloatSlider(value=(ylimin+ylimax)/2,min=ylimin,max=ylimax,step=(ylimax-ylimin)/511,orientation='horizontal',readout=True,description='y pos: ')
    sldr3=widgets.FloatRangeSlider(value=[xmin,xmax],min=xmin,max=xmax,step=(xmax-xmin)/511,orientation='horizontal',readout=True,description='Xlim: ')
    return interact(pltstf,fil=fixed(fil),xlim=sldr3,xpos=sldr1,ypos=sldr2,bars=fixed([sldr1]))

import visual
from matplotlib import ticker

def HeatMap_shower(tab):
    #Get the files list
    files=visual.filer(tab)[0]
    x=[]
    t=[]
    E=[]
    #Read the first file
    (data, info) = read(files[0])
    #build the x-axis array
    xmin=info.grid.axis[0].min
    xmax=info.grid.axis[0].max
    npts=info.grid.nx[0]
    x=np.linspace(xmin,xmax,npts)
    
    #build the x,T,E list
    for fil in files[0::1]:
        #Read the files chosen by the user
        (data, info) = read(fil)
        t.append(info.iteration.t)
        E.append(data)

    Ef=np.array(E)
    
    fig=plt.figure(figsize=(11, 9))
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    ax1=fig.gca()
    #ax1.set_ylim([T[0,0],T[-1,0]])
    #ax1.set_xlim(xrg)
    Emax=np.max(abs(Ef))
    Emin=-Emax
    #acutally plot stuff
    pax=ax1.imshow(Ef,cmap="RdBu",extent=(x[0],x[-1],t[0],t[-1]),aspect="auto",origin="lower",vmin=Emin/3,vmax=Emax/3)
    sfmt=ticker.ScalarFormatter(useMathText=True)
    sfmt.set_powerlimits((0, 0))
    cbar = fig.colorbar(pax,format=sfmt)
    cbar.set_label("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")
    ax1.set_xlabel("$"+info.grid.axis[0].label+"$"+" [$"+info.grid.axis[0].units+"$]")
    ax1.set_ylabel("$t$"+" $["+info.iteration.tunits+"]$")

    plt.show()

import scipy.fftpack

def HeatMap_FTshower(tab,om,opt_cmap='viridis'):
    from matplotlib.colors import LogNorm
    #Get the files list
    files=visual.filer(tab)[0]
    x=[]
    t=[]
    E=[]
    #Read the first file
    (data, info) = read(files[-1])
    #build the x-axis array
    xmin=info.grid.axis[0].min
    xmax=info.grid.axis[0].max
    npts=info.grid.nx[0]
    if info.grid.label=="E1":
        om=1
    # sample period
    T = xmax / npts
    #Build the frequency array
    xf = 2*3.14*np.linspace(0.0, 1.0/(1.0*T), int(npts//2))

    #build the x,T,E list
    for fil in files[0::1]:
        #Read the files chosen by the user
        (data, info) = read(fil)
        yf = 2.0/npts * np.abs(scipy.fftpack.fft(data))
        t.append(info.iteration.t)
        E.append(yf)

    #convert all values into matrix style
    x=xf
    Ef=np.array(E)
    #Plot the heat_map
    def ddplt(om,show_b,km,k0,kp,kpl,xrg=None):
        #set up the plot

        fig=plt.figure(figsize=(11, 9))
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')
        ax1=fig.gca()
        #ax1.set_ylim([T[0,0],T[-1,0]])
        #ax1.set_xlim(xrg)
        Emax=np.max(abs(Ef))
        Emin=0
        #acutally plot stuff
        pax=ax1.imshow(Ef,cmap="viridis",extent=(x[0],x[-1],t[0],t[-1]),aspect="auto",origin="lower",vmin=Emin,vmax=Emax)
        sfmt=ticker.ScalarFormatter(useMathText=True)
        sfmt.set_powerlimits((0, 0))
        cbar = fig.colorbar(pax,format=sfmt)        
        ax1.set_xlabel("$k$"+" [$"+info.grid.axis[0].units+"^{-1}$]")
        ax1.set_ylabel("$t$"+" $["+info.iteration.tunits+"]$")
        ax1.set_ylim([t[0],t[-1]])
        ax1.set_xlim(xrg)
        if show_b:
            if info.grid.label!="E1":
                ax1.axvline(x=k0,c='r',lw=2)
                ax1.axvline(x=kp,c='r',lw=2)
                ax1.axvline(x=km,c='r',lw=2)
            
                #ax1.axvline(x=np.sqrt(om**2-1),c='r',lw=2)
                #ax1.axvline(x=1+np.sqrt(om**2-1),c='r',lw=2)
                #ax1.axvline(x=-1+np.sqrt(om**2-1),c='r',lw=2)
            else:
                ax1.axvline(x=kpl,c='r',lw=2)
                #ax1.axvline(x=1,c='r',lw=2)
        #acutally plot stuff
        cbar.set_label(r"|FFT $"+info.grid.label+"$|"+" [$"+info.grid.units+"\times"+info.grid.axis[0].units+"$]")
        plt.show()
    
    maxx=2*om
    xrang_sldr=widgets.FloatRangeSlider(value=[min(x), maxx],min=min(x),max=max(x),step=(max(x)-min(x))/512,description='$x_{\\text{range}}$',
                                        orientation='horizontal',readout=True,readout_format='.1f',)
    show_bar=widgets.Checkbox(value=False,description='Show Prediction:')
    predam=widgets.FloatText(description="$k_-$")
    preda0=widgets.FloatText(description="$k_0$")
    predap=widgets.FloatText(description="$k_+$")
    predapl=widgets.FloatText(description="$k_p$")
    return interact(ddplt,xrg=xrang_sldr,show_b=show_bar,km=predam,k0=preda0,kp=predap,kpl=predapl,
                    Ef=fixed(Ef),om=fixed(om))


def grwoth_r(om,quant,tab,gr,type="rfs"):    
    fldr=tab.children[0].children[1].children[0].value

    if quant=="a_-" or quant=="a_+":
        key="E3"
        if quant=="a_-":
            omc=np.sqrt(om**2-1)-1
        if quant=="a_+":
            omc=np.sqrt(om**2-1)+1
    elif quant=="a_p" :
        key="E1"
        omc=1
        if type=="rbs":
            omc=2*np.sqrt(om**2-1)-1
    else:
        print("Unknown component:",quant,". Exiting")
        return 

    files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    (data, info) = read(files[-1])

    
    # find the current dimensions of the box
    xmin=info.grid.axis[0].min
    xmax=info.grid.axis[0].max
    npts=info.grid.nx[0]
    # sample spacing
    T = (xmax-xmin) / npts
    xf = 2*3.14*np.linspace(0.0, 1.0/(1.0*T), npts)
    tmax=info.iteration.t
    nt=len(files)-1
    p1=[]
    t=[]
    print(omc-0.1,omc+0.1)
    z1=[((x>omc-0.1) and (x<omc+0.1)) for x in xf]
    for fil in files[1:]:
        #Read the file chosen by the user
        (data, info) = read(fil)
        y = data
        yf = 2.0/npts * np.abs(scipy.fftpack.fft(y))
        p1.append(np.sum(yf[z1]))
        t.append(info.iteration.t)
    ymin=min(p1)
    ymax=max(p1)
    def pltstf(tini):
        fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')
        ax1=plt.gca()
        th=[]

        flag=False
        for it,fil in enumerate(files[1:]):
            if(t[it]>tini):
                if (not flag):
                    #print(tini,it,p1[it+1])
                    pc=p1[it]
                    flag=True

                #th.append(pc*np.exp(a0/np.sqrt(8)/om*((t[it]-tini))))
                th.append(pc*gr(t[it]-tini))
            else:
                th.append(0)

        ax=plt.gca()
        ax.semilogy(t,p1,':',lw=3,label="$"+quant+"$")
        ax.semilogy(t,th,lw=3,label="$"+quant+"$"+" (th)")
        ax.set_ylabel(r"|FFT $"+info.grid.label+"$|"+" [$"+info.grid.units+r"\times "+info.grid.axis[0].units+"$]")
        ax.set_xlabel("$t$ "+"$["+info.grid.axis[0].units+"$]")
        ax.set_ylim([ymin,ymax])
        plt.legend()
        plt.show()
    sldr1=widgets.FloatSlider(value=0,min=0,max=tmax,step=(tmax)/nt,orientation='horizontal',readout=True,description='tnint: ')    
    return interact(pltstf,tini=sldr1)
