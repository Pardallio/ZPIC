import os
import ipywidgets as widgets
from ipywidgets import interact, interactive, fixed, interact_manual,Layout

import numpy as np

import matplotlib
from matplotlib import pyplot as plt
from matplotlib import animation

from zdf import read
import matplotlib.patches as patches


if os.name == 'nt':
    bar="\\"
else:
    bar="/"



#an ugly file browser (sorry...)
class FileBrowser(object):
    def __init__(self):
        self.path = os.getcwd()
        self._update_files()
        self.i=0
    def _update_files(self):
        self.files = list()
        self.dirs = list()
        if os.name == 'nt':
            bar="\\"
        else:
            bar="/"        
        if(os.path.isdir(self.path)):
            for f in os.listdir(self.path):
                ff = self.path + bar + f
                if os.path.isdir(ff):
                    self.dirs.append(f)
                else:
                    if ".zdf" in ff:
                        self.files.append(f)
    def widget(self):
        vmbox=widgets.VBox(layout=Layout(width='100%'))
        vvb=widgets.VBox()
        mbox= widgets.HBox(layout=Layout(flex='1 1 auto',width='55%'))
        vb= widgets.VBox(layout=Layout(align_items="center",align_self="stretch",width="25px"))#layout=Layout(display="flex", flex_flow='collumn',align_items='stretch',border='solid'))
        box = widgets.VBox(layout=Layout(flex='1 1 auto',width='auto',border="1.5px solid PALEGOLDENROD"))
        mbox.children= tuple([box,vb])
        vmbox.children= tuple([mbox,vvb])
        self._update(box,vvb,vb)
        
        return vmbox
    
    def _update(self, box,vvb,vb):
        def on_click(b):
            #check for windows (bars are different)                
            if b.description == '..':
                self.path = os.path.split(self.path)[0]
            else:
                self.path = self.path + bar + b.description
            self._update_files()
            self._update(box,vvb,vb)
            
        items_layout = Layout(flex='1 1 auto',width='auto',margin="1px 0px 1px 0px")
        box_layout = Layout(display='flex',flex_flow="row",align_items='stretch',
                    width='100%')

        def clear(b):
            self._clear(box,vvb,vb)        
        buttons = []
        if self.files or 1:
            button = widgets.Button(description='..',layout=items_layout)
            button.style.button_color="MOCCASIN"
            #            b2=widgets.Button(description="",layout=items_layout)
            #b2.style.button_color="MOCCASIN"
            button.on_click(on_click)
            
            hb1=widgets.HBox([button],layout=box_layout)
    
            buttons.append(hb1)

        if len(self.files+self.dirs)<20:
            for f in self.dirs:
                ll=len(f)*8-(f.count('i')+f.count('l')+f.count('.')+f.count('j')+f.count('I'))*4+sum(1 for c in f if c.isupper())*2-(f.count('t')+f.count('r')+f.count('f')+f.count('-'))*2
                #print(f,ll)
                button = widgets.Button(description=f,layout=Layout(width="{}px".format(ll),margin="1px 0px 1px 0px"), background_color='#d0d0ff',    tooltip=f)
                b2=widgets.Button(description="",layout=items_layout)
                button.style.button_color="LEMONCHIFFON"
                b2.style.button_color="LEMONCHIFFON"

                button.on_click(on_click)
                hb1=widgets.HBox([button,b2],layout=box_layout)
                buttons.append(hb1)

            for f in self.files:
                ll=len(f)*8-(f.count('i')+f.count('l')+f.count('.')+f.count('j')+f.count('I'))*4+sum(1 for c in f if c.isupper())*2-(f.count('t')+f.count('r')+f.count('f')+f.count('-'))*2
                button = widgets.Button(description=f,layout=Layout(width="{}px".format(ll),margin="1px 0px 1px 0px"), background_color='#d0d0ff',    tooltip=f)
                b2=widgets.Button(description="",layout=items_layout)
                #button.on_click(on_click)

                hb1=widgets.HBox([button,b2],layout=box_layout)
    
                buttons.append(hb1)

                                
            vb.children=tuple([])

        else:
            def _scroll_dn(b):
                if self.i<len(self.dirs+self.files)-19:
                    self.i=self.i+1;
                    sldr.value=len(self.dirs+self.files)-self.i
                    self._update(box,vvb,vb)
                    
            def _scroll_up(b):
                if self.i>0:
                    self.i=self.i-1;
                    sldr.value=len(self.dirs+self.files)-self.i
                    self._update(box,vvb,vb)

            upbut=widgets.Button(icon="sort-up", button_style='info',layout=Layout(width="20px"))
            dbut=widgets.Button(icon="sort-down", button_style='info',layout=Layout(width="20px"))
            upbut.on_click(_scroll_up)
            dbut.on_click(_scroll_dn)
            sldr=widgets.IntSlider(
                value=len(self.dirs+self.files)-1-self.i,
                min=19,
                max=len(self.dirs+self.files)-1,
                continuous_update=False,
                orientation='vertical',
                readout=False,
                layout=Layout(display="flex",flex="1")
            )
            
            def on_value_change(change):
                self.i=len(self.dirs+self.files)-1-change["new"]
                self._update(box,vvb,vb)
                
            sldr.observe(on_value_change, names='value')
            vb.children=tuple([upbut,sldr,dbut])
            ult_list=self.dirs+self.files
            for f in ult_list[self.i:self.i+19]:
                ll=len(f)*8-(f.count('i')+f.count('l')+f.count('.')+f.count('j')+f.count('I'))*4+sum(1 for c in f if c.isupper())*4-(f.count('t')+f.count('r')+f.count('f')+f.count('-'))*4

                button = widgets.Button(description=f,layout=Layout(width="{}px".format(ll),margin="1px 0px 1px 0px"), background_color='#d0d0ff',    tooltip=f)
                b2=widgets.Button(description="",layout=items_layout)

                if f in self.dirs:
                    button.style.button_color="LEMONCHIFFON"
                    b2.style.button_color="LEMONCHIFFON"

                hb1=widgets.HBox([button,b2],layout=box_layout)
    
                #button.on_click(on_click)
                buttons.append(hb1)

            #buttons.append(widgets.Button(description=str(len(self.files))+" .zdf files in this folder"))
        but=widgets.Button(description="HIDE", button_style='info')
        but.on_click(clear)
        #buttons.append(but)
        if self.files:
            vaal=self.files[0].split('-')[0]
        else:
            vaal=None
        box.children = tuple(buttons)
        vvb.children=tuple([self.data_folder(),self.data_type(vaal)])

    def _clear(self,box,vvb,vb):
        def on_click(b):
            self._update_files()
            self._update(box,vvb,vb)
        buttons=[]
        but=widgets.Button(description="UNHIDE", button_style='info')
        but.on_click(on_click)
        buttons.append(but)
        if self.files:
            vaal=self.files[0].split('-')[0]
        else:
            vaal=None
        vb.children=tuple()
        box.children = tuple(buttons)
        vvb.children=tuple([self.data_folder(),self.data_type(vaal)])
        
        return box

            
    def data_folder(self):
        self.DF=widgets.Text(value=self.path,placeholder='Enter Data Folder Path (the folder where you have your data files)',description='Data Folder:',layout=Layout(width='75%'),disabled=False)
        return self.DF

    def data_type(self,val):
        self.DK=widgets.Text(value=val,placeholder='Enter the key of the data files (e.g. "charge")',description='Data Key:',layout=Layout(width='75%'),disabled=False)
        return self.DK

    def filelist(self):
        return [self.DF.value+bar+i for i in os.listdir(path) if os.path.isfile(os.path.join(path,i)) and self.DK.value in i]


def tabmaker():
    f = FileBrowser()
    f2= FileBrowser()
    
    w0=widgets.Label("Select which data sets you want to plot",layout=Layout(flex='1 1 auto',width='auto'))

    w1=widgets.Checkbox(
        value=True,
        description='Data Set 1',
        disabled=False
    )

    wcp1=widgets.ColorPicker(
        concise=False,
        description='line color',
        value="darkorange",
        disabled=False
    )

    hb1=widgets.HBox([w1,wcp1])
    
    w2=widgets.Checkbox(
        value=False,
        description='Data Set 2',
        disabled=False
    )


    wcp2=widgets.ColorPicker(
        concise=False,
        description='line color',
        value="seagreen",
        disabled=False
    )

    wp2=widgets.Checkbox(
        value=False,
        description='Plasma:',
        disabled=False
    )

    
    w02=widgets.Label("Select the x-axis range",layout=Layout(flex='1 1 auto',width='auto'))

    wxl=widgets.FloatText(
        description='lower limit:',
        value=160,
        disabled=False
    )

    wxu=widgets.FloatText(
        description='upper limit:',
        value=220,
        disabled=False
    )

    hb3=widgets.HBox([wxl,wxu])

    
    hb2=widgets.HBox([w2,wcp2,wp2])
    children = [f.widget(),f2.widget(),widgets.VBox([w0,hb1,hb2,w02,hb3])]
    tab = widgets.Tab()
    tab.children = children
    tab.set_title(0, "Data Set 1")
    tab.set_title(1, "Data Set 2")
    tab.set_title(2, "Settings")
    return tab

    

def pltinteract(filelst1,col1,filelst2=[],col2=None,xlims=None,ylim=None,fils=[],plas=False):
    ylim.clear()
    (data0,inf0)=read(filelst1[0])
    ymin=min(data0)
    ymax=max(data0)
    
    ylimin=ymin-(ymax-ymin)/10
    ylimax=ymax+(ymax-ymin)/10
    ylims=[ylimin,ylimax]
    ylims2=[0,0]
    ylim.append(ylims)

    if filelst2:
        (data02,inf02)=read(filelst2[0])
        ymin2=min(data02)
        ymax2=max(data02)

        ylimin2=ymin2-(ymax2-ymin2)/10
        ylimax2=ymax2+(ymax2-ymin2)/10
        ylims2=[ylimin2,ylimax2]
        ylim.append(ylims2)
    def pltstf(itr,fil,col,fil2=[],col2=None,xlims=None,ylims=None,fils=[],plasm=False):
        (data, info) = read(fil[itr])
        fils.clear()
        fils.append(fil[itr])
        xmin=info.grid.axis[0].min
        xmax=info.grid.axis[0].max
        npts=info.grid.nx[0]
        global ymin
        global ymax
        ymin=min(data)
        ymax=max(data)
        ylimin=ymin-(ymax-ymin)/10
        ylimax=ymax+(ymax-ymin)/10

        if ylimin<ylims[0][0]:
            ylims[0][0]=ylimin

        if ylimax>ylims[0][1]:
            ylims[0][1]=ylimax

        
        xx=np.linspace(xmin,xmax,npts)
                
        xmin=xlims[0];
        xmax=xlims[1];
        
        fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')

        ax1=plt.gca()
        lines=[]
        lines.append(ax1.plot(xx,data,c=col,lw=3.5,label="$"+info.grid.label+"$")[0]);
        #lines.append(ax1.plot(xx,data,'+',c="coral")[0]);
        ax1.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$\\times"+info.iteration.tunits+"$",loc='right')
        ax1.set_xlim(xlims)
        ax1.set_ylim(ylims[0])
        ax1.set_xlabel("$"+info.grid.axis[0].label+"$"+" ($"+info.grid.axis[0].units+"$)")
        ax1.set_ylabel("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")
        if plasm and not fil2:
            ax1.fill_between(xx,data,0,alpha=0.4,color=col)
        if fil2:
            (data2, info2) = read(fil2[itr])
            fils.append(fil2[itr])
            ax2=ax1.twinx()
            
            ymin2=min(data)
            ymax2=max(data)
            ylimin2=ymin2-(ymax2-ymin2)/10
            ylimax2=ymax2+(ymax2-ymin2)/10

            if ylimin2<ylims[1][0]:
                ylims[1][0]=ylimin2
            
            if ylimax2>ylims[1][1]:
                ylims[1][1]=ylimax2

            ax2.set_xlim(xlims)
            ax2.set_ylim(ylims[1])
            ax2.set_xlabel("$"+info2.grid.axis[0].label+"$"+" ($"+info2.grid.axis[0].units+"$)")
            ax2.set_ylabel("$"+info2.grid.label+"$"+" ($"+info2.grid.units+"$)")
            ax2.plot(xx,data2,c=col2,lw=3.5,label="$"+info2.grid.label+"$")
            ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))
            if plasm:
                ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
            #lin[0].set_data(xx,data)
            #lin[1].set_data(xx,data)
        
            #xx=np.transpose(xx)
            #print(xx,len(xx))
            ax1.legend(loc=0)
        plt.show()
        
    sldr=widgets.IntSlider(value=0,min=0,max=len(filelst1)-1,step=1,orientation='horizontal',readout=False,description=' ')
    buuuu=widgets.Button()
    return interact(pltstf,itr=sldr,fil=fixed(filelst1),fil2=fixed(filelst2),col=fixed(col1),col2=fixed(col2),xlims=fixed(xlims),ylims=fixed(ylim),fils=fixed(fils),plasm=fixed(plas))



def shower(tab,yl,fils):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value

    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value
    
    xlims=[i.value for i in   tab.children[2].children[4].children]

    pl2=tab.children[2].children[2].children[2].value
    
    if bool1 and bool2:
        fldr=tab.children[0].children[1].children[0].value
        key=tab.children[0].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    
        fldr=tab.children[1].children[1].children[0].value
        key=tab.children[1].children[1].children[1].value
        files2 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        return [pltinteract(files,col1,files2,col2,xlims,yl,fils,plas=pl2),files,files2]
    
    elif bool1 or bool2:    
        fldr=tab.children[int(bool2)].children[1].children[0].value
        key=tab.children[int(bool2)].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]

        col=tab.children[2].children[int(bool2)+1].children[1].value
        return [pltinteract(files,col,xlims=xlims,ylim=yl,fils=fils,plas=(pl2 and bool2)),files]
    

def filer(tab):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value

    xlims=[i.value for i in   tab.children[2].children[4].children]
    
    if bool1 and bool2:
        fldr=tab.children[0].children[1].children[0].value
        key=tab.children[0].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    
        fldr=tab.children[1].children[1].children[0].value
        key=tab.children[1].children[1].children[1].value
        files2 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    
        return [files,files2,xlims]
    
    elif bool1 or bool2:    
        fldr=tab.children[int(bool2)].children[1].children[0].value
        key=tab.children[int(bool2)].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        return [files,None,xlims]


def animate(tab,yl):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value
    
    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value
    
    xlims=[i.value for i in   tab.children[2].children[4].children]

    
    fil=filer(tab)
    
    pl2=tab.children[2].children[2].children[2].value
    # First set up the figure, the axis, and the plot element we want to animate
    fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')

    if bool2 and bool1:
        (data, info) = read(fil[0][0])
        xmin=info.grid.axis[0].min
        xmax=info.grid.axis[0].max
        npts=info.grid['nx'][0]
        xx=np.linspace(xmin,xmax,npts)

        (data2, info2) = read(fil[1][0])
        
        ax = plt.axes()
        ax.set_xlim(xlims)
        ax.set_ylim(yl[0])
        ax2 = ax.twinx()
        ax2.set_ylim(yl[1])
        lines =[]
        lines.append(ax.plot([], [], lw=2,c=col1,label="$"+info.grid.label+"$")[0])
        lines.append(ax2.plot([], [], lw=2,c=col2,label="$"+info2.grid.label+"$")[0])
        ax.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$\\times"+info.iteration.tunits+"$",loc='right')
        ax.set_xlabel("$"+info.grid.axis[0].label+"$"+" ($"+info.grid.axis[0].units+"$)")
        ax.set_ylabel("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")
        ax2.set_ylabel("$"+info2.grid.label+"$"+" ($"+info2.grid.units+"$)")
        ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))
        if pl2:
            ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
        ax.legend(loc=0)
        
    elif bool1 or bool2:
        (data, info) = read(fil[bool2][0])
        xmin=info.grid.axis[0].min
        xmax=info.grid.axis[0].max
        npts=info.grid['nx'][0]
        xx=np.linspace(xmin,xmax,npts)

        ax = plt.axes(xlim=xlims, ylim=yl[bool2])
        lines =[]
        lines.append(ax.plot([], [], lw=2)[0])
        ax.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$\\times"+info.iteration.tunits+"$",loc='right')
        ax.set_xlabel("$"+info.grid.axis[0].label+"$"+" ($"+info.grid.axis[0].units+"$)")
        ax.set_ylabel("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")
        if pl2 and not bool1:
            ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
        
    # initialization function: plot the background of each frame
    def init():
        for line in lines:
            line.set_data([], [])
        ax.set_xlim(xlims)
        return lines

    # animation function.  This is called sequentially
    def animate(i,files):
        if bool1 and bool2:
            (data, info) = read(files[0][i])
            ax.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$\\times"+info.iteration.tunits+"$",loc='right')
            x = xx
            y = data            
            lines[0].set_data(x, y)
            (data, info) = read(files[1][i])
            y = data            
            lines[1].set_data(x,y)
        elif bool1 or bool2:
            (data, info) = read(files[bool2][i])
            x = xx
            y = data            
            lines[0].set_data(x, y)
            
        return lines

    # call the animator.  blit=True means only re-draw the parts that have changed.
    return animation.FuncAnimation(fig, animate,fargs=(fil,), init_func=init,
                               frames=len(fil[0]), interval=20, blit=True)

def regselect(maxx,itr,w,file1,file2,datav,sldr):
    sldr.max=maxx
    datav.clear()
    #read data from file 1 (density file)
    (dens,info) = read(file1)
    
    #build the x axis array
    xmin=info.grid.axis[0].min
    xmax=info.grid.axis[0].max
    npts=info.grid.nx[0]
    xx=np.linspace(xmin,xmax,npts)
    
    #find the begining of the plasma
    itl=0
    for i, xi in enumerate(dens[1:]): 
        if xi < 0:
            itl=i-1
            break
    xmin=xx[itl];
            
    #read the data from file2 (EMF file)
    (data, info) = read(file2)
    
    #initialize the figure
    fig=plt.figure(figsize=(16, 9), dpi= 90, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    ax1=plt.gca()
    ax1.plot(xx,data,'.',c="darkorange",lw=3.5,label="$"+info.grid.label+"$");
    
    ax1.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$\\times"+info.iteration.tunits+"$",loc='right')
    ax1.set_xlim([xmin,maxx])
    ax1.set_xlabel("$"+info.grid.axis[0].label+"$"+" ($"+info.grid.axis[0].units+"$)")
    ax1.set_ylabel("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")
    
    ax1.legend(loc=0)
    #plot the trasition region
    plt.axvline(x=itr,color='k')
    ymin=min(data)
    ymax=max(data)
    ylimin=ymin-(ymax-ymin)/10
    ylimax=ymax+(ymax-ymin)/10
    ax1.add_patch(patches.Rectangle((itr-w/2, ylimin),w,ylimax-ylimin,alpha=0.5))

    for i, xi in enumerate(xx):
        if xi > maxx:
            itu=i
            break

    nxx=np.array(xx[itl:itu])-xmin
    ndata=data[itl:itu]

    i=0
    while nxx[i]<itr-w/2-xmin:
        i=i+1
    itll=i

    while nxx[i]<itr+w/2-xmin:
        i=i+1
    ituu=i

    nxx=list(nxx)
    ndata=list(ndata)
    del nxx[itll:ituu]
    del ndata[itll:ituu]

    datav.append(nxx)
    datav.append(ndata)
    datav.append(info)
    
    plt.show()
    
def intregsel(file1,file2,datav):
    (data0,inf0)=read(file1)

    #build the x axis array
    xmin=inf0.grid.axis[0].min
    xmax=inf0.grid.axis[0].max
    npts=inf0.grid.nx[0]
    xx=np.linspace(xmin,xmax,npts)


    itl=0
    for i, xi in enumerate(data0[1:]): 
        if xi < 0:   #check charge value!!!!!!!!!!#
            itl=i
            break
    xmin=xx[itl]
        
    sldr1=widgets.FloatSlider(value=(xmin+xmax)/2,min=xmin,max=xmax,orientation='horizontal',description='x-axis limit:')        
    sldr2=widgets.FloatSlider(value=(xmin+sldr1.value)/2,min=xmin,max=sldr1.value,orientation='horizontal',description='turning point:')
    sldr3=widgets.FloatSlider(value=(xmax-xmin)/npts*5,min=(xmax-xmin)/(npts-itl),max=(xmax-xmin)/10,orientation='horizontal',description='bar witdh:')
    
    return interact(regselect,maxx=sldr1,itr=sldr2,w=sldr3,file1=fixed(file1),file2=fixed(file2),datav=fixed(datav),sldr=fixed(sldr2))

from scipy.optimize import curve_fit

def fiter(datav,param,Emfun,save=False):
    plt.clf()
    plt.cla()
    plt.close()
    datavec=datav
    fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    info=datav[2]
    ax1=plt.gca()
    
        #lines.append(ax1.plot(xx,data,'+',c="coral")[0]);
    ax1.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$\\times"+info.iteration.tunits+"$",loc='right')
    #ax1.set_xlim([xmin,xmax])
    ax1.set_xlabel("$"+info.grid.axis[0].label+"-x_{tp}$"+" ($"+info.grid.axis[0].units+"$)")
    ax1.set_ylabel("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")

    par=[i.value for i in param]
    popt, pcov = curve_fit(Emfun, datavec[0], datavec[1],p0=par)
    ax1.plot(datavec[0]-popt[1],datavec[1],'.',c="dodgerblue",ms=10,label="$"+info.grid.label+"$");

    for i,b in enumerate(popt):
        param[i].value=b
    ax3=plt.gca()
    ax3.plot(datavec[0]-popt[1], Emfun(datavec[0], *popt), '-',c="darkviolet", label='fit')[0]
    #ax3.plot(nxx, Emfun(nxx, 1.3,30.1,-2.835e-10), 'b-', label='fit')[0]
    #ax3.set_ylim([-1.5e-10,1.5e-10])
    plt.legend()
    plt.show()
    if save:
        fig.savefig("fit.pdf")

def paramaker():
    a = widgets.FloatText(value=1,description='$\left(\omega/c\sqrt{L}=\beta\\right)^{2/3}$:')
    b = widgets.FloatText(value=1,description='$L$:')
    c = widgets.FloatText(value=1,description='$E_0$:')
    return (widgets.VBox([a,b,c]),[a,b,c])

def dircompars():
    om = widgets.FloatText(value=1,description='$\omega$')
    sl = widgets.FloatText(value=1,description='$slope$:')
    ao = widgets.FloatText(value=1,description='$a_0$:')
    return (widgets.VBox([om,sl,ao]),[om,sl,ao])


def dircomp(datav,param,Emfun,save=False):
    plt.clf()
    plt.cla()
    plt.close()
    datavec=datav
    fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    info=datav[2]
    ax1=plt.gca()
    
        #lines.append(ax1.plot(xx,data,'+',c="coral")[0]);
    ax1.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$\\times"+info.iteration.tunits+"$",loc='right')
    #ax1.set_xlim([xmin,xmax])
    ax1.set_xlabel("$"+info.grid.axis[0].label+"-x_{tp}$"+" ($"+info.grid.axis[0].units+"$)")
    ax1.set_ylabel("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")

    L=(param[0].value**2)/param[1].value
    A=(param[0].value/np.sqrt(L))**(2/3)
    E0=(param[2].value*param[1].value)
    
    par=[A,L,E0]
    popt, pcov = curve_fit(lambda x,E0:Emfun(x,par[0],par[1],E0), datavec[0], datavec[1],p0=par[2])

    ax1.plot(np.array(datavec[0])-par[1],datavec[1],'.',c="dodgerblue",ms=10,label="$"+info.grid.label+"$");
    par[2]=popt[0]
    ax3=plt.gca()
    ax3.plot(np.array(datavec[0])-par[1], Emfun(datavec[0], *par), '-',c="darkviolet",lw=2, label='fit')[0]
    plt.legend()
    plt.show()
    if save:
        fig.savefig("dcomp.pdf")
    
