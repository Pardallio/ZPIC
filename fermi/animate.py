### ANIMATION SET UP

from ipywidgets import widgets,interact,fixed
from IPython.display import display
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation
from scipy.integrate import odeint #probably needs to be installed as well
import loopfield as lf #better install this module before running
from pylab import *

from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

from magbot import Arrow3D


def anim(data, coil):
    a=coil
    ### ANIMATION SET UP
    def update_lines(num, dataLines, lines) :
        #for line, data in zip(lines, dataLines) :
        # NOTE: there is no .set_data() for 3 dim data...
        xd=0
        if num>55:
            xd=num-55
        
        
        for ilin in range(int(len(lines)/2)):
            dat1=data[ilin]
            lines[2*ilin].set_data(dat1[0:2, xd:num])
            lines[2*ilin].set_3d_properties(dat1[2,xd:num])
            lines[2*ilin+1].set_data(dat1[0:2, num-1:num])
            lines[2*ilin+1].set_3d_properties(dat1[2,num-1:num])
        
        
        
        if a.curr>0:
            ax.plot(cir[0][4:21],cir[1][4:21],cir[2][4:21],lw=1.5,c='red')
            ax.plot(cir2[0][4:21],cir2[1][4:21],cir2[2][4:21],lw=1.5,c='red')
        else:
            ax.plot(cir[0][11:26],cir[1][11:26],cir[2][11:26],lw=1.5,c='red')
            ax.plot(cir2[0][11:26],cir2[1][11:26],cir2[2][11:26],lw=1.5,c='red')
        return lines
    
    if len(data)<=4:
        plt.rc('axes',prop_cycle=(cycler('color', ['dodgerblue', 'b', 'darkorange', 'orangered','limegreen','forestgreen','mediumorchid','blueviolet'])))
    else:
        plt.rc('axes',prop_cycle=(cycler('color', ['dodgerblue', 'b'])))

    fig = plt.figure(figsize=(12, 6.75), dpi=80)
    ax = p3.Axes3D(fig)

    cir,cir2=a.calc_circ()
    
    ax.plot(cir[0],cir[1],cir[2],lw=1.5,c='red')
    ax.plot(cir2[0],cir2[1],cir2[2],lw=1.5,c='red')
    lines=[]
    for dat in data:
        lines.append(ax.plot(dat[0, 0:1], dat[1, 0:1], dat[2, 0:1])[0])
        lines.append(ax.plot(dat[0, 0:1], dat[1, 0:1], dat[2, 0:1],'o-')[0])
    
    
    if a.curr != 0:
        cir=np.transpose(cir)
        cir2=np.transpose(cir2)
        ra=cir[int(len(cir)/2)]
        rb=cir[int(len(cir)/2)+1]
        ar = Arrow3D([ra[0],rb[0]],[ra[1],rb[1]],[ra[2],rb[2]], mutation_scale=10,lw=1, arrowstyle="-|>", color="red")
        ax.add_artist(ar)
        ra=cir2[int(len(cir2)/2)]
        rb=cir2[int(len(cir2)/2)+1]
        br = Arrow3D([ra[0],rb[0]],[ra[1],rb[1]],[ra[2],rb[2]], mutation_scale=10,lw=1, arrowstyle="-|>", color="red")
        ax.add_artist(br)

        # Setting the axes properties
        #ax.set_xlim3d([-0.2, 2.2])
        #ax.set_xlabel('X')

        #ax.set_ylim3d([2, 4])
        #ax.set_ylabel('Y')

        #ax.set_zlim3d([2, 4])
        #ax.set_zlabel('Z')

        #ax.set_title('3D Test')


    # Creating the Animation object
    line_ani = animation.FuncAnimation(fig, update_lines,500, fargs=(data, lines),
                                   interval=50, blit=False)
    
    return line_ani


def animmu(coil,r,v):
    a=coil
    i=0
    vp2=[]
    Barr=[]
    muplt=[]
    while i<len(r[0]):
        vp2.append(v[1][i]**2+v[2][i]**2)
        Barr.append(np.linalg.norm(a.get_Bf([r[0][i],r[1][i],r[2][i]])))
        muplt.append(vp2[i]/(2*Barr[i]))
        i=i+1

    minx=min(r[0])
    maxx=max(r[0])

    minv=min(vp2)
    maxv=max(vp2)
    minB=min(Barr)
    maxB=max(Barr)
    minm=min(muplt)
    maxm=max(muplt)
    
    lines=[]
    fig2=plt.figure(figsize=(16, 4), dpi= 80, facecolor='w', edgecolor='k');
    ax1=plt.subplot(1,3,1);
    lines.append([])
    lines[0].append(ax1.plot([],[],c="k")[0]);
    lines[0].append(ax1.plot([],[],'o',c="orangered")[0]);
    lines[0].append(ax1.plot([],[],c="orangered")[0]);
    ax1.set_title("$v_\perp^2$")
    #ax1.set_xlim(0,9)
    #ax1.set_ylim(0,20)
    
    
    ax1.set_xlim([minx-(maxx-minx)/20, maxx+(maxx-minx)/20])
    ax1.set_ylim([minv-(maxv-minv)/20, maxv+(maxv-minv)/20])
    
    ax2=plt.subplot(1,3,2);
    lines.append([])
    lines[1].append(ax2.plot([],[],c="k")[0]);
    lines[1].append(ax2.plot([],[],'o',c="deepskyblue")[0]);
    lines[1].append(ax2.plot([],[],c="deepskyblue")[0]);
    ax2.set_title("$B$")
    ax2.set_xlim([minx-(maxx-minx)/20, maxx+(maxx-minx)/20])
    ax2.set_ylim([minB-(maxB-minB)/20, maxB+(maxB-minB)/20])
    
    
    ax3=plt.subplot(1,3,3);
    lines.append([])
    lines[2].append(ax3.plot([],[],c="k")[0]);
    lines[2].append(ax3.plot([],[],'o',c="blueviolet")[0]);
    lines[2].append(ax3.plot([],[],c="fuchsia")[0]);
    
    ax3.set_xlim([minx-(maxx-minx)/20, maxx+(maxx-minx)/20])
    ax3.set_ylim([minm-(maxm-minm)/20, maxm+(maxm-minm)/20])
    ax3.set_title("$\mu=mv_\perp^2/2B$")
    
    def update_lines2(num, data, lines) :
        #for line, data in zip(lines, dataLines) :
        # NOTE: there is no .set_data() for 3 dim data...
        xd=0
        if num>50:
            xd=num-50
        
        for ilin in np.arange(len(lines)):
            #print(data[ilin][:2, xd:num],ilin)
            lines[ilin][0].set_data(data[ilin][:2, :num]);
            lines[ilin][1].set_data(data[ilin][:2, num-1:num]);
            lines[ilin][2].set_data(data[ilin][:2, xd:num]);
        
        return lines


    dat1=np.array([[r[0],vp2],[r[0],Barr],[r[0],muplt]])
    #print(dat1[0][:2, 0:50])
    return animation.FuncAnimation(fig2, update_lines2,len(r[0]), fargs=(dat1, lines),
                                   interval=50, blit=False);
