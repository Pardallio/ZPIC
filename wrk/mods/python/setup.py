from distutils.core import setup, Extension
from Cython.Build import cythonize

from distutils import sysconfig

# Removes the debug options and replaces the optimization flag
cflags = sysconfig.get_config_var('CFLAGS')
cflags = cflags.replace(' -g', '')
cflags = cflags.replace(' -DNDEBUG', '')
cflags = cflags.replace('-O3', '-Ofast')

# Sets the standard to c99
cflags += ' -std=c99'

sysconfig._config_vars['CFLAGS'] = cflags



# Electrostatic (spectral) codes
es1d = Extension("es1d",
                sources=["es1d.pyx",
				"../../es1d/charge.c",
				"../../es1d/fft.c",
				"../../es1d/field.c",
				"../../es1d/grid.c",
				"../../es1d/particles.c",
				"../../es1d/random.c",
				"../../es1d/simulation.c",
				"../../es1d/timer.c",
				"../../es1d/zdf.c"])


setup(name="zpic",
      ext_modules = cythonize([es1d]))
