import pip

def install(package,option=''):
    pip.main(['install',option, package])

# Example
if __name__ == '__main__':
    #install('pip','--upgrade --user')
    try:
        import numpy
    except ImportError:
        print(" Installing NumPy...")
        install('numpy','--user')
        
    try:
        import scipy
    except ImportError:
        print(" Installing SciPy...")
        install('scipy','--user')

    try:
        import matplotlib
    except ImportError:
        print(" Installing matplotlib...")
        install('matplotlib','--user')

    try:
        import jupyter
    except ImportError:
        print(" Installing Jupyter...")
        install('jupyter','--user')

    try:
        import ipywidgets
    except ImportError:
        print(" Installing ipywidgets...")
        install('ipywidgets','--user')

    try:
        import widgetsnbextension
    except ImportError:
        print(" Installing widgetnbextension...")
        install('widgetsnbextension','--user')

    try:
        import cython
    except ImportError:
        print(" Installing cython...")
        install('cython','--user')

    print("\n Done installing...")
