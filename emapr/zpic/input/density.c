#include "simulation.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

#include "emf.h"

float custom_n0( float x ) {
    return 1.0 + 0.5*sin(x/M_PI)*sin(x/M_PI);
}

void sim_init( t_simulation* sim ){
    // Time step
    float dt = 0.028;
    float tmax = 170.00;
    
    // Simulation box
    int   nx  = 8000;
    float box = 225.0;
    
    // Diagnostic frequency
    int ndump = 10;
    
    // Initialize particles
    const int n_species = 1;
    
    // Use 128 particles per cell
    int ppc = 8;
    
    // Density profile
    //	t_density density = { .type = UNIFORM };
    //	t_density density = { .type = STEP, .start = 17.5 };
    //	t_density density = { .type = SLAB, .start = 17.5, .end = 22.5 };
    t_density density = { .type = RAMP, .start = 180, .end = 305, .ramp = {0.0, 150.} };
    //	t_density density = { .type = CUSTOM, .custom = &custom_n0 };
    
    t_species* species = (t_species *) malloc( n_species * sizeof( t_species ));
    spec_new( &species[0], "electrons", -1.0, ppc, NULL, NULL, nx, box, dt, &density );
    // Initialize Simulation data
    sim_new( sim, nx, box, dt, tmax, ndump, species, n_species );
    
    // Add laser pulse (this must come after sim_new)
    t_emf_laser laser = {
        .start =175.0,
        .rise=25,.flat=145,.fall=5,
        .a0 = 0.000001,
        .omega0 = 6,
        .polarization = M_PI_2
    };
    sim_add_laser( sim, &laser );
    sim -> emf.bc_type = EMF_BC_OPEN;
    sim -> emf.bc_type = EMF_BC_OPEN;
    
    
    // Set moving window (this must come after sim_new)
    //sim_set_moving_window( sim );
    
}


void sim_report( t_simulation* sim ){
    
    // All electric field components
    //	emf_report( &sim->emf, EFLD, 0 );
    emf_report( &sim->emf, EFLD, 2 );
    //	emf_report( &sim->emf, EFLD, 2 );
    
    // Charge density
    spec_report( &sim->species[0], CHARGE, NULL, NULL );
    
    // RAW dump
    //	spec_report( &sim->species[0], PARTICLES, NULL, NULL );
    
}
