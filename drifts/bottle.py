#matplotlib.use('PDF')

#import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import odeint
import loopfield as lf 


# create empty field with specified units 
field = lf.Field(length_units = lf.cm, 
current_units = lf.A, 
field_units = lf.uT) 

# single-turn 10 cm x-oriented coil at origin 
position = [0, 3., 3.] 
normal = [-1, 0., 0.] 
radius = 1. 
current = 1. 
c = lf.Loop(position, normal, radius, current) 


# add loop to field 
field.addLoop(c); 

position = [2., 3.,3.] 
normal = [-1, 0., 0] 
radius = 1. 
current = 1. 
d = lf.Loop(position, normal, radius, current) 
field.addLoop(d); 

# evaluate vector field at origin 
B = field.evaluate([0., 3, 3.]) 
print('B = ', B) 

def Ef(r):
    Ex=0;
    Ey=0;
    Ez=0;
    return (Ex,Ey,Ez)

def Bf(r):
    #Bx=-r[0]/2
    #By=-r[1]/2
    #Bz=(10+1*r[2]**2);
    return field.evaluate(r)

def f(Q,t,qm):
    r=Q[:3]
    v=Q[3:]
    drdt=v
    dvdt=qm*(Ef(r)+np.cross(v,Bf(r)))
    return np.concatenate((v,dvdt))

def solve(ri,vi,qm):
    Q0=np.concatenate((ri,vi))
    tf = 20; NPts = 1000
    t = np.linspace(0,tf,NPts)
    Qe = odeint(f, Q0, t, args=(qm,))
    return Qe


Q=solve((0.5,3,3),(-1,2,2),-1)

Q=np.transpose(Q)

r=Q[:3]

import matplotlib
matplotlib.use('PDF')

import matplotlib.pyplot as plt


def circ(y):
    x=np.sqrt(1-(y-2)**2)

cir=[]
i=0
while i<101:
    y=3+np.sin(2*3.14*(i/100))
    x=3+np.cos(2*3.14*(i/100))
    c=[0,x,y]
    #print(c)
    cir.append(c)
    i=i+1

cir2=[]
i=0
while i<101:
    y=3+np.sin(2*3.14*(i/100))
    x=3+np.cos(2*3.14*(i/100))
    c=[2,x,y]
    #print(c)
    cir2.append(c)
    i=i+1


    
#ax.plot(r[0],r[1],lw=0.5)
from mpl_toolkits.mplot3d import Axes3D


fig = plt.figure()
ax=fig.gca(projection='3d')
#ax = fig.add_subplot(111)
#fig.subplots_adjust(top=0.85)
#ax.set_title('Some drift')

ax.set_xlabel('x ($cm$)')
ax.set_ylabel('y ($cm$)')
ax.set_zlabel('z ($cm$)')

#ax.add_patch(p)
#art3d.pathpatch_2d_to_3d(p, z=0, zdir="x")



#r=np.transpose(r)
#print(r)
cir=np.transpose(cir)
cir2=np.transpose(cir2)

ax.plot(cir[0],cir[1],cir[2],lw=1.5,c='red')


ax.plot(r[0],r[1],r[2],lw=0.5)

ax.scatter(r[0,0],r[1,0],r[2,0],c='red')
ax.plot(cir2[0],cir2[1],cir2[2],lw=1.5,c='red')

#p = Circle((3, 3), 3)
#ax.add_patch(p)
#art3d.pathpatch_2d_to_3d(p, z=0, zdir="x")

ax.set_xlim([-0.001,2.1])
#ax.set_ylim([2.95,3.05])
#ax.set_zlim([2.95,3.05])


#ax.set_ylim([ymin,ymax])

#print(r)
fig.savefig('drft4.pdf')

#plt.show()


