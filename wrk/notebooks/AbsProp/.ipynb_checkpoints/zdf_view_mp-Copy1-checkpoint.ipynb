{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Raman Scattering\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Raman scattering is a parametric instability that arises when an electromagnetic wave excites a plasma wave with a given frequency $\\omega$.\n",
    "\n",
    "The equations that rule this interaction, shown below, couple the laser pulse (represented by the normalized vector potential $a_z$) and the plasma wave ($n_1$).\n",
    "\n",
    "$$\\left( \\frac{\\partial^2}{\\partial t^2}+\\omega_p^2\\right) \\frac{n_1}{n_0}=\\frac{c^2}{2}\\frac{\\partial^2}{\\partial x^2}a_z^2 $$\n",
    "\n",
    "$$ \\left( \\frac{1}{c^2}\\frac{\\partial^2}{\\partial t^2}-\\frac{\\partial^2}{\\partial x^2}\\right)a_z=-\\frac{\\omega_p^2}{c^2}\\left(1+\\frac{n_1}{n_0}-\\frac{a_z^2}{2}\\ \\right)a_z$$\n",
    "\n",
    "If the laser pulse is composed of 3 waves $a_+$ (Anti-Stokes), $a_-$ (Stokes) and $a_0$ (Pump), with frequencies $\\omega_+$, $\\omega_-$ and $\\omega_0$, respectively, the matching conditions dictate that the frequencies and  obey the following relations:\n",
    "\n",
    "$\\omega_+=\\omega+\\omega_0$<br>\n",
    "$\\omega_-=\\omega-\\omega_0$<br>\n",
    "$k_-=k-k_0$<br>\n",
    "$k_+k+k_0$\n",
    "\n",
    "The dispersion relation for these 4 waves (the 3 EM waves and the plasma wave) is given by the following set of equations:\n",
    "\n",
    "$$\\left(-\\omega^2+\\omega_p^2+3v_{th}^2k^2\\right)\\frac{n1}{n_0}\\equiv D \\frac{n_1}{n_0}=-\\frac{k^2}{2}\\left(a_+a_0^*+a_-a_0\\right)$$\n",
    "\n",
    "$$\\left(-\\omega_0^2+\\omega_p^2+k_0^2\\right)a_0\\equiv D_0 A_0=-\\frac{\\omega_p^2}{2c^2}\\left(\\frac{n_1^*}{n_0}a_++\\frac{n_1}{n_0}a_-\\right)$$\n",
    "\n",
    "$$\\left(-\\omega_-^2+\\omega_p^2+k_-^2\\right)a_-\\equiv D_- A_-=-\\frac{\\omega_p^2}{c^2}\\frac{n_1}{n_0}\\frac{a_0^*}{2}$$\n",
    "\n",
    "$$\\left(-\\omega_+^2+\\omega_p^2+k_+^2\\right)a_+\\equiv D_+ A_+=-\\frac{\\omega_p^2}{c^2}\\frac{n_1^*}{n_0}\\frac{a_0}{2}$$\n",
    "\n",
    "\n",
    "## Assumptions\n",
    "\n",
    "If we assume that the plasma waves frequency is close to the plasma frequency ($\\omega\\simeq\\omega_p$), because the frequency of the plasma oscillations is mostly due to the immobile background plasma, and that the pump's frequency is much greater than the plasma frequency ($\\omega \\gg \\omega_p$) -underdense regime. We can retrieve conditions for the waves that\n",
    "participate in the raman scattering process.\n",
    "\n",
    "Under these assumptions, we can conclude that:\n",
    "\n",
    "$\\omega_-\\simeq-\\omega_0$<br>\n",
    "$\\omega_+\\simeq\\omega_0$\n",
    "\n",
    "In order for the instabilit to grow we will require a resonating Stokes wave $\\left(D_-\\simeq 0\\right)$\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "D_-\\simeq 0&\\Rightarrow -\\omega_-^2+\\omega_p^2+k_-^2\\simeq 0\\\\\n",
    "&\\Rightarrow -\\omega_0^2+\\omega_p^2+k_-^2\\simeq 0\\\\\n",
    "&\\Rightarrow \\left|k_-\\right|\\simeq k_0\\\\\n",
    "&\\Rightarrow k_-\\simeq k_0 \\lor k_-\\simeq -k_0\\\\\n",
    "&\\Rightarrow k\\simeq 2k_0 \\lor k\\simeq 0 \\left(\\text{or }k\\ll k_0 \\right)\\\\\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "Up to this point we have concluded that: $\\omega_-\\simeq-\\omega_0$, $\\omega_+\\simeq\\omega_0$ and that there are two options for $k$\n",
    "\n",
    "## Cases\n",
    "\n",
    "### Case 1: $k\\simeq 2k_0$\n",
    "\n",
    "> $k\\simeq 2k_0$<br>\n",
    "> $\\Rightarrow k_+\\simeq 3k_0$<br>\n",
    "> $\\Rightarrow k_-\\simeq k_0$<br>\n",
    "\n",
    "In this case we can see that the anti-stokes wave is non resonant as $D_+\\gg 1$, this way, the anti-stokes wave does not grow.\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "D_+&=-\\omega_+^2+\\omega_p^2+k_+^2c^2\\\\\n",
    "&=-\\omega_0^2+\\omega_p^2+k_+^2c^2\\\\\n",
    "&=-k_0^2c^2+\\left(3k_0\\right)^2c^2\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "From the sign of the wavevectors and the wave's frequency we can infer the propagation direction:\n",
    "\n",
    "| Wave | Wavenumber  $\\qquad$    | Frequency $\\;\\;\\qquad$ | Propagation direction\n",
    "| --- | --- | --- | --- |\n",
    "| Pump | $k_0>0$ | $\\omega_0>0$ | Forward\n",
    "| Anti-Stokes | $k_+\\simeq 3k_0>0$ | $\\omega_+\\simeq \\omega_0>0$ | Forward (Non-resonant)\n",
    "| Stokes | $k_-\\simeq k_0\\gg0$ | $\\omega_-\\simeq -\\omega_0<0$ | Backward\n",
    "| Plasma | $k\\simeq 2k_0>0$ | $w\\simeq\\omega_p>0$ | Forward\n",
    "\n",
    "<img src=\"rbs.png\" width=\"300\">\n",
    "\n",
    "This case corresponds to a 3-wave process called Raman backscattering.\n",
    "\n",
    "> **Growth Rate: **\n",
    "> The growth rate for this instability is given by:\n",
    "> $\\gamma={\\large \\frac{a_0\\sqrt{\\omega_0\\omega_p}}{2}}$\n",
    "\n",
    "### Case 2: $k\\simeq 0 \\left(\\text{or }k\\ll k_0 \\right)$\n",
    "\n",
    "$k\\simeq 0$<br>\n",
    "$\\Rightarrow k_+\\simeq k_0$<br>\n",
    "$\\Rightarrow k_-\\simeq -k_0$<br>\n",
    "\n",
    "In this case we can see that the anti-stokes wave is also resonant as $D_+\\simeq 0$, this way, the anti-stokes wave also grows.\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "D_+&=-\\omega_+^2+\\omega_p^2+k_+^2c^2\\\\\n",
    "&=-\\omega_0^2+\\omega_p^2+k_+^2c^2\\\\\n",
    "&=-k_0^2c^2+k_0^2c^2\n",
    "\\end{align}\n",
    "$$\n",
    "\n",
    "From the sign of the wavevectors and the wave's frequency we can infer the propagation direction:\n",
    "\n",
    "| Wave | Wavenumber  $\\qquad$    | Frequency $\\;\\;\\qquad$ | Propagation direction\n",
    "| --- | --- | --- | --- |\n",
    "| Pump | $k_0>0$ | $\\omega_0>0$ | Forward\n",
    "| Anti-Stokes | $k_+\\simeq k_0>0$ | $\\omega_+\\simeq \\omega_0>0$ | Forward \n",
    "| Stokes | $k_-\\simeq -k_0<0$ | $\\omega_-\\simeq -\\omega_0<0$ | Forward\n",
    "| Plasma | $k\\simeq 0>0$ | $w\\simeq\\omega_p>0$ | Forward\n",
    "\n",
    "<img src=\"rfs.png\" width=\"300\">\n",
    "\n",
    "This case corresponds to a 4-wave process called Raman Forward Scattering.\n",
    "\n",
    "> **Growth Rate: **\n",
    "> The growth rate for this instability is given by:\n",
    "> $\\gamma={\\large \\frac{a_0\\omega_p^2}{\\sqrt{8}\\omega_0}}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "ename": "AttributeError",
     "evalue": "module 'em1d' has no attribute 'Species'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mAttributeError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-3-65f7d036ce1a>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     14\u001b[0m \u001b[0muth\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;34m[\u001b[0m\u001b[0;36m0.139890928\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;36m0.139890928\u001b[0m\u001b[0;34m,\u001b[0m\u001b[0;36m0.139890928\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     15\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 16\u001b[0;31m \u001b[0mspec\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mem1d\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mSpecies\u001b[0m\u001b[0;34m(\u001b[0m \u001b[0;34m\"electrons\"\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m-\u001b[0m\u001b[0;36m1.0\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mppc\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mufl\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mufl\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0muth\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0muth\u001b[0m \u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     17\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     18\u001b[0m \u001b[0;32mdef\u001b[0m \u001b[0mrep\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0msim\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mAttributeError\u001b[0m: module 'em1d' has no attribute 'Species'"
     ]
    }
   ],
   "source": [
    "import em1d\n",
    "import numpy as np\n",
    "\n",
    "nx=6*4096\n",
    "box=6*328\n",
    "\n",
    "dt = 0.079\n",
    "tmax = 200.\n",
    "\n",
    "ndump = 50\n",
    "\n",
    "ppc = 16\n",
    "ufl = [0.0,    0.0,  0.0]\n",
    "uth = [0.139890928,0.139890928,0.139890928]\n",
    "\n",
    "spec = em1d.Species( \"electrons\", -1.0, ppc, ufl = ufl, uth = uth )\n",
    "\n",
    "def rep(sim):\n",
    "    # sim.n has the current simulation iteration\n",
    "    if (sim.n % ndump == 0):\n",
    "        sim.emf.report(em1d.EMF.efld,0)\n",
    "        sim.emf.report(em1d.EMF.efld,2)\n",
    "\n",
    "\n",
    "\n",
    "sim = em1d.Simulation( nx, box, dt, spec, report = rep )\n",
    "\n",
    "# Add laser pulse\n",
    "sim.add_laser( em1d.Laser( start = 17.0, rise = 13,flat=300,fall=13, a0 = 0.09, omega0 = 6.32, polarization = np.pi/2 ))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Instability growing from noise"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To Plot ZDF data files you must first import the ZDF module"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import nb_hlpr.tooler as tooler\n",
    "import nb_hlpr.widg as widg\n",
    "import nb_hlpr.visual as visual\n",
    "from IPython.display import HTML"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First you need to select the data files that correspond to your simulation using the file chooser below.\n",
    "<br> You may also specify the data type (*e.g* \"charge\")\n",
    "\n",
    "You can also choose to plot a single quantity or two at the same time on the \"Settings\" tab.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "ae35b777b523463d995b61cfe61c2ba1"
      }
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "tab=widg.tabmaker()\n",
    "tab"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualization\n",
    "\n",
    "Here you can use the slider to advance or go backwards in time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "bb767d462dae41f59bf9c4890d35933d"
      }
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<function nb_hlpr.visual.pltinteract.<locals>.pltstf>"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ylims=[[0,0],[0,0]]\n",
    "fils=[]\n",
    "%matplotlib inline\n",
    "widg.shower(tab,ylims,fils)[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "4b578996f4394e2ab37e32307621ae55"
      }
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<function nb_hlpr.widg.HeatMap_shower.<locals>.ddplt>"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "widg.HeatMap_shower(tab,6.32)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Moduladtion at K=$1/\\lambda_p$\n",
    "\n",
    "The bar's length is $2\\pi$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "f2629b6831ab4d728323a2e60ea6185b"
      }
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<function nb_hlpr.visual.pltcomp.<locals>.pltstf>"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%matplotlib inline\n",
    "widg.comper(fils[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Here you can the the growth of the Stokes and anti-Stokes modes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "9064d588b576479c909832545ca41b09"
      }
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<function nb_hlpr.visual.FT_pltinteract.<locals>.pltstf>"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ylims=[[1e32,1e-16],[1e32,1e-16]]\n",
    "fils=[]\n",
    "%matplotlib inline\n",
    "widg.FT_shower(tab,ylims,fils)[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "fb9d5b55faaa4e1dabb28a19b3db0039"
      }
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<function nb_hlpr.widg.HeatMap_FTshower.<locals>.ddplt>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "widg.HeatMap_FTshower(tab,6.32)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Comparision of the Growth Rates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "6d5e266f14a04ab7842aa7002d2e7d07"
      }
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<function __main__.grwoth_r.<locals>.pltstf>"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "widg.grwoth_r(6.32,\"a_p\",0.09,tab)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "2c4c99355c524aad9bfc53cb741ed7ec"
      }
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<function nb_hlpr.widg.grwoth_r.<locals>.pltstf>"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "widg.grwoth_r(6.32,\"a_+\",0.09,tab)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "11.3807692071 11.5807692071\n"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "ecd6a97fe59a4929859232783608c203"
      }
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<function nb_hlpr.widg.grwoth_r.<locals>.pltstf>"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "import numpy as np\n",
    "def gr(t):\n",
    "    return np.exp(0.09*np.sqrt(6.32)*t/2)\n",
    "\n",
    "widg.grwoth_r(6.32,\"a_p\",tab,gr,type=\"rbs\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
