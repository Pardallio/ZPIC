import os
import ipywidgets as widgets
from ipywidgets import interact, interactive, fixed, interact_manual,Layout

import numpy as np

import matplotlib
from matplotlib import pyplot as plt
from matplotlib import animation

from zdf import read_grid, read_particles
import matplotlib.patches as patches
import scipy.fftpack

from .visual import *


if os.name == 'nt':
    bar="\\"
else:
    bar="/"



#an ugly file browser (sorry...)
class FileBrowser(object):
    def __init__(self):
        self.path = os.getcwd()
        self._update_files()
        self.i=0
    def _update_files(self):
        self.files = list()
        self.dirs = list()
        if os.name == 'nt':
            bar="\\"
        else:
            bar="/"        
        if(os.path.isdir(self.path)):
            for f in os.listdir(self.path):
                ff = self.path + bar + f
                if os.path.isdir(ff):
                    self.dirs.append(f)
                else:
                    if ".zdf" in ff:
                        self.files.append(f)
    def widget(self):
        vmbox=widgets.VBox(layout=Layout(width='100%'))
        vvb=widgets.VBox()
        mbox= widgets.HBox(layout=Layout(flex='1 1 auto',width='55%'))
        vb= widgets.VBox(layout=Layout(align_items="center",align_self="stretch",width="25px"))#layout=Layout(display="flex", flex_flow='collumn',align_items='stretch',border='solid'))
        box = widgets.VBox(layout=Layout(flex='1 1 auto',width='auto',border="1.5px solid PALEGOLDENROD"))
        mbox.children= tuple([box,vb])
        vmbox.children= tuple([mbox,vvb])
        self._update(box,vvb,vb)
        
        return vmbox
    
    def _update(self, box,vvb,vb):
        def on_click(b):
            #check for windows (bars are different)                
            if b.description == '..':
                self.path = os.path.split(self.path)[0]
            else:
                self.path = self.path + bar + b.description
            self._update_files()
            self._update(box,vvb,vb)
            
        items_layout = Layout(flex='1 1 auto',width='auto',margin="1px 0px 1px 0px")
        box_layout = Layout(display='flex',flex_flow="row",align_items='stretch',
                    width='100%')

        def clear(b):
            self._clear(box,vvb,vb)        
        buttons = []
        if self.files or 1:
            button = widgets.Button(description='..',layout=items_layout)
            button.style.button_color="MOCCASIN"
            #            b2=widgets.Button(description="",layout=items_layout)
            #b2.style.button_color="MOCCASIN"
            button.on_click(on_click)
            
            hb1=widgets.HBox([button],layout=box_layout)
    
            buttons.append(hb1)

        if len(self.files+self.dirs)<20:
            for f in self.dirs:
                ll=len(f)*8-(f.count('i')+f.count('l')+f.count('.')+f.count('j')+f.count('I'))*4+sum(1 for c in f if c.isupper())*2-(f.count('t')+f.count('r')+f.count('f')+f.count('-'))*2
                #print(f,ll)
                button = widgets.Button(description=f,layout=Layout(width="{}px".format(ll),margin="1px 0px 1px 0px"), background_color='#d0d0ff',    tooltip=f)
                b2=widgets.Button(description="",layout=items_layout)
                button.style.button_color="LEMONCHIFFON"
                b2.style.button_color="LEMONCHIFFON"

                button.on_click(on_click)
                hb1=widgets.HBox([button,b2],layout=box_layout)
                buttons.append(hb1)

            for f in self.files:
                ll=len(f)*8-(f.count('i')+f.count('l')+f.count('.')+f.count('j')+f.count('I'))*4+sum(1 for c in f if c.isupper())*2-(f.count('t')+f.count('r')+f.count('f')+f.count('-'))*2
                button = widgets.Button(description=f,layout=Layout(width="{}px".format(ll),margin="1px 0px 1px 0px"), background_color='#d0d0ff',    tooltip=f)
                b2=widgets.Button(description="",layout=items_layout)
                button.on_click(on_click)

                hb1=widgets.HBox([button,b2],layout=box_layout)
    
                buttons.append(hb1)

                                
            vb.children=tuple([])

        else:
            def _scroll_dn(b):
                if self.i<len(self.dirs+self.files)-19:
                    self.i=self.i+1;
                    sldr.value=len(self.dirs+self.files)-self.i
                    self._update(box,vvb,vb)
                    
            def _scroll_up(b):
                if self.i>0:
                    self.i=self.i-1;
                    sldr.value=len(self.dirs+self.files)-self.i
                    self._update(box,vvb,vb)

            upbut=widgets.Button(icon="sort-up", button_style='info',layout=Layout(width="20px"))
            dbut=widgets.Button(icon="sort-down", button_style='info',layout=Layout(width="20px"))
            upbut.on_click(_scroll_up)
            dbut.on_click(_scroll_dn)
            sldr=widgets.IntSlider(
                value=len(self.dirs+self.files)-1-self.i,
                min=19,
                max=len(self.dirs+self.files)-1,
                continuous_update=False,
                orientation='vertical',
                readout=False,
                layout=Layout(display="flex",flex="1")
            )
            
            def on_value_change(change):
                self.i=len(self.dirs+self.files)-1-change["new"]
                self._update(box,vvb,vb)
                
            sldr.observe(on_value_change, names='value')
            vb.children=tuple([upbut,sldr,dbut])
            ult_list=self.dirs+self.files
            for f in ult_list[self.i:self.i+19]:
                ll=len(f)*8-(f.count('i')+f.count('l')+f.count('.')+f.count('j')+f.count('I'))*4+sum(1 for c in f if c.isupper())*4-(f.count('t')+f.count('r')+f.count('f')+f.count('-'))*4

                button = widgets.Button(description=f,layout=Layout(width="{}px".format(ll),margin="1px 0px 1px 0px"), background_color='#d0d0ff',    tooltip=f)
                b2=widgets.Button(description="",layout=items_layout)

                if f in self.dirs:
                    button.style.button_color="LEMONCHIFFON"
                    b2.style.button_color="LEMONCHIFFON"
                    button.on_click(on_click)
            
                hb1=widgets.HBox([button,b2],layout=box_layout)
    
    
                buttons.append(hb1)

            #buttons.append(widgets.Button(description=str(len(self.files))+" .zdf files in this folder"))
        but=widgets.Button(description="HIDE", button_style='info')
        but.on_click(clear)
        #buttons.append(but)
        if self.files:
            vaal=self.files[0].split('-')[0]
        else:
            vaal=None
        box.children = tuple(buttons)
        vvb.children=tuple([self.data_folder(),self.data_type(vaal)])

    def _clear(self,box,vvb,vb):
        def on_click(b):
            self._update_files()
            self._update(box,vvb,vb)
        buttons=[]
        but=widgets.Button(description="UNHIDE", button_style='info')
        but.on_click(on_click)
        buttons.append(but)
        if self.files:
            vaal=self.files[0].split('-')[0]
        else:
            vaal=None
        vb.children=tuple()
        box.children = tuple(buttons)
        vvb.children=tuple([self.data_folder(),self.data_type(vaal)])
        
        return box

            
    def data_folder(self):
        self.DF=widgets.Text(value=self.path,placeholder='Enter Data Folder Path (the folder where you have your data files)',description='Data Folder:',layout=Layout(width='75%'),disabled=False)
        return self.DF

    def data_type(self,val):
        self.DK=widgets.Text(value=val,placeholder='Enter the key of the data files (e.g. "charge")',description='Data Key:',layout=Layout(width='75%'),disabled=False)
        return self.DK

    def filelist(self):
        return [self.DF.value+bar+i for i in os.listdir(path) if os.path.isfile(os.path.join(path,i)) and self.DK.value in i]


def tabmaker():
    f = FileBrowser()
    f2= FileBrowser()
    f3= FileBrowser()
    
    w0=widgets.Label("Select which data sets you want to plot",layout=Layout(flex='1 1 auto',width='auto'))
    
    #Data set 1 settings
    w1=widgets.Checkbox(value=True,description='Data Set 1',disabled=False)
    wcp1=widgets.ColorPicker(concise=False,description='Line color',value="darkorange",disabled=False)
    autods1r=widgets.Checkbox(value=True,description='Auto-Range:',disabled=False)
    hb1=widgets.HBox([w1,wcp1,autods1r])
    
    #Data set 2 settings
    w2=widgets.Checkbox(value=False,description='Data Set 2',disabled=False)
    wcp2=widgets.ColorPicker(concise=False,description='Line color',value="dodgerblue",disabled=False)
    autods2r=widgets.Checkbox(value=True,description='Auto-Range:',disabled=False)
    wp2=widgets.Checkbox(value=False,description='Plasma:',disabled=False)
    hb2=widgets.HBox([w2,wcp2,autods2r,wp2])


    wmw2=widgets.Checkbox(value=False,description='Moving Window:',disabled=False)

    hb1=widgets.HBox([w1,wcp1])
    
    w2=widgets.Checkbox(
        value=False,
        description='Data Set 2',
        disabled=False
    )

    wcp2=widgets.ColorPicker(
        concise=False,
        description='line color',
        value="seagreen",
        disabled=False
    )

    hb22=widgets.HBox([w2,wcp2])
    
    w3=widgets.Checkbox(
        value=False,
        description='Data Set 3',
        disabled=False
    )


    wcp3=widgets.ColorPicker(
        concise=False,
        description='line color',
        value="seagreen",
        disabled=False
    )


    wp3=widgets.Checkbox(
        value=False,
        description='Plasma:',
        disabled=False
    )

    wmw2=widgets.Checkbox(
        value=False,
        description='Moving Window:',
        disabled=False
    )

    

    
    w02=widgets.Label("Select the x-axis range",layout=Layout(flex='1 1 auto',width='auto'))

    wxl=widgets.FloatText(description='lower limit:',value=0,disabled=False)
    wxu=widgets.FloatText(description='upper limit:',value=100,disabled=False)
    hb3=widgets.HBox([wxl,wxu])

    

    hb2=widgets.HBox([w3,wcp3,wp3])
    children = [f.widget(),f2.widget(),widgets.VBox([w0,hb1,hb22,hb2,wmw2,w02,hb3])]
    tab = widgets.Tab()
    tab.children = children
    tab.set_title(0, "Data Set 1")
    tab.set_title(1, "Data Set 2")
    tab.set_title(2, "Settings")
    return tab


def shower(tab,yl,fils):
    #Check which datasets are to be plotted
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value
    #Get the plot colors
    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value
    #Get the yrange settings
    yr=[tab.children[2].children[1].children[2].value,tab.children[2].children[2].children[2].value]
    
    #Moving window
    mw=tab.children[2].children[3].value
    #fixed x limis
    xlims=[i.value for i in   tab.children[2].children[5].children]
    
    #plasma shadow mode
    pl2=tab.children[2].children[2].children[3].value
    bool3=tab.children[2].children[3].children[0].value
    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value
    col2=tab.children[2].children[3].children[1].value

    mw=tab.children[2].children[4].value
    xlims=[i.value for i in   tab.children[2].children[6].children]

    pl2=tab.children[2].children[3].children[2].value
    
    if bool1 and bool2:
        fldr=tab.children[0].children[1].children[0].value
        key=tab.children[0].children[1].children[1].value
        files1 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    
        fldr=tab.children[1].children[1].children[0].value
        key=tab.children[1].children[1].children[1].value
        files2 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        files=[files1,files2]
        cols=[col1,col2]
        return [pltinteract(files,cols,xlims,yl,fils,plas=pl2,mw=mw,yrs=yr),*files]
    
    elif bool1 or bool2:    
        fldr=tab.children[int(bool2)].children[1].children[0].value
        key=tab.children[int(bool2)].children[1].children[1].value
        files = [[fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i],[]]

        col=tab.children[2].children[int(bool2)+1].children[1].value
        return [pltinteract(files,[col,None],xlims=xlims,ylim=yl,fils=fils,plas=(pl2 and bool2),mw=mw,yrs=yr),files[0]]


def FT_shower(tab,yl,fils):
    #Check which datasets are to be plotted
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value
    #Get the plot colors
    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value
    
    xlims=[i.value for i in   tab.children[2].children[5].children]

    
    if bool1 and bool2:
        fldr=tab.children[0].children[1].children[0].value
        key=tab.children[0].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    
        fldr=tab.children[1].children[1].children[0].value
        key=tab.children[1].children[1].children[1].value
        files2 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        return [FT_pltinteract(files,col1,files2,col2,xlims,yl,fils)]
    
    elif bool1 or bool2:    
        fldr=tab.children[int(bool2)].children[1].children[0].value
        key=tab.children[int(bool2)].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]

        col=tab.children[2].children[int(bool2)+1].children[1].value
        return [FT_pltinteract(files,col,xlims=xlims,ylim=yl,fils=fils)]


#Show a heat map with the EM wave!
#X---> X
#Y---> Time

from matplotlib import ticker

def HeatMap_shower(tab,om,opt_cmap='RdBu'):
    #Get the files list
    files=filer(tab)[0]
    x=[]
    t=[]
    E=[]
    #Read the first file
    (data, info) = read_grid(files[0])
    #build the x-axis array
    xmin=info['grid']['axis'][0]['min']
    xmax=info['grid']['axis'][0]['max']
    npts=info['grid']['nx'][0]
    xx=np.linspace(xmin,xmax,npts)
    
    #build the x,T,E list
    for fil in files[0::1]:
        #Read the files chosen by the user
        (data, info) = read_grid(fil)
        for it,xi in enumerate(xx[0::1]):
            t.append(info['iteration']['t'])
            x.append(xi)
            E.append(data[it])

    #convert all values into matrix style
    x=np.unique(x)
    t=np.unique(t)
    X,T= np.meshgrid(x,t)
    Ef=np.array(E).reshape(len(t),len(x))
    #Plot the heat_map
    def ddplt(X,T,Ef,om,show_b,xbas=10,ybas=40,dx=15,xrg=None,bars=None):
        #compute the slope
        m=1/(np.sqrt(1+1/(om**2-1))-1)
        
        #Update the sliders
        bars[0].min=xrg[0]
        bars[0].max=xrg[1]-dx
        bars[1].min=T[0,0]
        bars[1].max=T[-1,0]-dx*m
        bars[2].min=0
        bars[2].max=min((T[-1,0]-ybas)/m,xrg[1]-xbas)
        
        #set up the plot
        fig=plt.figure(figsize=(16, 9))
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')
        ax1=plt.gca()
        ax1.set_xlabel("$"+info['grid']['axis'][0]['label']+"$"+" [$"+info['grid']['axis'][0]['units']+"$]")
        ax1.set_ylabel("$t$"+" $["+info['iteration']['tunits']+"]$")
        sfmt=ticker.ScalarFormatter(useMathText=True)
        sfmt.set_powerlimits((0, 0))
        ax1.set_ylim([T[0,0],T[-1,0]])
        ax1.set_xlim(xrg)
        
        #acutally plot stuff
        pax=ax1.pcolormesh(X,T,Ef,cmap=opt_cmap)
        if(show_b):
            ax1.plot([xbas,xbas+dx],[ybas,ybas+dx*m],c='k',lw=10)
            ax1.plot([xbas,xbas+dx],[ybas,ybas+dx*m],c='lime',lw=5)
        cbar = fig.colorbar(pax,format=sfmt)
        cbar.set_label("$"+info['grid']['label']+"$"+" ($"+info['grid']['units']+"$)")
        plt.show()
    
    xbas_sldr=widgets.FloatSlider(value=10,orientation='horizontal',description='$x_1$')
    ybas_sldr=widgets.FloatSlider(value=40,orientation='horizontal',description='$y_1$')
    dx_sldr=widgets.FloatSlider(value=15,orientation='horizontal',description='$\Delta x$')
    
    xrang_sldr=widgets.FloatRangeSlider(value=[min(x), max(x)],min=min(x),max=max(x),step=0.1,description='$x_{\\text{range}}$',
                                        orientation='horizontal',readout=True,readout_format='.1f',)
    show_bar=widgets.Checkbox(value=True,description='Show slope:')
    
    return interact(ddplt,xbas=xbas_sldr,ybas=ybas_sldr,dx=dx_sldr,xrg=xrang_sldr,show_b=show_bar,
                    X=fixed(X),T=fixed(T),Ef=fixed(Ef),om=fixed(om),bars=fixed([xbas_sldr,ybas_sldr,dx_sldr]))
