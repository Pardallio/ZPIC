import os
import ipywidgets as widgets
from ipywidgets import interact, interactive, fixed, interact_manual,Layout

import numpy as np

import matplotlib
from matplotlib import pyplot as plt
from matplotlib import animation

from zdf import read

import matplotlib.patches as patches
import scipy.fftpack
from scipy.signal import blackman
if os.name == 'nt':
    bar="\\"
else:
    bar="/"


def pltinteract(filelst1,col1,filelst2=[],col2=None,xlims=None,ylim=None,fils=[],plas=False,mw=False):
    ylim.clear()
    (data0,inf0)=read(filelst1[0])
    ymin=min(data0)
    ymax=max(data0)
    
    ylimin=ymin-(ymax-ymin)/10
    ylimax=ymax+(ymax-ymin)/10
    ylims=[ylimin,ylimax]
    ylims2=[0,0]
    ylim.append(ylims)
    filelst1.sort()
    ndump=int(filelst1[1].split("/")[-1].split("-")[-1].split(".zdf")[0])
    if filelst2:
        filelst2.sort()
        (data02,inf02)=read(filelst2[0])
        ymin2=min(data02)
        ymax2=max(data02)

        ylimin2=ymin2-(ymax2-ymin2)/10
        ylimax2=ymax2+(ymax2-ymin2)/10
        ylims2=[ylimin2,ylimax2]
    ylim.append(ylims2)
    def pltstf(itr,fil,col,fil2=[],col2=None,xlims=None,ylims=None,fils=[],plasm=False):
        itr=int(itr/ndump)
        (data, info) = read(fil[itr])
        fils.clear()
        fils.append(fil[itr])
        xmin=info.grid.axis[0].min
        xmax=info.grid.axis[0].max
        npts=info.grid.nx[0]
        global ymin
        global ymax
        ymin=min(data)
        ymax=max(data)
        ylimin=ymin-(ymax-ymin)/10
        ylimax=ymax+(ymax-ymin)/10

        if ylimin<ylims[0][0]:
            ylims[0][0]=ylimin

        if ylimax>ylims[0][1]:
            ylims[0][1]=ylimax

        
        xx=np.linspace(xmin,xmax,npts)
        if not mw:
            xmin=xlims[0];
            xmax=xlims[1];
        
        fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')

        ax1=plt.gca()
        lines=[]
        lines.append(ax1.plot(xx,data,c=col,lw=3.5,label="$"+info.grid.label+"$")[0]);
        #lines.append(ax1.plot(xx,data,'+',c="coral")[0]);
        ax1.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='right')
        ax1.set_xlim([xmin,xmax])
        ax1.set_ylim(ylims[0])
        ax1.set_xlabel("$"+info.grid.axis[0].label+"$"+" ($"+info.grid.axis[0].units+"$)")
        ax1.set_ylabel("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")
        if plasm and not fil2:
            ax1.fill_between(xx,data,0,alpha=0.4,color=col)
        if fil2:
            (data2, info2) = read(fil2[itr])
            fils.append(fil2[itr])
            ax2=ax1.twinx()
            
            ymin2=min(data2)
            ymax2=max(data2)
            ylimin2=ymin2-(ymax2-ymin2)/10
            ylimax2=ymax2+(ymax2-ymin2)/10

            if ylimin2<ylims[1][0]:
                ylims[1][0]=ylimin2
            
            if ylimax2>ylims[1][1]:
                ylims[1][1]=ylimax2

            ax2.set_xlim([xmin,xmax])
            ax2.set_ylim(ylims[1])
            ax2.set_xlabel("$"+info2.grid.axis[0].label+"$"+" ($"+info2.grid.axis[0].units+"$)")
            ax2.set_ylabel("$"+info2.grid.label+"$"+" ($"+info2.grid.units+"$)")
            ax2.plot(xx,data2,c=col2,lw=3.5,label="$"+info2.grid.label+"$")
            ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))
            if plasm:
                ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
            #lin[0].set_data(xx,data)
            #lin[1].set_data(xx,data)
        
            #xx=np.transpose(xx)
            #print(xx,len(xx))
            ax1.legend(loc=0)
        plt.show()
    
    xmin=inf0.grid.axis[0].min
    xmax=inf0.grid.axis[0].max

    vxmin=inf0.grid.axis[0].min if inf0.grid.axis[0].min > xlims[0] else xlims[0] 
    vxmax=inf0.grid.axis[0].max if inf0.grid.axis[0].max < xlims[1] else xlims[1]

    sldr=widgets.IntSlider(value=0,min=0,max=ndump*(len(filelst1)-1),step=ndump,orientation='horizontal',readout=True,description='Iteration: ')
    sldr2=widgets.FloatRangeSlider(value=[vxmin,vxmax],min=xmin,max=xmax,step=(xmax-xmin)/511,orientation='horizontal',readout=True,description='Xlim: ')

    return interact(pltstf,itr=sldr,xlims=sldr2,fil=fixed(filelst1),fil2=fixed(filelst2),col=fixed(col1),col2=fixed(col2),ylims=fixed(ylim),fils=fixed(fils),plasm=fixed(plas))


def FT_pltinteract(filelst1,col1,filelst2=[],col2=None,xlims=None,ylim=None,fils=[]):
    #empty the y limits list
    ylim.clear()
    #read the first data set
    (data0,inf0)=read(filelst1[0])
    #find the dimensions of the box
    xmin=inf0.grid.axis[0].min
    xmax=inf0.grid.axis[0].max
    npts=inf0.grid.nx[0]
    
    # sample period
    T = xmax / npts
    #Compute the first FFT
    yf = 2.0/npts * np.abs(scipy.fftpack.fft(data0))
    #Build the frequency array
    xf = 2*3.14*np.linspace(0.0, 1.0/(2.0*T), int(npts//2))
    
    # use propper limits for the y-axis
    ymin=min(yf)
    ymax=max(yf)
    ylimin=ymin#-(ymax-ymin)/10
    ylimax=ymax#+(ymax-ymin)/10
    ylims=[ylimin,ylimax]
    ylims2=[0,0]
    ylim.append(ylims)
    filelst1.sort()
    ndump=int(filelst1[1].split("/")[-1].split("-")[-1].split(".zdf")[0])
    if filelst2:
        #read the second data set if there is one
        (data02,inf02)=read(filelst2[0])
        #compute the second FFT
        yf = 2.0/npts * np.abs(scipy.fftpack.fft(data02))
        
        # use propper limits for the y-axis again
        ymin2=min(yf)
        ymax2=max(yf)
        ylimin2=ymin2-(ymax2-ymin2)/10
        ylimax2=ymax2+(ymax2-ymin2)/10
        ylims2=[ylimin2,ylimax2]
    ylim.append(ylims2)
    
    #This function is called whenever the user moves the slider
    def pltstf(itr,fil,col,fil2=[],col2=None,xlims=None,ylims=None,fils=[]):
        itr=int(itr/ndump)
        # Build the frame for the plot
        fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')
        ax1=plt.gca()
        
        #Read the file chosen by the user
        (data, info) = read(fil[itr])
        
        # This variable stores the information about the current file so that it can
        # be accessed outside the function
        fils.clear()
        fils.append(fil[itr])
        
        # find the current dimensions of the box
        xmin=info.grid.axis[0].min
        xmax=info.grid.axis[0].max
        npts=info.grid.nx[0]
        
        # sample spacing
        T = (xmax-xmin) / npts
        y = data
        wind=1#blackman(len(y))
        yf = 2.0/npts * np.abs(scipy.fftpack.fft(wind*y))
        xf = 2*3.14*np.linspace(0.0, 1.0/(2.0*T), int(npts//2))
        
        # compute propper limits for the y axis
        ymin=min(yf)
        ymax=max(yf)
        ylimin=ymin#-(ymax-ymin)/10
        ylimax=ymax#+(ymax-ymin)/10
        #check if an update to the y-axis limits is necessary
        if ylimin<ylims[0][0]:
            ylims[0][0]=ylimin
        if ylimax>ylims[0][1]:
            ylims[0][1]=ylimax
        #ax1.set_ylim(ylims[0])
        ax1.set_xlim(xlims)
        
        # plot the fourier transform
        ax1.semilogy(xf[1:],yf[1:int(npts//2)],c=col,lw=3.5,label="$"+info.grid.label+"$")
        
        #set the tilte of the plot: it will contain the information about the current time
        ax1.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='center')
        
        #label the axis
        ax1.set_xlabel("$ k$ [$\omega_p/c$]")
        ax1.set_ylabel("$FFT("+info.grid.label+")$"+" [$|"+info.grid.units+"|^2$]")
        
        #repeat the process for the second data set
        if fil2:
            (data2, info2) = read(fil2[itr])
            fils.append(fil2[itr])
            ax2=ax1.twinx()
            
            yf2 = 2.0/npts * np.abs(scipy.fftpack.fft(data2))
            
            ymin2=min(yf2)
            ymax2=max(yf2)
            ylimin2=ymin2-(ymax2-ymin2)/10
            ylimax2=ymax2+(ymax2-ymin2)/10
            
            if ylimin2<ylims[1][0]:
                ylims[1][0]=ylimin2
            
            if ylimax2>ylims[1][1]:
                ylims[1][1]=ylimax2
            ax2.set_xlim(xlims)
            #ax2.set_ylim(ylims[1])
            ax2.set_ylabel("$FFT("+info2.grid.label+")$"+" [$|"+info.grid.units+"|^2$]")
            #ax2.plot(xf,yf2[:int(npts//2)],c=col2,lw=3.5,label="$"+info2.grid.label+"$")
            ax2.semilogy(xf[1:],yf2[1:int(npts//2)],c=col2,lw=3.5,label="$"+info2.grid.label+"$")
            ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))
            ax1.legend(loc=0)
        plt.show()

    xmin=0
    xmax=2*3.14*1.0/(2.0*T)

    vxmin=xmin if xmin > xlims[0] else xlims[0] 
    vxmax=xmax if xmax < xlims[1] else xlims[1]

    sldr=widgets.IntSlider(value=0,min=0,max=ndump*(len(filelst1)-1),step=ndump,orientation='horizontal',readout=True,description='Iteration: ')
    sldr2=widgets.FloatRangeSlider(value=[vxmin,vxmax],min=xmin,max=xmax,step=(xmax-xmin)/511,orientation='horizontal',readout=True,description='Xlim: ')
    return interact(pltstf,itr=sldr,xlims=sldr2,fil=fixed(filelst1),fil2=fixed(filelst2),col=fixed(col1),col2=fixed(col2),ylims=fixed(ylim),fils=fixed(fils))


def filer(tab):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value
    
    xlims=[i.value for i in   tab.children[2].children[5].children]
    
    if bool1 and bool2:
        fldr=tab.children[0].children[1].children[0].value
        key=tab.children[0].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        fldr=tab.children[1].children[1].children[0].value
        key=tab.children[1].children[1].children[1].value
        files2 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        return [files,files2,xlims]
    
    elif bool1 or bool2:
        fldr=tab.children[int(bool2)].children[1].children[0].value
        key=tab.children[int(bool2)].children[1].children[1].value
        files = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
        
        return [files,None,xlims]


def pltcomp(fil):
    (data0,inf0)=read(fil)
    ymin=min(data0)
    ymax=max(data0)
    
    ylimin=ymin-(ymax-ymin)/10
    ylimax=ymax+(ymax-ymin)/10

    xmin=inf0.grid.axis[0].min
    xmax=inf0.grid.axis[0].max

    def pltstf(fil,xlim,ypos,xpos,bars):
        fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k');
        matplotlib.rcParams.update({'font.size': 16})
        plt.grid(alpha=0.6,linestyle='dashed')
        ax1=plt.gca()
        #Read the file chosen by the user
        (data, info)=read(fil)
        bars[0].min=xlim[0]
        bars[0].max=xlim[1]
        bars[0].step=(xlim[1]-xlim[0])/511
              
        # find the current dimensions of the box
        xmin=info.grid.axis[0].min
        xmax=info.grid.axis[0].max
        npts=info.grid.nx[0]
        
        ymin=min(data)
        ymax=max(data)
        ylimin=ymin-(ymax-ymin)/10
        ylimax=ymax+(ymax-ymin)/10

        ax1.set_ylim([ylimin,ylimax])
        ax1.set_xlim(xlim)
        
        xx=np.linspace(xmin,xmax,npts)
        

        lines=[]
        lines.append(ax1.plot(xx,data,lw=3.5,label="$"+info.grid.label+"$")[0]);
        #lines.append(ax1.plot(xx,data,'+',c="coral")[0]);
        ax1.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='right')
        ax1.set_xlabel("$"+info.grid.axis[0].label+"$"+" ($"+info.grid.axis[0].units+"$)")
        ax1.set_ylabel("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")

        xbarmin=xpos
        xbarmax=xbarmin+6.28
        ybar=ypos
        ylims=ax1.get_ylim()
        ybarmin=(ybar-ylimin)/(ylimax-ylimin)-0.03
        ybarmax=(ybar-ylimin)/(ylimax-ylimin)+0.03
        xbarmin=(xbarmin-xlim[0])/(xlim[1]-xlim[0])
        xbarmax=(xbarmax-xlim[0])/(xlim[1]-xlim[0])
        

        ax1.axhline(y=ybar, xmin=xbarmin, xmax=xbarmax,c='k',lw=5)
        ax1.axvline(x=xpos, ymin=ybarmin, ymax=ybarmax,c='k',lw=5)
        ax1.axvline(x=xpos+6.28, ymin=ybarmin, ymax=ybarmax,c='k',lw=5)
        plt.show()



    sldr1=widgets.FloatSlider(value=xmin,min=xmin,max=xmax,step=(xmax-xmin)/511,orientation='horizontal',readout=True,description='x pos: ')
    sldr2=widgets.FloatSlider(value=(ylimin+ylimax)/2,min=ylimin,max=ylimax,step=(ylimax-ylimin)/511,orientation='horizontal',readout=True,description='y pos: ')
    sldr3=widgets.FloatRangeSlider(value=[xmin,xmax],min=xmin,max=xmax,step=(xmax-xmin)/511,orientation='horizontal',readout=True,description='Xlim: ')
    return interact(pltstf,fil=fixed(fil),xlim=sldr3,xpos=sldr1,ypos=sldr2,bars=fixed([sldr1]))


def animate(tab,yl):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value
    
    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value
    
    
    fil=filer(tab)
    xlims=fil[2]
    
    pl2=tab.children[2].children[2].children[2].value
    # First set up the figure, the axis, and the plot element we want to animate
    fig=plt.figure(figsize=(16,9), dpi= 120, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    mw=tab.children[2].children[4].value
    if bool2 and bool1:
        (data, info) = read(fil[0][0])
        xmin=info.grid.axis[0].min
        xmax=info.grid.axis[0].max
        npts=info.grid.nx[0]
        xx=np.linspace(xmin,xmax,npts)

        (data2, info2) = read(fil[1][0])
        
        ax = plt.axes()
        if not mw:
            xmin=xlims[0];
            xmax=xlims[1];

        ax.set_xlim([xmin,xmax])
        ax.set_ylim(yl[0])
        ax2 = ax.twinx()
        ax2.set_ylim(yl[1])
        lines =[]
        lines.append(ax.plot([], [], lw=2,c=col1,label="$"+info.grid.label+"$")[0])
        lines.append(ax2.plot([], [], lw=2,c=col2,label="$"+info2.grid.label+"$")[0])
        ax.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='right')
        ax.set_xlabel("$"+info.grid.axis[0].label+"$"+" ($"+info.grid.axis[0].units+"$)")
        ax.set_ylabel("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")
        ax2.set_ylabel("$"+info2.grid.label+"$"+" ($"+info2.grid.units+"$)")
        ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))
        if pl2:
            for coll in (ax2.collections):
                ax2.collections.remove(coll)
            ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
        ax.legend(loc=0)
        
    elif bool1 or bool2:
        (data, info) = read(fil[bool2][0])
        xmin=info.grid.axis[0].min
        xmax=info.grid.axis[0].max
        npts=info.grid.nx[0]
        xx=np.linspace(xmin,xmax,npts)
        if not mw:
            xmin=xlims[0];
            xmax=xlims[1];
    
        ax = plt.axes(xlim=[xmin,xmax], ylim=yl[bool2])
        lines =[]
        lines.append(ax.plot([], [], lw=2)[0])
        ax.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='right')
        ax.set_xlabel("$"+info.grid.axis[0].label+"$"+" ($"+info.grid.axis[0].units+"$)")
        ax.set_ylabel("$"+info.grid.label+"$"+" ($"+info.grid.units+"$)")
        if pl2 and not bool1:
            ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
        
    # initialization function: plot the background of each frame
    def init():
        for line in lines:
            line.set_data([], [])
        ax.set_xlim(xlims)
        return lines

    # animation function.  This is called sequentially
    def animate(i,files,mw):
        if bool1 and bool2:
            (data, info) = read(files[0][i])
            xmin=info.grid.axis[0].min
            xmax=info.grid.axis[0].max
            npts=info.grid.nx[0]
            xx=np.linspace(xmin,xmax,npts)
            if not mw:
                xmin=xlims[0];
                xmax=xlims[1];
            ax.set_xlim([xmin,xmax])
            ax2.set_xlim([xmin,xmax])
            ax.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='right')
            lines[0].set_data(xx, data)
            (data, info) = read(files[1][i])
            lines[1].set_data(xx,data)
            if pl2:
                for coll in (ax2.collections):
                    ax2.collections.remove(coll)
                ax2.fill_between(xx,data,0,alpha=0.4,color=col2)
        elif bool1 or bool2:
            (data, info) = read(files[bool2][i])
            ax.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='right')
            xmin=info.grid.axis[0].min
            xmax=info.grid.axis[0].max
            npts=info.grid.nx[0]
            if not mw:
                xmin=xlims[0];
                xmax=xlims[1];
            ax.set_xlim([xmin,xmax])
            xx=np.linspace(xmin,xmax,npts)
            x = xx
            y = data            
            lines[0].set_data(x, y)
            
        return lines

    # call the animator.  blit=True means only re-draw the parts that have changed.
    return animation.FuncAnimation(fig, animate,fargs=(fil,mw,), init_func=init,
                               frames=len(fil[0]), interval=20, blit=True)

def FT_animate(tab,yl):
    bool1=tab.children[2].children[1].children[0].value
    bool2=tab.children[2].children[2].children[0].value
    
    col1=tab.children[2].children[1].children[1].value
    col2=tab.children[2].children[2].children[1].value
    
    
    fil=filer(tab)
    xlims=fil[2]
    
    pl2=tab.children[2].children[2].children[2].value
    # First set up the figure, the axis, and the plot element we want to animate
    fig=plt.figure(figsize=(16,9), dpi= 120, facecolor='w', edgecolor='k');
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    
    if bool2 and bool1:
        (data, info) = read(fil[0][0])
        xmin=info.grid.axis[0].min
        xmax=info.grid.axis[0].max
        npts=info.grid.nx[0]
        
        # sample spacing
        T = (xmax-xmin) / npts
        xf = 2*3.14*np.linspace(0.0, 1.0/(2.0*T), int(npts//2))
        
        
        (data2, info2) = read(fil[1][0])
        
        ax = plt.axes()
        ax.set_xlim(xlims)
        ax.set_ylim(yl[0])
        ax2 = ax.twinx()
        ax2.set_ylim(yl[1])
        lines =[]
        lines.append(ax.plot([], [], lw=2,c=col1,label="$"+info.grid.label+"$")[0])
        lines.append(ax2.plot([], [], lw=2,c=col2,label="$"+info2.grid.label+"$")[0])
        ax.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='right')
        ax.set_xlabel("$ k$ [$\omega_p/c$]")
        ax.set_ylabel("$FFT("+info.grid.label+")$"+" [$|"+info.grid.units+"|^2$]")
        ax2.set_ylabel("$FFT("+info2.grid.label+")$"+" [$|"+info.grid.units+"|^2$]")
        ax2.legend(loc=0, bbox_to_anchor=(1, 0.92))
        if pl2:
            ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)
        ax.legend(loc=0)

    elif bool1 or bool2:
        (data, info) = read(fil[bool2][0])
        xmin=info.grid.axis[0].min
        xmax=info.grid.axis[0].max
        npts=info.grid.nx[0]
        xx=np.linspace(xmin,xmax,npts)
        
        ax = plt.axes(xlim=xlims, ylim=yl[bool2])
        lines =[]
        lines.append(ax.plot([], [], lw=2)[0])
        ax.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='right')
        ax.set_xlabel("$ k$ [$\omega_p/c$]")
        ax.set_ylabel("$FFT("+info.grid.label+")$"+" [$|"+info.grid.units+"|^2$]")
        if pl2 and not bool1:
            ax2.fill_between(xx,data2,0,alpha=0.4,color=col2)

    # initialization function: plot the background of each frame
    def init():
        for line in lines:
            line.set_data([], [])
            ax.set_xlim(xlims)
        return lines
    
    # animation function.  This is called sequentially
    def animate(i,files):
        if bool1 and bool2:
            (data, info) = read(files[0][i])
            ax.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='center')
            
            xmin=info.grid.axis[0].min
            xmax=info.grid.axis[0].max
            npts=info.grid.nx[0]
            # sample spacing
            T = (xmax-xmin) / npts
            xf = 2*3.14*np.linspace(0.0, 1.0/(2.0*T), int(npts//2))
            yf2 = 2.0/npts * np.abs(scipy.fftpack.fft(data))
            
            lines[0].set_data(xf, yf2[:int(npts//2)])
            (data, info) = read(files[1][i])
            yf2 = 2.0/npts * np.abs(scipy.fftpack.fft(data))
            lines[1].set_data(xf,yf2[:int(npts//2)])
        elif bool1 or bool2:
            (data, info) = read(files[bool2][i])
            xmin=info.grid.axis[0].min
            xmax=info.grid.axis[0].max
            npts=info.grid.nx[0]
            # sample spacing
            T = (xmax-xmin) / npts
            xf = 2*3.14*np.linspace(0.0, 1.0/(2.0*T), int(npts//2))
            ax.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='center')
            yf2 = 2.0/npts * np.abs(scipy.fftpack.fft(data))
            lines[0].set_data(xf, yf2[:int(npts//2)])
        
        return lines

    # call the animator.  blit=True means only re-draw the parts that have changed.
    return animation.FuncAnimation(fig, animate,fargs=(fil,), init_func=init,
                               frames=len(fil[0]), interval=20, blit=True)

def pha_animate(ndump,fldrnam,xlims):
    import zdf2 as zdf
    import matplotlib.pyplot as plt
    bar="/"
    fldr=fldrnam+bar
    key="marker1"
    files1 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    key="marker2"
    files2 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]
    key="marker3"
    files3 = [fldr+bar+i for i in os.listdir(fldr) if os.path.isfile(os.path.join(fldr,i)) and key in i]

    (particles1,info) = zdf.read(files1[0])
    (particles2,info2) = zdf.read(files2[0])
    (particles3,info3) = zdf.read(files3[0])

    x1=particles1["x1"]
    v1=particles1["v1"]
    x2=particles2["x1"]
    v2=particles2["v1"]
    x3=particles3["x1"]
    v3=particles3["v1"]

    xmin=min([min(x1),min(x3)])
    xmax=max([max(x1),max(x3)])

    vmin=min([min(v1),min(v3)])
    vmax=max([max(v1),max(v3)])
    dv=vmax-vmin
    vmin=vmin-dv/2
    vmax=vmax+dv/2    

    
    fig=plt.figure(figsize=(16, 9), dpi= 120, facecolor='w', edgecolor='k')
    matplotlib.rcParams.update({'font.size': 16})
    plt.grid(alpha=0.6,linestyle='dashed')
    #plt.style.use("default")
    ax1=plt.gca()
    ax1.patch.set_facecolor("k")
    lines=[]
    lines.append(ax1.plot(x1,v1,".",ms=3.,alpha=1,c="y")[0])
    lines.append(ax1.plot(x2,v2,".",ms=3.,alpha=0.8,c="orange")[0])
    lines.append(ax1.plot(x3,v3,".",ms=3.,alpha=0.8,c="r")[0])
    #lines.append(ax1.plot(xx,data,'+',c="coral")[0]);
    ax1.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='right')
        
    ax1.set_xlim(xlims)
    ax1.set_ylim([vmin,vmax])
    #xlabel = "x_1\,[{:s}]".format( info.particles.units.x1 )
    ylabel = "$u_1\,[{:s}]$".format( info.particles.units.v1 )
    xlabel = "$x_1\,[{:s}]$".format( info.particles.units.x1 )
    ax1.set_xlabel(xlabel)
    ax1.set_ylabel(ylabel)

    


    # initialization function: plot the background of each frame
    def init():
        for line in lines:
            line.set_data([], [])
        return lines
    
    # animation function.  This is called sequentially
    def animate(i):
        (particles1,info) = zdf.read(files1[i])
        (particles2,info2) = zdf.read(files2[i])
        (particles3,info3) = zdf.read(files3[i])

        x1=particles1["x1"]
        v1=particles1["v1"]
        x2=particles2["x1"]
        v2=particles2["v1"]
        x3=particles3["x1"]
        v3=particles3["v1"]
        ax1.set_title("$n$="+str(info.iteration.n)+", $t$="+'{:.2f}'.format(info.iteration.t)+"$("+info.iteration.tunits+")$",loc='right')

        lines[0].set_data(x1,v1)
        lines[1].set_data(x2,v2)
        lines[2].set_data(x3,v3)
        return lines

    # call the animator.  blit=True means only re-draw the parts that have changed.
    return animation.FuncAnimation(fig, animate, init_func=init,
                               frames=len(files1), interval=20, blit=True)

